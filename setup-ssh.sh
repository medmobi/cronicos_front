#!/bin/bash

echo "Adding SSH config" >&1

# Ensure we have the ssh folder
if [ ! -d ~/.ssh ]; then
  mkdir -p ~/.ssh
  chmod 700 ~/.ssh
fi

# Setting SSH key as environment variable
unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
   GIT_SSH_KEY=$(base64 -w 0 < private_modules_private_key_id_rsa)
   echo $GIT_SSH_KEY | base64 -d > ~/.ssh/deploy_key
elif [[ "$unamestr" == 'Darwin' ]]; then
   GIT_SSH_KEY=$(base64 -b 0 < private_modules_private_key_id_rsa)
   echo $GIT_SSH_KEY | base64 -D > ~/.ssh/deploy_key
fi

# Change the permissions on the file to
# be read-write for this user.
chmod 600 ~/.ssh/deploy_key

# Setup the ssh config file.
# Switch out the hostname for different hosts.
echo -e "Host bitbucket.org\n"\
        " IdentityFile ~/.ssh/deploy_key\n"\
        " IdentitiesOnly yes\n"\
        " UserKnownHostsFile=/dev/null\n"\
        " StrictHostKeyChecking no"\
        > ~/.ssh/config
