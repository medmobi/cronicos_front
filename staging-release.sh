#!/usr/bin/env bash

rm -r dist

echo "----- Starting application build -----"
gulp build
echo "----- Application builded with success! -----"

cp src/.htaccess dist/.htaccess
tar -czvf cronicos-staging-front-production.tar.gz dist
scp cronicos-staging-front-production.tar.gz root@cronicos.anexs.com.br:/var/www/
ssh root@cronicos.anexs.com.br '/var/www/scripts/cronicos-front-staging-backup-deploy.sh'


