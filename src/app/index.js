(function() {
    'use strict';

    angular.module('singApp', [
        'lbServices',
        'singApp.core',
        'singApp.dashboard',
        'singApp.login',
        'pascoal.crud',
        'singApp.especialidades',
        'singApp.programas',
        'singApp.convenios',
        'singApp.doencas',
        'singApp.categoria-doencas',
        'singApp.feriados',
        'singApp.unidades-medida',
        'singApp.bancos',
        'singApp.planodesaudes',
        'singApp.estadocivils',
        'singApp.tipodetelefones',
        'singApp.tiposanguineos',
        'singApp.posologias',
        'singApp.duracaos',
        'singApp.vias',
        'singApp.frequencias',
        'singApp.perfilusuarios',
        'singApp.pacientes',
        'singApp.prestadores',
        'singApp.users',
        'singApp.conveniados',
        'singApp.empresas',
        'singApp.indicadores',
        'singApp.conselhos',
        'singApp.formularios',
        'singApp.prontuarios',
        'singApp.atendimentos',
        'singApp.medicamentos',
        'singApp.agenda',
        'singApp.preReceitas',
        'singApp.documentosMedicos',
        'singApp.form.elements',
        // 'singApp.maps.google',
        // 'singApp.maps.vector',
        // 'singApp.grid',
        // 'singApp.tables.basic',
        // 'singApp.tables.dynamic',
        // 'singApp.extra.calendar',
        // 'singApp.extra.invoice',
        // 'singApp.error',
        // 'singApp.extra.gallery',
        // 'singApp.extra.search',
        // 'singApp.extra.timeline',
        // 'singApp.ui.components',
        // 'singApp.ui.notifications',
        // 'singApp.ui.icons',
        // 'singApp.ui.buttons',
        // 'singApp.ui.tabs-accordion',
        // 'singApp.ui.list-groups',
        // 'singApp.inbox',
        // 'singApp.profile',
        // 'singApp.widgets',
        'singApp.charts',
        // 'singApp.form.validation',
        // 'singApp.form.wizard',
    ]);
})();
