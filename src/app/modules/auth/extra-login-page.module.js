(function() {
  'use strict';

  var module = angular.module('singApp.login', [
    'ui.router',
    'satellizer',
    'ui.jq',
    'singApp.components.wizard'
  ]);

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/modules/auth/extra-login-page.html',
        controller: 'LoginController'
      })
      .state('password', {
        url: '/password',
        templateUrl: 'app/modules/auth/password-recovery.html',
        controller: 'PasswordController'
      })
      .state('password-reset', {
        url: '/nova-senha',
        templateUrl: 'app/modules/auth/password-recovery-new.html',
        controller: 'PasswordResetController',
        controllerAs: 'vm'
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/modules/auth/signup.html',
        controller: 'SignUpController'
      });
  }

  module.controller('LoginController', function($scope, $rootScope) {
    $scope.login = function() {
      // console.log($scope.email, $scope.password);
      var credentials = {
        email : $scope.email.toLowerCase(),
        password: $scope.password
      };

      $rootScope.login(credentials);
    };
    $scope.getYear = function () {
      return new Date().getFullYear();
    };
  });

  module.controller('SignUpController', function($scope, $rootScope, $location) {
    $scope.signup = {};

    var query = $location.search();
    for (var x in query) {
      $scope.signup[x] = query[x];
    }

    $scope.register = function() {
      // var credentials = {
      //   email : $scope.email,
      //   password: $scope.password,
      //   phone: $scope.phone,
      //   first_name: $scope.first_name,
      //   last_name: $scope.last_name
      // };

      var credentials = $scope.signup;

      $rootScope.register(credentials);
    }
  });

  module.controller('PasswordResetController', function($scope, $rootScope, $location, $state) {
    var vm = this;

    vm.accessToken = '';

    vm.reset = reset;
    vm.checkPwd = checkPwd;
    vm.year = moment().format('YYYY');

    ////////////

    init();

    function init() {
        vm.accessToken = $location.search().access_token;
        if (!vm.accessToken) $state.go('login');
    }

    function reset () {
        // console.log($scope.email, $scope.password);
        var data = {
            accessToken: vm.accessToken,
            password : vm.password,
            confirmation: vm.password_confirmation
        };

        $rootScope.passwordReset(data);
    }

    function checkPwd() {
        return vm.password != vm.password_confirmation;
    }
  });

  module.controller('PasswordController', function($scope, $rootScope) {
    $scope.year = moment().format('YYYY');

    $scope.recover = function() {
      // console.log($scope.email, $scope.password);
      var credentials = {
        email : $scope.email,
      };

      $rootScope.recover(credentials);
    };
  });

})();
