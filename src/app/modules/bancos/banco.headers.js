(function() {
    'use strict';

    var module = angular.module('singApp.bancos');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Bancos",
            "label_row": "Banco",
            "route": "bancos",
            "settings": {
                "add": true,
                "edit": true,
                "delete": true
            },
            "fields": [{
                    "name": "id",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "ID",
                    "editable": false,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "banco_code",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Código",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "banco_name",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Banco",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                }
            ]
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('bancos', _headers);
    }

})();