(function() {
    'use strict';

    var module = angular.module('singApp.indicadores', [
        'pascoal.crud'
    ]);

    // console.log('321');

    module.config(appConfig);

    module.controller('IndicadorEditController', function($scope, Restangular, $stateParams, $timeout, $modal, module, $state, $rootScope, $q, ngToast, $http, Upload, $controller) {
        $controller('CRUDEditController', { $scope: $scope, Restangular: Restangular, $stateParams: $stateParams, $timeout: $timeout, $modal: $modal, module: module, $state: $state, $rootScope: $rootScope, $q: $q, ngToast: ngToast, $http: $http, Upload: Upload }); //This works

        $scope.setVisibility = function() {
            var _control = ['_formula'];
            var _tipo = $scope.data.tipo;

            if ($scope.data.tipo != undefined && typeof $scope.data.tipo != 'number') {
                _tipo = $scope.data.tipo.id;
            }

            if (_tipo == undefined || _tipo == null) {
                var show = false;
                $scope.headers.masterdetails.resultados.visible = false;
            }
            else if (_tipo != undefined && [1].indexOf(_tipo) != -1) { // number
                var show = false;
                $scope.headers.masterdetails.resultados.visible = true;
                $scope.headers.masterdetails.alternativas.visible = false;
            }
            else if (_tipo != undefined && [ 3].indexOf(_tipo) != -1) { // resultado
                var show = true;
                $scope.headers.masterdetails.resultados.visible = true;
                $scope.headers.masterdetails.alternativas.visible = false;
            } else if (_tipo != undefined && [4, 5, 6].indexOf(_tipo) != -1) { // alternativa, lista e multipla escolha
                var show = false;
                $scope.headers.masterdetails.resultados.visible = false;
                $scope.headers.masterdetails.alternativas.visible = true;
            } else {
                var show = false;
                $scope.headers.masterdetails.resultados.visible = false;
                $scope.headers.masterdetails.alternativas.visible = false;
            }

            for (var idx in $scope.$parent.headers.fields) {
                if (_control.indexOf($scope.$parent.headers.fields[idx].name) != -1) {
                    // console.log($scope.$parent.headers.fields[idx].name);
                    $scope.$parent.headers.fields[idx].editable = show;
                    if (show == false) {
                        $scope.$parent.headers.fields[idx].notnull = false;
                        $scope.headers.fields[idx].notnull = false;
                        // debugger;
                    }

                    var _field = $scope.$parent.headers.fields[idx];

                    // force update - two-way data bind not working
                    // $timeout(function() {
                    //     angular.element('#_formula').scope().field = _field;
                    //     $scope.$apply();
                    // }, 200);
                }
            }



        };

        $scope.$watch('data.alternativas', function(_new, _old) {
            var __new = [];
            _.each(_new, function(val, idx) {
                if (val.chave !== null && val.legenda !== null && val.cor !== null) {
                    __new.push({ chave: val.chave, legenda: val.legenda, cor: val.cor });
                }
            })

            $scope.data.formula = JSON.stringify({ alternativas: __new });
            console.log('formula', $scope.data.formula)
        }, true);

        $scope.$watch('data._formula', function(_new, _old) {
            $scope.data.formula = JSON.stringify({ formula: $scope.data._formula });
            console.log('formula', $scope.data.formula)
        }, true);

        $scope.$watch('data.tipo', function(_new, _old) {
            if ($scope.dataLoaded) {
                $scope.setVisibility();
            }

        })


        $scope.$on('data-loaded', function() {
            $scope.setVisibility();
            var _tipo = $scope.data.tipo.id;

            if ($scope.data.formula != null) {
                if ([4, 5, 6].indexOf(_tipo) != -1) {
                    $scope.data.alternativas = JSON.parse($scope.data.formula).alternativas;
                } else if ([3].indexOf(_tipo) != -1) {
                    $scope.data._formula = JSON.parse($scope.data.formula).formula;
                }
            }
        });
    });

    appConfig.$inject = ['$stateProvider'];

    function appConfig($stateProvider) {
        // console.log('tango312');
        $stateProvider
            .state('app.indicadores', {
                abstract: true,
                url: '/indicadores',
                templateUrl: 'app/modules/core/utils/crud/crud.html',
                controller: 'CRUDController',
                resolve: {
                    id: ['$stateParams', function($stateParams) {
                        return $stateParams.id;
                    }],
                    module: function() {
                        return 'indicadores';
                    }
                }
            })
            .state('app.indicadores.list', {
                url: '',
                templateUrl: 'app/modules/core/utils/crud/crud.list.html'
            })
            .state('app.indicadores.new', {
                url: '/new',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'IndicadorEditController'
            })
            .state('app.indicadores.edit', {
                url: '/:id/edit',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'IndicadorEditController'
            })
    };
})();
