(function() {
    'use strict';

    var module = angular.module('singApp.indicadores');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Indicadores",
            "label_row": "Indicador",
            "route": "indicadores",
            "settings": {
                "add": true,
                "edit": true,
                "delete": true
            },
            "fields": [{
                    "name": "id",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "ID",
                    "editable": false,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "codigo",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Código",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "descricao",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Descrição",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "tipo",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Tipo",
                    "editable": true,
                    "viewable": false,
                    "autocomplete": true,
                    "quickAdd": false,
                    "autocomplete_dependencies": [],
                    "customOptions": { "select": true }
                },
                {
                    "name": "_formula",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Fórmula",
                    "editable": true,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "unidade_medida",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Unidade de Medida",
                    "editable": true,
                    "viewable": false,
                    "autocomplete": true,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },

                {
                    "name": "formula",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Fórmula",
                    "editable": false,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "situacao",
                    "type": "boolean",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Situação",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": {
                      "statusTrueText": "Ativo",
                      "statusFalseText": "Inativo",
                      "default": true
                    }
                },
                {
                    "name": "exame",
                    "type": "boolean",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Exame",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": {
                      "statusTrueText": "Sim",
                      "statusFalseText": "Não"
                    }
                }
            ],
            "masterdetails": {
                "alternativas": {
                    "visible": false,
                    "label": "Alternativas",
                    "label_row": "Alternativa",
                    "label_stem": "alternativas",
                    "fields": [
                        { "name": "chave", "type": "float", "notnull": true, "length": null, "precision": 10, "label": "Indicador", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { min: 0 } },
                        { "name": "legenda", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Legenda", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "cor", "type": "simplecolor", "notnull": false, "length": null, "precision": 10, "label": "Sinalização", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { colors: ['green', 'yellow', 'orange', 'red'] } },
                    ],
                    "masterdetails": []
                },
                "resultados": {
                    "visible": true,
                    "label": "Tabelas de Resultados",
                    "label_row": "Faixa de Resultado",
                    "label_stem": "resultados",
                    "fields": [
                        { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "indice_min", "type": "float", "notnull": true, "length": null, "precision": 10, "label": "Faixa Início", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { min: 0 } },
                        { "name": "indice_max", "type": "float", "notnull": true, "length": null, "precision": 10, "label": "Faixa Fim", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "resultado", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Resultado", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "cor", "type": "simplecolor", "notnull": false, "length": null, "precision": 10, "label": "Sinalização", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { colors: ['green', 'yellow', 'orange', 'red'] } },
                    ],
                    "masterdetails": []
                },
                "cids": {
                    "visible": true,
                    "label": "CIDs",
                    "label_row": "CID",
                    "label_stem": "cids",
                    "fields": [
                        { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "cid", "type": "integer", "notnull": false, "length": 50, "precision": 100, "label": "CID", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                    ],
                    "masterdetails": [],
                    "route": "cids",
                }
            }
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('indicadores', _headers);
    }

})();
