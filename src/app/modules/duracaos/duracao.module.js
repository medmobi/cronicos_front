(function() {
  'use strict';

  var module = angular.module('singApp.duracaos', [
    'pascoal.crud'
  ]);

  // console.log('321');

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    // console.log('tango312');
    $stateProvider
      .state('app.duracaos', {
        abstract: true,
        url: '/duracaos',
        templateUrl: 'app/modules/core/utils/crud/crud.html',
        controller: 'CRUDController',
        resolve:{
          id: ['$stateParams', function($stateParams){
            return $stateParams.id;
          }],
          module: function () {
            return 'duracaos';
          }
        }
      })
      .state('app.duracaos.list', {
        url: '',
        templateUrl: 'app/modules/core/utils/crud/crud.list.html'
      })
      .state('app.duracaos.new', {
        url: '/new',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
      .state('app.duracaos.edit', {
        url: '/:id/edit',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
  };
})();
