(function(){
    'use strict';

    var module = angular.module('singApp.vias');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Vias",
            "label_row": "Vias",
            "route": "vias",
            "settings": {
                "add": true,
                "edit": true,
                "delete": true
            },
            "fields": [
                {
                    "name": "id",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "ID",
                    "editable": false,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "nome",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Descrição",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                }
            ]
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('vias', _headers);
    }

})();
