(function() {
  'use strict';

  angular.module('singApp.components.nvd3')
    .directive('nvd3Chart', nvd3Chart)
    .factory('nv', nvFactory)
    .factory('d3', d3Factory)
  ;

  nvd3Chart.$inject = ['$window', 'jQuery', 'nv', 'd3', '$rootScope'];
  function nvd3Chart($window, jQuery, nv, d3, $rootScope){
    return {
      link: function (scope, $el, attrs) {
        function render(){
          nv.addGraph(function() {
            var chart = scope[attrs.chart];
            d3.select($el.find('svg')[0])
              .style('height', attrs.height || '300px')
              .datum(scope[attrs.datum])
              .transition().duration(500)
              .call(chart)
            ;

            jQuery($window).on('sn:resize', chart.update);
            chart.update();

            return chart;
          });
        }

        render();

        $rootScope.$on('update-chart', function (event, args) {
            var chart = scope[attrs.chart];
            d3.select($el.find('svg')[0])
              .style('height', attrs.height || '300px')
              .datum(scope[attrs.datum])
              .transition().duration(500)
              .call(chart)
            ;

            if (args.type === 'filter') chart.update();
            else {
                setTimeout(function () {
                    $window.dispatchEvent(new Event('resize'));
                }, 100);
            }
        });
      }
    }
  }

  nvFactory.$inject = ['$window'];
  function nvFactory($window) {
    return $window.nv;
  }

  d3Factory.$inject = ['$window'];
  function d3Factory($window) {
    return $window.d3;
  }



})();
