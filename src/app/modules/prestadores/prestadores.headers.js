(function() {
    'use strict';

    var module = angular.module('singApp.prestadores');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Prestadores",
            "label_row": "Prestador",
            "route": "prestadores",
            "fixed_tab": true,
            "settings": {
                "add": true,
                "edit": true,
                "delete": true
            },
            "fields": [
                { "tab": 'fixed', "name": "id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },


                { "tab": 'fixed', "name": "nome", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Nome", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },

                { "tab": 'fixed', "name": "conselho", "type": "string", "notnull": false, "length": 50, "precision": 10, "label": "Conselho", "editable": true, "viewable": false, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 'fixed', "name": "conselho_uf", "type": "integer", "notnull": false, "length": 10, "precision": 10, "label": "Conselho UF", "editable": true, "viewable": false, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 'fixed', "name": "conselho_numero", "type": "string", "notnull": false, "length": 50, "precision": 10, "label": "Conselho Número", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },

                { "tab": 'fixed', "name": "type", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Tipo", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "enum": { "F": "Física", "PJ": "Jurídica" } } },


                {
                    "name": "conselho_concat",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Conselho",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function(rawData, model) {
                        // debugger;
                        // var _label = [];

                        var out = '';

                        if (model.attributes.conselho != undefined && model.attributes.conselho_uf != undefined) {
                            out = model.attributes.conselho.nome + '/' + model.attributes.conselho_uf.nome;

                            if (model.attributes.conselho_numero != undefined && model.attributes.conselho_numero != null) {
                                out += ' ' + model.attributes.conselho_numero;
                            }
                        }
                        return out;

                        // return _label.join(', ');
                    }
                },
                {
                    "name": "especialidades",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Especialidades",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function(rawData, model) {
                        // debugger;
                        var _label = [];
                        model.attributes.especialidades.forEach(function(val, key) {
                          if (val.especialidade) {
                            _label.push(val.especialidade.descricao)
                          }
                        });

                        return _label.join(' / ');
                    }
                },


                { "tab": 'fixed', "name": "ativo", "type": "boolean", "notnull": false, "length": 50, "precision": 10, "label": "Situação", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": {"statusTrueText": "Ativo", "statusFalseText": "Inativo", "default": true} },

                { "tab": 'fixed', "name": "email", "type": "string", "notnull": false, "length": 255, "precision": 10, "label": "E-mail", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },


                { "tab": 0, "name": "rg", "type": "string", "notnull": false, "length": 10, "precision": 10, "label": "RG", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 0, "name": "orgao_emissor", "type": "string", "notnull": false, "length": 10, "precision": 10, "label": "Org\u00e3o Emissor", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 0, "name": "cpf", "type": "string", "notnull": false, "length": 20, "precision": 10, "label": "CPF", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "cpf": [] } },
                { "tab": 0, "name": "cnpj", "type": "string", "notnull": false, "length": 20, "precision": 10, "label": "CNPJ", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "cnpj": [] } },
                { "tab": 0, "name": "data_nascimento", "type": "date", "notnull": false, "length": null, "precision": 10, "label": "Data de Nascimento", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 0, "name": "sexo", "type": "string", "notnull": false, "length": 100, "precision": 10, "label": "Sexo", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "enum": { "m": "Masculino", "f": "Feminino", "o": "Indefinido" } } },


                { "tab": 1, "name": "inss", "type": "string", "notnull": false, "length": 50, "precision": 10, "label": "INSS", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 1, "name": "issqn", "type": "string", "notnull": false, "length": 50, "precision": 10, "label": "ISSQN", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 1, "name": "pis_pasep", "type": "string", "notnull": false, "length": 50, "precision": 10, "label": "PIS/PASEP", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 1, "name": "cns", "type": "string", "notnull": false, "length": 50, "precision": 10, "label": "CNS", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 1, "name": "instituicao_ensino_superior", "type": "string", "notnull": false, "length": 50, "precision": 10, "label": "Instituição de Ensino Superior", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },

                { "tab": 1, "name": "obs", "type": "text", "notnull": false, "length": 50, "precision": 10, "label": "Observações", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },

            ],
            "main_tabs": [
                // "Gerais",f
                "Dados Cadastrais",
                "Mais Informações"
            ],
            "masterdetails": {

            },
            "tabs_session": {
                "enderecos": {
                    "label": "Endere\u00e7os",
                    "label_row": "Endere\u00e7o",
                    "label_stem": "enderecos",
                    "fields": [
                        { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.padd_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "pes_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "cep", "type": "string", "notnull": true, "length": 5, "precision": 10, "label": "CEP", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "cep": { "address": "logradouro", "city": "cidade", "district": "bairro", "state": "estado" } } },
                        { "name": "logradouro", "type": "string", "notnull": true, "length": 20, "precision": 10, "label": "Endere\u00e7o", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "numero", "type": "string", "notnull": true, "length": 3, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "complemento", "type": "string", "notnull": false, "length": 7, "precision": 10, "label": "Complemento", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        // { "name": "quadra", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Quadra", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        // { "name": "lote", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Lote", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "bairro", "type": "string", "notnull": true, "length": 7, "precision": 10, "label": "Bairro", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "cidade", "type": "string", "notnull": true, "length": 10, "precision": 10, "label": "Cidade", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "estado", "type": "string", "notnull": true, "length": 1, "precision": 10, "label": "Estado", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },


                    ],
                    "masterdetails": []
                },
                "telefones": {
                    "label": "Telefones",
                    "label_row": "Telefone",
                    "label_stem": "telefones",
                    "fields": [
                        { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "pres_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "tipo_telefone", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Tipo de Telefone", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "numero", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "telefone": true } },
                        { "name": "contato", "type": "string", "notnull": false, "length": 100, "precision": 10, "label": "Nome do Contato", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
                    ],
                    "masterdetails": [],
                    "route": "telefones"
                },
                "especialidades": {
                    "label": "Especialidades",
                    "label_row": "Especialidade",
                    "label_stem": "especialidades",
                    "fields": [
                        { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "pres_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "especialidade", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Especialidade", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },

                    ],
                    "masterdetails": [],
                    "route": "especialidades"
                },
                "bancos": {
                    "label": "Contas Bancárias",
                    "label_row": "Conta Bancária",
                    "label_stem": "bancos",
                    "fields": [
                        { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "pres_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "banco", "type": "integer", "notnull": true, "length": 100, "precision": 10, "label": "Banco", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "agencia", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Agência", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "conta", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "C/C", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
                    ],
                    "masterdetails": [],
                    "route": "bancos"
                }
            },
            "tabs": {
                // "bancos": {
                //     "label": "Contas Bancárias",
                //     "label_row": "Conta Bancária",
                //     "route": "/details/bancos",
                //     "fields": [
                //         { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "pres_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "banco", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Banco", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "agencia", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "Agência", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "conta", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "C/C", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
                //     ],
                //     "masterdetails": []
                // }
            }
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('prestadores', _headers);
    }

})();
