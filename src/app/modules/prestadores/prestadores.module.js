(function() {
    'use strict';

    var module = angular.module('singApp.prestadores', [
        'pascoal.crud'
    ]);

    // console.log('321');

    module.config(appConfig);

    module.controller('PrestadorController', function($scope, Restangular, $stateParams, $timeout, $modal, module, $state, $rootScope, $q, ngToast, $http, Upload, $controller) {
        $controller('CRUDEditController', { $scope: $scope, Restangular: Restangular, $stateParams: $stateParams, $timeout: $timeout, $modal: $modal, module: module, $state: $state, $rootScope: $rootScope, $q: $q, ngToast: ngToast, $http: $http, Upload: Upload }); //This works

        $scope.$watch('data.type', function (_new, _old) {
            var _cpfEl = document.getElementById('cpf');
            var _cnpjEl = document.getElementById('cnpj');
            if (_new === 'PJ') {
                if (_cpfEl) _cpfEl.setAttribute('disabled', true);
                if (_cnpjEl) _cnpjEl.removeAttribute('disabled');
            } else if (_new === 'F') {
                if (_cnpjEl) _cnpjEl.setAttribute('disabled', true);
                if (_cpfEl) _cpfEl.removeAttribute('disabled');
            }
        });
    });

    appConfig.$inject = ['$stateProvider'];

    function appConfig($stateProvider) {
        // console.log('tango312');
        $stateProvider
            .state('app.prestadores', {
                abstract: true,
                url: '/prestadores',
                templateUrl: 'app/modules/core/utils/crud/crud.html',
                controller: 'CRUDController',
                resolve: {
                    id: ['$stateParams', function($stateParams) {
                        return $stateParams.id;
                    }],
                    module: function() {
                        return 'prestadores';
                    }
                }
            })
            .state('app.prestadores.list', {
                url: '',
                templateUrl: 'app/modules/core/utils/crud/crud.list.html'
            })
            .state('app.prestadores.new', {
                url: '/new',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'PrestadorController'
            })
            .state('app.prestadores.edit', {
                url: '/:id/edit',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'PrestadorController'
            });
    }
})();
