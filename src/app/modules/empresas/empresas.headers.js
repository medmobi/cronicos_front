(function() {
    'use strict';

    var module = angular.module('singApp.empresas');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Empresas",
            "label_row": "Empresa",
            "route": "empresas",
            "fixed_tab": true,
            "settings": {
                "add": true,
                "edit": true,
                "delete": true
            },
            "fields": [
                { "tab": 'fixed', "name": "id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 'fixed', "name": "nome", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Nome", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 'fixed', "name": "cnpj", "type": "string", "notnull": false, "length": 100, "precision": 10, "label": "CNPJ", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "cnpj": [] } },
                { "tab": 'fixed', "name": "email", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "E-mail", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 'fixed', "name": "observacao", "type": "text", "notnull": false, "length": 255, "precision": 10, "label": "Observação", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": "fixed", "name": "logo", "type": "string", "notnull": false, "length": 100, "precision" :10, "label": "Foto", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "file": { "container": "logos" }, "only_upload": true } },
                {
                    "name": "cidade",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Cidade",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function(rawData, model) {
                        if (model.attributes.enderecos[0] != undefined) {
                            return model.attributes.enderecos[0].cidade + ' - ' + model.attributes.enderecos[0].estado;
                        } else {}
                    }
                },
            ],
            "masterdetails": {

            },
            "main_tabs": [
                // "Gerais",f
                // "Dados Cadastrais",
                // "Mais Informações"
            ],
            "tabs_session": {
                "enderecos": {
                    "label": "Endere\u00e7os",
                    "label_row": "Endere\u00e7o",
                    "label_stem": "enderecos",
                    "fields": [
                        { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.padd_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "pes_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "cep", "type": "string", "notnull": true, "length": 5, "precision": 10, "label": "CEP", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "cep": { "address": "logradouro", "city": "cidade", "district": "bairro", "state": "estado" } } },
                        { "name": "logradouro", "type": "string", "notnull": true, "length": 20, "precision": 10, "label": "Endere\u00e7o", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "numero", "type": "string", "notnull": true, "length": 3, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "complemento", "type": "string", "notnull": false, "length": 7, "precision": 10, "label": "Complemento", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                       // { "name": "quadra", "type": "string", "notnull": false, "length": 100, "precision": 10, "label": "Quadra", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                       // { "name": "lote", "type": "string", "notnull": false, "length": 100, "precision": 10, "label": "Lote", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "bairro", "type": "string", "notnull": true, "length": 7, "precision": 10, "label": "Bairro", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "cidade", "type": "string", "notnull": true, "length": 10, "precision": 10, "label": "Cidade", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "estado", "type": "string", "notnull": true, "length": 1, "precision": 10, "label": "Estado", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }

                    ],
                    "masterdetails": []
                },
                "telefones": {
                    "label": "Telefones",
                    "label_row": "Telefone",
                    "label_stem": "telefones",
                    "fields": [
                        // { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        // { "name": "pes_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "tipo_telefone", "type": "integer", "notnull": true, "length": 30, "precision": 10, "label": "Tipo de Telefone", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "numero", "type": "string", "notnull": true, "length": 60, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "telefone": true } },
                        { "name": "contato", "type": "string", "notnull": false, "length": 60, "precision": 10, "label": "Nome do Contato", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
                    ],
                    "masterdetails": [],
                    "route": "telefones"
                }
            },
            "tabs": {
                // "telefones": {
                //     "name": "telefones",
                //     "label": "Telefones",
                //     "label_row": "Telefone",
                //     "route": "/details/telefones",
                //     "fields": [
                //         { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         // { "name": "pac_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "tipo_telefone", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Tipo de Telefone", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "numero", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "telefone": true } }
                //     ],
                //     "masterdetails": []
                // },
                // "enderecos": {
                //     "label": "Endere\u00e7os",
                //     "label_row": "Endere\u00e7o",
                //     "route": "/details/enderecos",
                //     "fields": [
                //         { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.padd_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         // { "name": "pac_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "cep", "type": "string", "notnull": true, "length": 10, "precision": 10, "label": "CEP", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "cep": { "address": "logradouro", "city": "cidade", "district": "bairro", "state": "estado" } } },
                //         { "name": "logradouro", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "Endere\u00e7o", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "numero", "type": "string", "notnull": true, "length": 5, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "complemento", "type": "string", "notnull": false, "length": 255, "precision": 10, "label": "Complemento", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "quadra", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Quadra", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "lote", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Lote", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "bairro", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Bairro", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "cidade", "type": "string", "notnull": true, "length": 60, "precision": 10, "label": "Cidade", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                //         { "name": "estado", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Estado", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
                //     ],
                //     "masterdetails": []
                // }
            }
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('empresas', _headers);
    }

})();
