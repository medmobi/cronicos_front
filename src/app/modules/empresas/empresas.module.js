(function() {
    'use strict';

    var module = angular.module('singApp.empresas', [
        'pascoal.crud'
    ]);

    // console.log('321');

    module.config(appConfig);

    appConfig.$inject = ['$stateProvider'];

    module.controller('EmpresaEditController', function ($scope, Restangular, $stateParams, $timeout, $modal, module, $state, $rootScope, $q, ngToast, $http, Upload, $controller) {
      $controller('CRUDEditController', {
        $scope: $scope,
        Restangular: Restangular,
        $stateParams: $stateParams,
        $timeout: $timeout,
        $modal: $modal,
        module: module,
        $state: $state,
        $rootScope: $rootScope,
        $q: $q,
        ngToast: ngToast,
        $http: $http,
        Upload: Upload
      }); //This works

      $scope.customFixedTabHTML = 'app/modules/empresas/logo.html';

    });

    function appConfig($stateProvider) {
        // console.log('tango312');
        $stateProvider
            .state('app.empresas', {
                abstract: true,
                url: '/empresas',
                templateUrl: 'app/modules/core/utils/crud/crud.html',
                controller: 'CRUDController',
                resolve: {
                    id: ['$stateParams', function($stateParams) {
                        return $stateParams.id;
                    }],
                    module: function() {
                        return 'empresas';
                    }
                }
            })
            .state('app.empresas.list', {
                url: '',
                templateUrl: 'app/modules/core/utils/crud/crud.list.html'
            })
            .state('app.empresas.new', {
                url: '/new',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'EmpresaEditController'
            })
            .state('app.empresas.edit', {
                url: '/:id/edit',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'EmpresaEditController'
            });
    }
})();
