(function() {
    'use strict';

    var module = angular.module('singApp.feriados');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Feriados",
            "label_row": "Feriados",
            "route": "feriados",
            "settings": {
                "add": true,
                "edit": true,
                "delete": true
            },
            "fields": [{
                    "name": "id",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "ID",
                    "editable": false,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "data",
                    "type": "date",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Data",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "dia",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Dia",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],

                    "toString": function(rawData, model) {
                        // debugger;
                        var _label = [];
                        // model.attributes.data_nascimento;

                        return moment(new Date(model.attributes.data)).format('ddd');

                    }
                },
                {
                    "name": "nome",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Nome",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "tipoFeriado",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Tipo",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": true,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": { "select": true }
                },
                {
                    "name": "expediente",
                    "type": "boolean",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Expediente",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": {
                      "statusTrueText": "Sim",
                      "statusFalseText": "Não"
                    }
                }
            ]
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('feriados', _headers);
    }

})();
