(function() {
    'use strict';

    var module = angular.module('singApp.feriados', [
        'pascoal.crud'
    ]);

    // console.log('321');

    module.config(appConfig);

    appConfig.$inject = ['$stateProvider'];

    function appConfig($stateProvider) {
        // console.log('tango312');
        $stateProvider
            .state('app.feriados', {
                abstract: true,
                url: '/feriados',
                templateUrl: 'app/modules/core/utils/crud/crud.html',
                controller: 'CRUDController',
                resolve: {
                    id: ['$stateParams', function($stateParams) {
                        return $stateParams.id;
                    }],
                    module: function() {
                        return 'feriados';
                    }
                }
            })
            .state('app.feriados.list', {
                url: '',
                templateUrl: 'app/modules/core/utils/crud/crud.list.html'
            })
            .state('app.feriados.new', {
                url: '/new',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'CRUDEditController'
            })
            .state('app.feriados.edit', {
                url: '/:id/edit',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'CRUDEditController'
            })
    };
})();