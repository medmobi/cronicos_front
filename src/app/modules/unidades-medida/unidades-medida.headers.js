(function() {
    'use strict';

    var module = angular.module('singApp.unidades-medida');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Unidades de Medida",
            "label_row": "Unidade de Medida",
            "route": "unidades-medida",
            "settings": {
                "add": true,
                "edit": true,
                "delete": true
            },
            "fields": [{
                    "name": "id",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "ID",
                    "editable": false,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "nome",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Nome",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "sigla",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Sigla",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                }
            ]
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('unidades-medida', _headers);
    }

})();