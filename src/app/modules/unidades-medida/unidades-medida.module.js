(function() {
    'use strict';

    var module = angular.module('singApp.unidades-medida', [
        'pascoal.crud'
    ]);

    // console.log('321');

    module.config(appConfig);

    appConfig.$inject = ['$stateProvider'];

    function appConfig($stateProvider) {
        // console.log('tango312');
        $stateProvider
            .state('app.unidades-medida', {
                abstract: true,
                url: '/unidades-medida',
                templateUrl: 'app/modules/core/utils/crud/crud.html',
                controller: 'CRUDController',
                resolve: {
                    id: ['$stateParams', function($stateParams) {
                        return $stateParams.id;
                    }],
                    module: function() {
                        return 'unidades-medida';
                    }
                }
            })
            .state('app.unidades-medida.list', {
                url: '',
                templateUrl: 'app/modules/core/utils/crud/crud.list.html'
            })
            .state('app.unidades-medida.new', {
                url: '/new',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'CRUDEditController'
            })
            .state('app.unidades-medida.edit', {
                url: '/:id/edit',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'CRUDEditController'
            })
    };
})();