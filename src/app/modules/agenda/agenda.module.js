

(function() {
  'use strict';

  var module = angular.module('singApp.agenda', [
    'ui.router',
    'ui.bootstrap',
    'ui.calendar'
  ]);

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    $stateProvider
      .state('app.agenda', {
        url: '/agenda',
        templateUrl: 'app/modules/agenda/agenda.html',
        controller: 'CalendarAppController as vm',
        resolve: {
          user: function (auth) {
            return auth.getUser();
          },
          prestadores: function (Prestador, auth) {
              var user = auth.getUser();
              if (user !== null && user.emp_id && user.admin) {
                return Prestador.find({
                  filter: {
                    where: {
                      emp_id: user.emp_id
                    }
                  }
                })
                .$promise
                .then(function (prestadores) {
                  var _prestadores = [];
                  prestadores.forEach(function (prestador) {
                    _prestadores.push({ id: prestador.id, nome: prestador.nome, checked: true });
                  });
                  return _prestadores;
                });
              } else return [];
          },
          pacientes: function (Paciente, auth) {
            var user = auth.getUser();
            if (user !== null && user.emp_id && user.admin) {
              return Paciente.find({
                  filter: {
                    where: {
                      emp_id: user.emp_id
                    }
                  }
                })
                .$promise
                .then(function (pacientes) {
                  var _pacientes = [];
                  pacientes.forEach(function (paciente) {
                    _pacientes.push({ id: paciente.id, nome: paciente.nome, checked: false });
                  });
                  return _pacientes;
                });
            } else return [];
          },
          empresa: function (Empresa, auth) {
            var user = auth.getUser();
            return Empresa.findOne({
              filter: {
                where: {
                  id: user.emp_id
                }
              }
            })
            .$promise
            .then(function (empresa) {
              return empresa;
            });
          }
        }
      });
  }
})();
