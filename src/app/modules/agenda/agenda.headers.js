(function() {
  'use strict';

  var module = angular.module('singApp.agenda');

  module.config(cfg);

  function cfg(headersProvider) {
    var _headers = {
      "label": "Eventos",
      "label_row": "Evento",
      "route": "eventos",
      "settings": {
        "add": true,
        "edit": true,
        "delete": true
      },
      "fields": [
        { "name": "id", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "title", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Título", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "paciente", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Paciente", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "prestador", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Prestador", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "atendimento", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Atendimento", "editable": false, "viewable": false, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "tipo", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Tipo", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "status", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Status", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "start", "type": "date", "notnull": true, "length": 50, "precision": 10, "label": "Início", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "startTime", "type": "time", "notnull": true, "length": 50, "precision": 10, "label": "Horário de Início", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "end", "type": "date", "notnull": true, "length": 50, "precision": 10, "label": "Fim", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "endTime", "type": "time", "notnull": true, "length": 50, "precision": 10, "label": "Horário de Fim", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "backgroundColor", "type": "simplecolor", "notnull": true, "length": null, "precision": 10, "label": "Cor de Fundo", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { colors: ['green', 'red', 'orange', 'black'] } },
        { "name": "description", "type": "text", "notnull": false, "length": 255, "precision": 10, "label": "Observação", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "rows": 5 } },
        { "name": "allDay", "type": "boolean", "notnull": false, "length": 20, "precision": 10, "label": "Dia Inteiro", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "statusTrueText": "Sim", "statusFalseText": "Não" } },
        { "name": "arrival", "type": "date", "notnull": false, "length": 50, "precision": 10, "label": "Chegada", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "arrivalTime", "type": "time", "notnull": false, "length": 50, "precision": 10, "label": "Horário de Chegada", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
      ],
      "buttons": [
        { "name": "atendimento", "label": "Novo Atendimento", "viewable": true, "icon": "fa-plus", "method": "startAtendimento" }
      ]
    };

    headersProvider.set('eventos', _headers);
  }

})();
