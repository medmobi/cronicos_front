(function() {
  'use strict';

  angular.module('singApp.agenda')
    .controller('CalendarAppController', CalendarAppController);
    // .controller('CreateEventModalInstanceCtrl', CreateEventModalInstanceCtrl)
    // .controller('ShowEventModalInstanceCtrl', ShowEventModalInstanceCtrl)
    // .factory('moment', momentService);

  CalendarAppController.$inject = ['$modal', 'uiCalendarConfig', 'jQuery', 'Evento', 'headers', 'fwModalService', 'Restangular', '$scope', 'Usuario', 'ngToast', 'user', 'prestadores', 'empresa', 'pacientes'];
  function CalendarAppController ($modal, uiCalendarConfig, jQuery, Evento, headers, fwModalService, Restangular, $scope, Usuario, ngToast, user, prestadores, empresa, pacientes) {
    var vm = this;

    vm.uiConfig = {
      calendar: {
        header: {
          left: '',
          center: '',
          right: ''
        },
        selectable: true,
        selectHelper: true,
        editable: true,
        droppable: false,
        resizable: false,
        height: 'auto',

        defaultView: 'agendaDay',
        timeFormat: 'HH:mm',
        axisFormat: 'HH:mm',
        columnFormat: {
          month: 'ddd',
          week : 'ddd, D',
          day  : 'dddd - D/MM'
        },
        businessHours: {
          dow: [0,1,2,3,4,5,6],
          start: "00:00",
          end: "23:59"
        },
        slotLabelInterval : '00:15:00',
        slotDuration: '00:30:00',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        allDayText: 'DIA',

        select: function (start, end) {
          var _event = {
            start: start.format('YYYY-MM-DD[T]HH:mm:ss'),
            startTime: start.format('YYYY-MM-DD[T]HH:mm:ss'),
            end: end.format('YYYY-MM-DD[T]HH:mm:ss'),
            endTime: end.format('YYYY-MM-DD[T]HH:mm:ss'),
            arrivalTime: _moment().startOf('day'),
            allDay: allDay(start, end)
          };

          openEvent(_event);
          // var modal = $modal.open({
          //   templateUrl: 'create-event-modal.html',
          //   controller: 'CreateEventModalInstanceCtrl as vm',
          //   size: 'sm',
          //   resolve: {
          //     event: function () {
          //       return {
          //         start: start,
          //         end: end,
          //         allDay: allDay
          //       }
          //     }
          //   }
          // });
          //
          // modal.result.then(vm.addEvent, angular.noop);
        },

        eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) { // this function is called when something is dropped
          if (vm.uiConfig.calendar.droppable) {
            // retrieve the dropped element's stored Event Object
            var originalEventObject = {
              title: jQuery.trim(jQuery(event.target).text()) // use the element's text as the event title
            };

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = jQuery.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = !date.hasTime();

            var $categoryClass = jQuery(event.target).data('event-class');
            if ($categoryClass)
              copiedEventObject['className'] = [$categoryClass];

            // render the event on the calendar
            // the last `true` argument determines if the event 'sticks' (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            uiCalendarConfig.calendars.eventsCalendar.fullCalendar('renderEvent', copiedEventObject, true);

            jQuery(event.target).remove();
          }
          else revertFunc();
        },
        eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
          if (vm.uiConfig.calendar.resizable) {

          }
          else revertFunc();
        },
        eventClick: function (event) {
          // opens events in a popup window
          openEvent(event);
          // $modal.open({
          //   templateUrl: 'show-event-modal.html',
          //   controller: 'ShowEventModalInstanceCtrl as vm',
          //   size: 'sm',
          //   resolve: {
          //     event: function () {
          //       return event;
          //     }
          //   }
          // })
        },

        viewRender: function (view) {
          vm.calendarView = view;
          vm.calendar = vm.calendarView.calendar;
          vm.currentDay = vm.calendar.getDate().format('dddd');
          vm.currentDayNumber = vm.calendar.getDate().format('DD/MM/YYYY');
          if (view.name === 'agendaDay') {
            vm.view = 'Dia';
            vm.viewDay = true;
            vm.currentMainDate = vm.calendar.getDate().format('dddd') + ', ' + vm.calendar.getDate().format('DD/MM/YYYY');
          } else {
            vm.view = 'Semana';
            var startWeekDay = vm.calendar.getDate().startOf('week');
            vm.currentMainDate = startWeekDay.format('DD/MM/YY') + '-' + startWeekDay.add(6, 'day').format('DD/MM/YY');
          }
        }
      }
    };

    vm.smallCalendarConfig = {
      calendar: {
        header: false,
        selectable: true,
        selectHelper: true,
        editable: false,
        droppable: false,
        height: 'auto',

        select: function (start, end, allDay) {
          vm.calendarView.calendar.gotoDate(start);
        },
        viewRender: function (view) {
          vm.calendarSmallView = view;
          vm.currentPreviewMonth = vm.calendarSmallView.calendar.getDate().format('MMMM') + ' ' + vm.calendarSmallView.calendar.getDate().format('YYYY');
        }
      }
    };

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    // vm.events = [
    //   {
    //     title: 'Acompanhamento Holter',
    //     start: new Date(y, m, 1),
    //     backgroundColor: '#79A5F0',
    //     textColor: '#fff',
    //     description: 'Acompanhamento 24h'
    //   },
    //   {
    //     title: 'Cirurgia',
    //     backgroundColor: '#79A5F0',
    //     textColor: '#fff',
    //     start: new Date(y, m, d + 5),
    //     end: new Date(y, m, d + 7),
    //     description: 'Cirurgia longa'
    //   },
    //   {
    //     id: 999,
    //     title: 'Consulta - Dr. Carlos',
    //     backgroundColor: '#79A5F0',
    //     textColor: '#fff',
    //     start: new Date(y, m, d - 3, 16, 0),
    //     allDay: false,
    //     description: 'Consulta com exame'
    //   },
    //   {
    //     id: 1000,
    //     title: 'Consulta - Dra. Maria',
    //     start: new Date(y, m, d + 3, 12, 0),
    //     allDay: false,
    //     backgroundColor: '#555',
    //     textColor: '#fff',
    //     description: 'Consulta de retorno'
    //   },
    //   {
    //     title: 'Exame Cardiológico',
    //     start: new Date(y, m, d + 18, 12, 0),
    //     end: new Date(y, m, d + 18, 13, 0),
    //     backgroundColor: '#79A5F0',
    //     textColor: '#fff',
    //     description: 'Eletrocardiograma intenso'
    //   }
    // ];
    vm.events = [];
    vm.eventsCopy = [];
    vm.eventSources = [];
    vm.prestadores = prestadores;
    vm.pacientes = pacientes;
    vm.selectedPaciente = '';
    vm.selectedPrestador = '';
    vm.admin = true;
    vm.empresa = empresa;
    vm.view = 'Dia';
    vm.viewDay = true;

    vm.addEvent        = addEvent;
    vm.changeView      = changeView;
    vm.prev            = prev;
    vm.next            = next;
    vm.filterPrestador = filterPrestador;
    vm.filterPaciente  = filterPaciente;
    vm.newEvent        = newEvent;

    ////////

    init();

    function init () {
        vm.headers = headers.get('eventos');

        $scope.resource = Restangular.all('eventos');

        var pres_id = null;

        if (user !== null && !user.admin) {
            pres_id = user.pres_id;
            vm.admin = false;
        }

        Evento.getEventosFromEmpresa()
            .$promise
            .then(function (eventos) {
                // Remove events that aren't from a specific pres_id (only prestadores that aren't admins)
                if (!vm.admin) {
                    var _eventos = [];
                    eventos.forEach(function (evento, index) {
                        if (evento.pres_id === pres_id) {
                            _convertEventDateTimes(evento);
                            _eventos.push(evento);
                        }
                    });
                    vm.events = Restangular.stripRestangular(_eventos);
                    vm.eventsCopy = Restangular.stripRestangular(_eventos);
                    vm.eventSources.slice(0, vm.eventSources.length);
                    vm.eventSources.push(vm.events);
                }
                // Separate events in different lists by each prestador
                else {
                    var _eventos = [];
                    // Add agendas list to _eventos according to vm.prestadores order
                    vm.prestadores.forEach(function () {
                        _eventos.push([]);
                    });
                    eventos.forEach(function (evento) {

                        evento = _convertEventDateTimes(evento);

                        vm.prestadores.forEach(function (_prestador, index) {
                            if (evento.pres_id === _prestador.id) {
                                _eventos[index].push(evento);
                            }
                        });
                    });
                    vm.events = Restangular.stripRestangular(_eventos);
                    vm.eventsCopy = Restangular.stripRestangular(_eventos);
                    vm.eventSources.slice(0, vm.eventSources.length);
                    // Push each prestador agenda to eventSources
                    vm.events.forEach(function (agenda) {
                        vm.eventSources.push(agenda);
                    });
                }

            });
    }
    function _convertEventDateTimes (event) {
        // event.start = _moment(evento.start).format('YYYY-MM-DD[T]HH:mm:ss');
        // event.end = _moment(evento.end).format('YYYY-MM-DD[T]HH:mm:ss');

        event.startTime = _moment(event.start).format('YYYY-MM-DD[T]HH:mm:ss');
        event.endTime = _moment(event.end).format('YYYY-MM-DD[T]HH:mm:ss');
        if (event.arrival) event.arrivalTime = _moment(event.arrival).format('YYYY-MM-DD[T]HH:mm:ss');
        else event.arrivalTime = _moment().startOf('day');

        return event;
    }
    function addEvent (event) {
      vm.events.push(event);
    }
    function changeView (view) {
      vm.calendarView.calendar.changeView(view);
    }
    function prev (type) {
      if(type === 'preview') vm.calendarSmallView.calendar.prev();
      else {
        if(_moment(vm.calendarView.calendar.getDate()).month() !== (_moment(vm.calendarView.calendar.getDate()).subtract(1, 'days')).month()) {
          vm.calendarSmallView.calendar.prev();
        }
        vm.calendarView.calendar.prev();
      }
    }
    function next (type) {
      if(type === 'preview') vm.calendarSmallView.calendar.next();
      else {
        if(_moment(vm.calendarView.calendar.getDate()).month() !== (_moment(vm.calendarView.calendar.getDate()).add(1, 'days')).month()) {
          vm.calendarSmallView.calendar.next();
        }
        vm.calendarView.calendar.next();
      }
    }
    function openEvent (event) {
        var edit = false;
        var _eventCopy = angular.copy(event);

        if (_eventCopy.id) {
            _eventCopy.start = _moment(_eventCopy.start).format('YYYY-MM-DD[T]HH:mm:ss');
            _eventCopy.startTime = _moment(_eventCopy.start).format('YYYY-MM-DD[T]HH:mm:ss');

            if (_eventCopy.arrival) {
                _eventCopy.arrival = _moment(_eventCopy.arrival).format('YYYY-MM-DD[T]HH:mm:ss');
                _eventCopy.arrivalTime = _moment(_eventCopy.arrival).format('YYYY-MM-DD[T]HH:mm:ss');
            }
            else _eventCopy.arrivalTime = _moment().startOf('day');

            // Fullcalendar has one issue that if start date and end date are same it simply makes end date as null.
            // @see: https://stackoverflow.com/questions/24596587/fullcalendar-return-event-end-null-when-allday-is-true
            if (!_eventCopy.end) {
                _eventCopy.endTime = _moment(_eventCopy.start).format('YYYY-MM-DD[T]HH:mm:ss');
                _eventCopy.end = _moment(_eventCopy.start).format('YYYY-MM-DD[T]HH:mm:ss');
            }
            else {
                _eventCopy.endTime = _eventCopy.end.format('YYYY-MM-DD[T]HH:mm:ss');
                _eventCopy.end = _moment(_eventCopy.end).format('YYYY-MM-DD[T]HH:mm:ss');
            }

            fwModalService.createCRUDModal(vm.headers, 'eventos', _eventCopy, false)
                .then(function (evento) {
                    var start, end;

                    if (evento.allDay) {
                        start = _moment(evento.start).format('YYYY-MM-DD') + ' 00:00:00';
                        end = _moment(evento.start).add(1, 'days').format('YYYY-MM-DD') + ' 00:00:00';
                    }
                    else {
                        start = _moment(evento.start).format('YYYY-MM-DD') + ' ' + _moment(evento.startTime).format('HH:mm:ss');
                        end = _moment(evento.end).format('YYYY-MM-DD') + ' ' + _moment(evento.endTime).format('HH:mm:ss');
                    }

                    evento.start = _moment(start).format('YYYY-MM-DD[T]HH:mm:ss');
                    evento.end = _moment(end).format('YYYY-MM-DD[T]HH:mm:ss');

                    if (evento.arrival && evento.arrivalTime) evento.arrival = _moment(evento.arrival).format('YYYY-MM-DD') + ' ' + _moment(evento.arrivalTime).format('HH:mm:ss');
                    else delete evento.arrival;

                    delete evento.startTime;
                    delete evento.endTime;
                    delete evento.arrivalTime;

                    if (typeof evento['tipo.label'] === 'object' && typeof evento['paciente.label'] === 'object') {
                        evento.title = evento['tipo.label'].label + ' - Paciente ' + evento['paciente.label'].label;
                    } else {
                        evento.title = evento['tipo.label'] + ' - Paciente ' + evento['paciente.label'];
                    }
                    switch(evento['status.label'].label) {
                        case 'Finalizado':
                            evento.backgroundColor = '#111111';
                            break;
                        case 'Agendado':
                            evento.backgroundColor = '#FF851B';
                            break;
                        case 'Cancelado':
                            evento.backgroundColor = '#FF4136';
                            break;
                        case 'Em Andamento':
                            evento.backgroundColor = '#3D9970';
                            break;
                        case 'Faltou':
                            evento.backgroundColor = '#85144b';
                            break;
                        case 'Aguardando':
                            evento.backgroundColor = '#001F3F';
                            break;
                    }

                    if (evento.id) {
                        edit = true;
                        delete evento.source;
                        return evento.$save();
                    }
                    else return Evento.create(evento).$promise;
                })
                .then(function (_evento) {
                    _updateEventList(_evento, edit);
                })
                .catch(function (err) {
                    // console.log('canceled modal: ' + err);
                });
        }
        else {
            if (user !== null) {
                Usuario.findOne({
                    filter: {
                        where: {
                            id: user.id
                        },
                        include: [
                            'prestador'
                        ]
                    }
                })
                .$promise
                .then(function (userPrestador) {
                    if (userPrestador.prestador) {
                        event.prestador = userPrestador.prestador.id;
                        event['prestador.label'] = { id: userPrestador.prestador.id, label: userPrestador.prestador.nome };
                        vm.headers.fields.forEach(function (field) {
                            if (field.name === 'prestador') field.readonly = true;
                        });
                    }
                    return fwModalService.createCRUDModal(vm.headers, 'eventos', event, false);
                })
                .then(function (evento) {
                    var start, end;

                    if (evento.allDay) {
                        start = _moment(evento.start).format('YYYY-MM-DD') + ' 00:00:00';
                        end = _moment(evento.start).add(1, 'days').format('YYYY-MM-DD') + ' 00:00:00';
                    }
                    else {
                        start = _moment(evento.start).format('YYYY-MM-DD') + ' ' + _moment(evento.startTime).format('HH:mm:ss');
                        end = _moment(evento.end).format('YYYY-MM-DD') + ' ' + _moment(evento.endTime).format('HH:mm:ss');
                    }

                    evento.start = _moment(start).format('YYYY-MM-DD[T]HH:mm:ss');
                    evento.end = _moment(end).format('YYYY-MM-DD[T]HH:mm:ss');

                    if (evento.arrival && evento.arrivalTime) evento.arrival = _moment(evento.arrival).format('YYYY-MM-DD') + ' ' + _moment(evento.arrivalTime).format('HH:mm:ss');
                    else delete evento.arrival;

                    delete evento.startTime;
                    delete evento.endTime;
                    delete evento.arrivalTime;

                    evento.title = evento['tipo.label'].label + ' - Paciente ' + evento['paciente.label'].label;
                    switch(evento['status.label'].label) {
                        case 'Finalizado':
                            evento.backgroundColor = '#111111';
                            break;
                        case 'Agendado':
                            evento.backgroundColor = '#FF851B';
                            break;
                        case 'Cancelado':
                            evento.backgroundColor = '#FF4136';
                            break;
                        case 'Em Andamento':
                            evento.backgroundColor = '#3D9970';
                            break;
                        case 'Faltou':
                            evento.backgroundColor = '#85144b';
                            break;
                        case 'Aguardando':
                            evento.backgroundColor = '#001F3F';
                            break;
                    }

                    if (evento.id) {
                        edit = true;
                        delete evento.source;
                        return evento.$save();
                    }
                    else return Evento.create(evento).$promise;
                })
                .then(function (_evento) {
                    _updateEventList(_evento, edit);
                });
            }
            else ngToast.warning('Nenhum usuário logado! :(');
        }
    }
    function filterPrestador (prestador, check) {
        vm.prestadores.forEach(function (_prestador) {
            if (prestador) {
                if (prestador.id === _prestador.id) {
                    _prestador.checked = check;
                } else {
                    _prestador.checked = false;
                }
            } else _prestador.checked = false;
        });
        vm.selectedPrestador = '';

        uiCalendarConfig.calendars.eventsCalendar.fullCalendar('removeEvents');
        uiCalendarConfig.calendars.eventsCalendar.fullCalendar('removeEventSource', vm.eventSources);
        // vm.prestadores.forEach(function (prestador, pIndex) {
        //     if (prestador.checked) {
        //         uiCalendarConfig.calendars.eventsCalendar.fullCalendar('addEventSource', vm.events[pIndex]);
        //     }
        // });
        vm.prestadores.forEach(function (_prestador, pIndex) {
            if (prestador.checked) {
                if (prestador.id === _prestador.id) {
                    _prestador.checked = true;
                    uiCalendarConfig.calendars.eventsCalendar.fullCalendar('addEventSource', vm.events[pIndex]);
                } else {
                    _prestador.checked = false;
                }
            } else {
                _prestador.checked = true;
                uiCalendarConfig.calendars.eventsCalendar.fullCalendar('addEventSource', vm.events[pIndex]);
            }
        });
    }
    function filterPaciente(paciente, check) {
        vm.pacientes.forEach(function (_paciente) {
            if (paciente) {
                if (paciente.id === _paciente.id) {
                    _paciente.checked = check;
                } else {
                    _paciente.checked = false;
                }
            } else _paciente.checked = false;
        });
        vm.selectedPaciente = '';

        uiCalendarConfig.calendars.eventsCalendar.fullCalendar('removeEvents');
        uiCalendarConfig.calendars.eventsCalendar.fullCalendar('removeEventSource', vm.eventSources);
        vm.prestadores.forEach(function (_prestador, pIndex) {
            if (_prestador.checked) {
                var _events = angular.copy(vm.events[pIndex]);
                if (check) {
                    _events = new Array();
                    vm.events[pIndex].forEach(function (_ev, eIndex) {
                        if (_ev.paciente.id === paciente.id) _events.push(_ev);
                    });
                }
                uiCalendarConfig.calendars.eventsCalendar.fullCalendar('addEventSource', _events);
            }
        });

    }
    function newEvent () {
        var _date = _moment().format('YYYY-MM-DD[T]HH:mm:ss');
        var _event = {
          start: _date,
          startTime: _date,
          end: _date,
          endTime: _date,
          arrivalTime: _moment().startOf('day'),
          allDay: allDay(_moment(), _moment())
        };

        openEvent(_event);
    }
    function allDay (start, end) {
      return ((end.diff(start, 'hours') === 24) && (!start.hour() && !start.minutes() && !start.seconds()) && (!end.hour() && !end.minutes() && !end.seconds()));
    }

    function _updateEventList (event, editStatus) {
        vm.eventsCopy = angular.copy(vm.events);
        if (!vm.admin) {
            if (editStatus) {
                vm.events.forEach(function (_ev, index) {
                    if (_ev.id === event.id) {
                        vm.eventsCopy.splice(index, 1);
                        event._allDay = event.allDay;
                        event._id = event.id;
                        event._start = event.start;
                        event._end = event.end;
                        if (event.arrival) event._arrival = event.arrival;
                        vm.eventsCopy.push(event);
                        return;
                    }
                });
            } else vm.eventsCopy.push(event);
            uiCalendarConfig.calendars.eventsCalendar.fullCalendar('removeEvents');
            uiCalendarConfig.calendars.eventsCalendar.fullCalendar('removeEventSource', vm.eventSources);
            uiCalendarConfig.calendars.eventsCalendar.fullCalendar('addEventSource', vm.eventsCopy);
            vm.events = angular.copy(vm.eventsCopy);
        }
        else {
            if (editStatus) {
                vm.events.forEach(function (agenda, aIndex) {
                    agenda.forEach(function (_ev, eIndex) {
                        if (_ev.id === event.id) {
                            if (_ev.pres_id !== event.pres_id) {
                                // Remove from old prestador agenda list
                                vm.eventsCopy[aIndex].splice(eIndex, 1);
                                // Push to the new prestador agenda list
                                vm.prestadores.forEach(function (_pres, pIndex) {
                                    if (event.pres_id === _pres.id) {
                                        event._allDay = event.allDay;
                                        event._id = event.id;
                                        event._start = event.start;
                                        event._end = event.end;
                                        if (event.arrival) event._arrival = event.arrival;
                                        vm.eventsCopy[pIndex].push(event);
                                    }
                                });
                            }
                            else {
                                vm.eventsCopy[aIndex].splice(eIndex, 1);
                                event._allDay = event.allDay;
                                event._id = event.id;
                                event._start = event.start;
                                event._end = event.end;
                                if (event.arrival) event._arrival = event.arrival;
                                vm.eventsCopy[aIndex].push(event);
                                return;
                            }
                        }
                    });
                });
            }
            else {
                vm.prestadores.forEach(function (_pres, pIndex) {
                    if (event.pres_id === _pres.id) {
                        vm.eventsCopy[pIndex].push(event);
                    }
                });
            }
            uiCalendarConfig.calendars.eventsCalendar.fullCalendar('removeEvents');
            uiCalendarConfig.calendars.eventsCalendar.fullCalendar('removeEventSource', vm.eventSources);
            vm.eventsCopy.forEach(function (_agenda) {
                uiCalendarConfig.calendars.eventsCalendar.fullCalendar('addEventSource', _agenda);
            });
            vm.events = angular.copy(vm.eventsCopy);
        }
    }

    function _moment (data) {
      return uiCalendarConfig.calendars.eventsCalendar.fullCalendar('moment', data);
    }

    $scope.$watch('vm.viewDay', function (newValue, oldValue) {
        if (newValue !== oldValue) {
            // true: day, false: week
            if (newValue) {
                vm.view = 'Dia';
                changeView('agendaDay');
            }
            else {
                vm.view = 'Semana';
                changeView('agendaWeek');
            }
        }
    });
  }

  // CreateEventModalInstanceCtrl.$inject = ['$modalInstance', 'event'];
  // function CreateEventModalInstanceCtrl ($modalInstance, event) {
  //   var vm = this;
  //
  //   vm.event = event;
  //
  //   vm.ok = ok;
  //   vm.cancel = cancel;
  //
  //   function ok () {
  //     $modalInstance.close($scope.event);
  //   };
  //
  //   function cancel () {
  //     $modalInstance.dismiss('cancel');
  //   };
  // }
  //
  // ShowEventModalInstanceCtrl.$inject = ['$modalInstance', 'event'];
  // function ShowEventModalInstanceCtrl ($modalInstance, event) {
  //   var vm = this;
  //
  //   vm.event = event;
  //
  //   vm.ok = ok;
  //   vm.cancel = cancel;
  //
  //   function ok () {
  //     $modalInstance.close();
  //   };
  //
  //   function cancel () {
  //     $modalInstance.dismiss('cancel');
  //   };
  // }
  //
  // momentService.$inject = ['$window'];
  // function momentService($window) {
  //   return $window.moment;
  // }



})();
