(function() {
  'use strict';

  var module = angular.module('singApp.preReceitas');

  module.config(cfg);

  function cfg(headersProvider) {
    var _headers = {
      "label": "Pré-Receitas",
      "label_row": "Pré-Receita",
      "route": "preReceitas",
      "settings": {
        "add": true,
        "edit": true,
        "delete": true
      },
      "fields": [
        {
          "name": "id",
          "type": "string",
          "notnull": true,
          "length": null,
          "precision": 10,
          "label": "ID",
          "editable": false,
          "viewable": false,
          "autocomplete": false,
          "quickAdd": [],
          "autocomplete_dependencies": [],
          "customOptions": []
        },
        {
          "name": "nome",
          "type": "string",
          "notnull": true,
          "length": null,
          "precision": 10,
          "label": "Nome",
          "editable": true,
          "viewable": true,
          "autocomplete": false,
          "quickAdd": [],
          "autocomplete_dependencies": [],
          "customOptions": []
        }
      ],
      "masterdetails": {
        "preReceitaItens": {
          "label": "Pré-Receita Itens",
          "label_row": "item",
          "label_stem": "preReceitaItens",
          "fields": [
            {
              "name": "medicamento", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Medicamento", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": {
                "autocomplete_table": {
                  "table_name": "medicamentos",
                  "table_columns": [
                    { "name": "nome_apresentacao", "label": "Descrição do Produto" },
                    { "name": "tipo.label", "label": "Tipo" },
                    { "name": "tarja.label", "label": "Tarja" }
                  ]
                }
              }
            },
            { "name": "qtd", "type": "integer", "notnull": true, "length": 10, "precision": 10, "label": "Qtd.", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            { "name": "posologia", "type": "integer", "notnull": true, "length": 30, "precision": 10, "label": "Posologia", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
            { "name": "dosagem", "type": "integer", "notnull": true, "length": 10, "precision": 10, "label": "Dosagem", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            { "name": "unidade_medida", "type": "string", "notnull": true, "length": 20, "precision": 10, "label": "U. Medida", "editable": true, "viewable": false, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
            { "name": "via", "type": "integer", "notnull": true, "length": 20, "precision": 10, "label": "Via", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
            { "name": "frequencia", "type": "integer", "notnull": true, "length": 20, "precision": 10, "label": "Frequência", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
            { "name": "duracao", "type": "integer", "notnull": true, "length": 10, "precision": 10, "label": "Duração", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
            { "name": "obs", "type": "text", "notnull": false, "length": 5, "precision": 10, "label": "Obs:", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
          ],
          "masterdetails": [],
          "route": "preReceitaItens"
        }
      }
    };

    headersProvider.set('preReceitas', _headers);
  }

})();
