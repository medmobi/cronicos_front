(function () {
    'use strict';

    var module = angular.module('singApp.prontuarios');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Prontuários",
            "label_row": "Prontuário",
            "route": "prontuarios",
            "fixed_tab": true,
            "settings": {
                "add": false,
                "edit": true,
                "delete": false
            },
            "fields": [
                { "tab": 'fixed', "name": "id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 'fixed', "name": "nome", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Nome", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 'fixed', "name": "foto", "type": "string", "notnull": false, "length": 100, "precision": 10, "label": "Foto", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "file": { "container": "fotos" } } },
                { "tab": 'fixed', "name": "sexo", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Sexo", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "enum": { "m": "Masculino", "f": "Feminino", "o": "Indefinido" } } },
                { "tab": 'fixed', "name": "data_nascimento", "type": "date", "notnull": true, "length": null, "precision": 10, "label": "Data de Nascimento", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 'fixed', "name": "email", "type": "string", "notnull": false, "length": 255, "precision": 10, "label": "E-mail", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "email": true } },
                { "tab": 'fixed', "name": "obs", "type": "text", "notnull": false, "length": 255, "precision": 10, "label": "Observações", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },

                {
                    "name": "idade",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Idade",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function (rawData, model) {
                        // debugger;
                        var _label = [];
                        // model.attributes.data_nascimento;

                        return _calculateAge(new Date(model.attributes.data_nascimento));

                        function _calculateAge(birthday) { // birthday is a date
                            var _birthType = ' meses';
                            var _birthMoment = moment(birthday);
                            var _age = moment().diff(_birthMoment, 'months');
                            if (!_age) {
                                _birthType = ' dias';
                                _age = moment().diff(_birthMoment, 'days');
                            }
                            else if (_age > 12) {
                                _birthType = ' anos';
                                _age = moment().diff(_birthMoment, 'years');
                            }

                            return _age + _birthType;
                            // var ageDifMs = Date.now() - birthday.getTime();
                            // var ageDate = new Date(ageDifMs); // miliseconds from epoch
                            // return Math.abs(ageDate.getUTCFullYear() - 1970);
                        }

                    }
                },

                {
                    "name": "cidade",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Cidade",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function (rawData, model) {
                        if (model.attributes.enderecos[0] != undefined) {
                            return model.attributes.enderecos[0].cidade + ' - ' + model.attributes.enderecos[0].estado;
                        } else { }
                    }
                },

                {
                    "name": "convenios",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Convênios",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function (rawData, model) {
                        // debugger;
                        var _label = [];
                        model.attributes.programas.forEach(function (val, key) {
                          if (val.convenio) {
                            _label.push(val.convenio.codigo)
                          }
                        });

                        return _label.join(', ');
                    }
                },
                {
                    "name": "cids",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Cids",
                    "editable": false,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function (rawData, model) {
                        // debugger;
                        var _label = [];
                        model.attributes.cids.forEach(function (val, key) { _label.push(val.cid.descricao) })

                        return _label.join(', ');
                    }
                },
            ],
            "main_tabs": [
                // "Gerais",f
                // "Dados Cadastrais",
                // "Mais Informações"
            ],
            "masterdetails": {

            },
            // "tabs_session": {
            //     "enderecos": {
            //         "label": "Endere\u00e7os",
            //         "label_row": "Endere\u00e7o",
            //         "fields": [
            //             { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.padd_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "pes_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "cep", "type": "string", "notnull": true, "length": 10, "precision": 10, "label": "CEP", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "cep": { "address": "logradouro", "city": "cidade", "district": "bairro", "state": "estado" } } },
            //             { "name": "logradouro", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "Endere\u00e7o", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "numero", "type": "string", "notnull": true, "length": 5, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "complemento", "type": "string", "notnull": false, "length": 255, "precision": 10, "label": "Complemento", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             // { "name": "quadra", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Quadra", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             // { "name": "lote", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Lote", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "bairro", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Bairro", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "cidade", "type": "string", "notnull": true, "length": 60, "precision": 10, "label": "Cidade", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "estado", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Estado", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         ],
            //         "masterdetails": []
            //     },
            //     "telefones": {
            //         "label": "Telefones",
            //         "label_row": "Telefone",
            //         "label_stem": "telefones",
            //         "fields": [
            //             { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "pres_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "tipo_telefone", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Tipo de Telefone", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
            //             { "name": "numero", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "telefone": true } }
            //         ],
            //         "masterdetails": [],
            //         "route": "telefones"
            //     },
            //     "especialidades": {
            //         "label": "Especialidades",
            //         "label_row": "Especialidade",
            //         "fields": [
            //             { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "pres_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "especialidade", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Especialidade", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         ],
            //         "masterdetails": [],
            //         "route": "especialidades"
            //     },
            //     "bancos": {
            //         "label": "Contas Bancárias",
            //         "label_row": "Conta Bancária",
            //         "fields": [
            //             { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "pres_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "banco", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Banco", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "agencia", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "Agência", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //             { "name": "conta", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "C/C", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
            //         ],
            //         "masterdetails": [],
            //         "route": "contas"
            //     }
            // },
            "tabs": {
              "cid": {
                "label": "CIDs",
                "label_row": "CID",
                "label_stem": "cids",
                "fields": [
                  { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "cid", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "CID", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                ],
                "masterdetails": [],
                "route": "cids",
                "tab_route": "pacientes/details/"
              },
              "exames_digitais": {
                "label": "Exames Digitais",
                "label_row": "exame",
                "label_stem": "exames_digitais",
                "fields": [
                  { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.exams.exam_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "date", "type": "date", "notnull": true, "length": 255, "precision": 100, "label": "Data", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "obs", "type": "string", "notnull": false, "length": 50, "precision": 100, "label": "Obs", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "indicador", "type": "string", "notnull": true, "length": 50, "precision": 100, "label": "Exame", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "valor", "type": "float", "notnull": true, "length": null, "precision": 100, "label": "Valor", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { min: 0 } },
                  { "name": "resultado", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Resultado", "editable": true, "readonly": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "cor", "type": "simplecolor", "notnull": true, "length": null, "precision": 10, "label": "Sinalização", "editable": true, "readonly": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
                ],
                "route": "exames_digitais",
                "tab_route": "pacientes/details/",
                "chart_settings": {
                  'xType': 'date',
                  'xLabel': 'data',
                  'xOffset': 1,
                  'yType': 'number',
                  'yLabel': 'valor',
                  'yOffset': 10
                },
                "masterdetails": {
                  "exameDigitalItem": {
                    "visible": true,
                    "label": "Item de Exame Digital",
                    "label_row": "Item",
                    "label_stem": "exameDigitalItems",
                    "fields": [
                      [
                        { "name": "date", "type": "date", "notnull": true, "length": 50, "precision": 100, "label": "Data", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                      ],
                      [
                        { "name": "indicador", "type": "string", "notnull": false, "length": 50, "precision": 100, "label": "Exame", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "valor", "type": "float", "notnull": true, "length": null, "precision": 100, "label": "Valor", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { min: 0 } },
                      ],
                      [
                        { "name": "resultado", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Resultado", "editable": true, "readonly": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": []},
                        { "name": "cor", "type": "simplecolor", "notnull": true, "length": null, "precision": 10, "label": "Sinalização", "editable": true, "readonly": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": []}
                      ],
                      [
                        { "name": "obs", "type": "string", "notnull": false, "length": 50, "precision": 100, "label": "Obs", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                      ]
                    ]
                  }
                }
              },
              "exames": {
                "label": "Exames",
                "label_row": "arquivo",
                "label_stem": "exames",
                "fields": [
                  { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.exams.exam_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "titulo", "type": "string", "notnull": true, "length": 50, "precision": 100, "label": "Título", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "descricao", "type": "string", "notnull": false, "length": 50, "precision": 100, "label": "Descrição", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "arquivo", "type": "string", "notnull": true, "length": null, "precision": 100, "label": "Arquivo", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "file": { "container": "exames" } } },
                  { "name": "download", "type": "custom", "notnull": true, "length": null, "precision": 10, "label": "Download", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [],
                    "toString": function (rawData, model) {
                      var btn = '<button type="button" class="btn btn-default" onclick="window.open(\'http://api.cronicos.anexs.com.br:3001/api/upload/exames/download/\' + angular.element(this).parent().parent().find(\'.string-cell\').last()[0].innerText)">' +
                        '<span class="glyphicon glyphicon-download"></span>&nbsp;&nbsp;Visualizar' +
                        '</button>';

                      return btn;
                    }
                  }
                ],
                "masterdetails": [],
                "route": "exames",
                "tab_route": "pacientes/details/"
              },
              "documentos-medicos": {
                "label": "Documentos Médicos do(a) Paciente",
                "label_row": "Documento Médico",
                "label_stem": "documentos_medicos",
                "settings": {
                  "edit": true,
                  "delete": true
                },
                "fields": [
                  { "name": "id", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "updatedAt", "type": "date", "notnull": true, "length": 50, "precision": 10, "label": "Data Criada", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "nome", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Nome", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "documento_tipo", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Tipo", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "texto", "type": "text", "notnull": true, "length": 100, "precision": 10, "label": "Modelo Texto", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "placeholder": "Opções de substituição de informações: [PACIENTE]" } },
                  { "name": "assinatura_pac_status", "type": "boolean", "notnull": false, "length": 20, "precision": 10, "label": "Assin. Paciente", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "statusTrueText": "Ativo", "statusFalseText": "Inativo" } },
                  { "name": "assinatura_pres_status", "type": "boolean", "notnull": false, "length": 20, "precision": 10, "label": "Assin. Prestador", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "statusTrueText": "Ativo", "statusFalseText": "Inativo" } },
                  { "name": "prestador", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Prestador", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                  { "name": "print", "type": "custom", "notnull": true, "length": null, "precision": 10, "label": "Imprimir", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [],
                    "toString": function (rawData, model) {
                      // angular.element(this).scope().printDocumentoById(angular.element(this).parent().parent().find('.string-cell').first()[0].outerText)
                      var btn = '<button type="button" class="btn btn-default" onclick="angular.element(this).scope().printDocumentoById(angular.element(this).parent().parent().find(\'.string-cell\').first()[0].innerText)">' +
                        '<span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Imprimir' +
                        '</button>';

                      return btn;
                    }
                  }
                ],
                "masterdetails": {
                   "search": {
                     "label": "Documentos Médicos",
                      "label_row": "Documentos Médicos",
                      "route": "documentos_medicos",
                      "fields": [
                         { "name": "documentos-medicos", "type": "integer", "notnull": false, "length": null, "precision": 10, "label": "Selecione um documento médico pré-cadastrado", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [{"field": 'emp_id'}], "customOptions": { "general": 'DocumentoMedico' } }
                       ]
                   }
                },
                "route": "documentos_medicos",
                "tab_route": "pacientes/details/"
              }
            }
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('prontuarios', _headers);
    }

})();
