(function () {
    'use strict';

    var module = angular.module('singApp.atendimentos');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Atendimentos",
            "label_row": "Atendimento",
            "route": "atendimentos",
            "parent_route": "prontuarios",
            "fixed_tab": false,
            "settings": {
                "add": false,
                "edit": true,
                "delete": true
            },
            "fields": [
                { "tab": 'fixed', "name": "id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 'fixed', "name": "nome", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Nome", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 'fixed', "name": "foto", "type": "string", "notnull": false, "length": 100, "precision": 10, "label": "Foto", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "file": { "container": "fotos" } } },
                { "tab": 'fixed', "name": "sexo", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Sexo", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "enum": { "m": "Masculino", "f": "Feminino", "o": "Indefinido" } } },
                { "tab": 'fixed', "name": "data_nascimento", "type": "date", "notnull": true, "length": null, "precision": 10, "label": "Data de Nascimento", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "tab": 'fixed', "name": "email", "type": "string", "notnull": false, "length": 255, "precision": 10, "label": "E-mail", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "email": true } },
                { "tab": 'fixed', "name": "obs", "type": "text", "notnull": false, "length": 255, "precision": 10, "label": "Observações", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },

                {
                    "name": "idade",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Idade",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function (rawData, model) {
                        // debugger;
                        var _label = [];
                        // model.attributes.data_nascimento;

                        return _calculateAge(new Date(model.attributes.data_nascimento));

                        function _calculateAge(birthday) { // birthday is a date
                            var ageDifMs = Date.now() - birthday.getTime();
                            var ageDate = new Date(ageDifMs); // miliseconds from epoch
                            return Math.abs(ageDate.getUTCFullYear() - 1970);
                        }

                    }
                },

                {
                    "name": "cidade",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Cidade",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function (rawData, model) {
                        if (model.attributes.enderecos[0] != undefined) {
                            return model.attributes.enderecos[0].cidade + ' - ' + model.attributes.enderecos[0].estado;
                        } else { }
                    }
                },

                {
                    "name": "convenios",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Convênios",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function (rawData, model) {
                        // debugger;
                        var _label = [];
                        model.attributes.programas.forEach(function (val, key) { _label.push(val.convenio.codigo) })

                        return _label.join(', ');
                    }
                },
                {
                    "name": "cids",
                    "type": "custom",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Cids",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function (rawData, model) {
                        // debugger;
                        var _label = [];
                        model.attributes.cids.forEach(function (val, key) { _label.push(val.cid.descricao) })

                        return _label.join(', ');
                    }
                },
            ],
            // "main_tabs": [
            //     "Gerais",
            //     // "Dados Cadastrais",
            //     // "Mais Informações"
            // ],
            "masterdetails": [],
            // "tabs_session": {
            // },
            "tabs": {
                "formulario": {
                    "label": "Formulários",
                    "label_row": "Formulário",
                    "label_stem": "formulario_respostas",
                    "fields": [
                      { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                      { "name": "especialidades", "type": "array", "notnull": true, "length": null, "precision": 10, "label": "Especialidades", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                      { "name": "formulario", "type": "integer", "notnull": true, "required": true, "length": null, "precision": 10, "label": "Formulário", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [{"field": 'especialidades'}], "customOptions": { "general": 'Formulario' } },
                      { "name": "resposta", "type": "string", "notnull": true, "required": true, "length": null, "precision": 10, "label": "Resposta", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                    ],
                    "masterdetails": [],
                    "settings": {
                      'edit': true,
                      'delete': true
                    },
                    "route": "formulario_respostas",
                    "tab_route": "atendimentos/details/",
                },
                "cid": {
                    "label": "CIDs",
                    "label_row": "CID",
                    "label_stem": "cids",
                    "fields": [
                      { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                      { "name": "cid", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "CID", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                    ],
                    "masterdetails": [],
                    "route": "cids",
                    "tab_route": "prontuarios/details/",
                    "data_path": "paciente"
                },
                "exames_digitais": {
                  "label": "Exames Digitais",
                  "label_row": "exame",
                  "label_stem": "exames_digitais",
                  "fields": [
                    { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.exams.exam_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "date", "type": "date", "notnull": true, "length": 50, "precision": 100, "label": "Data", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "obs", "type": "string", "notnull": false, "length": 50, "precision": 100, "label": "Obs", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "indicador", "type": "string", "notnull": false, "length": 50, "precision": 100, "label": "Exame", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "valor", "type": "float", "notnull": true, "length": null, "precision": 100, "label": "Valor", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { min: 0 } },
                    { "name": "resultado", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Resultado", "editable": true, "readonly": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": []},
                    { "name": "cor", "type": "simplecolor", "notnull": true, "length": null, "precision": 10, "label": "Sinalização", "editable": true, "readonly": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": []}
                  ],
                  "route": "exames_digitais",
                  "tab_route": "prontuarios/details/",
                  "data_path": "paciente",
                  "masterdetails": {
                    "exameDigitalItem": {
                      "visible": true,
                      "label": "Item de Exame Digital",
                      "label_row": "Item",
                      "label_stem": "exameDigitalItems",
                      "fields": [
                        [
                          { "name": "date", "type": "date", "notnull": true, "length": 50, "precision": 100, "label": "Data", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        ],
                        [
                          { "name": "indicador", "type": "string", "notnull": false, "length": 50, "precision": 100, "label": "Exame", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                          { "name": "valor", "type": "float", "notnull": true, "length": null, "precision": 100, "label": "Valor", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { min: 0 } },
                        ],
                        [
                          { "name": "resultado", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Resultado", "editable": true, "readonly": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": []},
                          { "name": "cor", "type": "simplecolor", "notnull": true, "length": null, "precision": 10, "label": "Sinalização", "editable": true, "readonly": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": []}
                        ],
                        [
                          { "name": "obs", "type": "string", "notnull": false, "length": 50, "precision": 100, "label": "Obs", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        ]
                      ]
                    }
                  }
                },
                "exames": {
                  "label": "Exames",
                  "label_row": "arquivo",
                  "label_stem": "exames",
                  "fields": [
                    { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.exams.exam_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "titulo", "type": "string", "notnull": true, "length": 50, "precision": 100, "label": "Título", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "descricao", "type": "string", "notnull": false, "length": 50, "precision": 100, "label": "Descrição", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "arquivo", "type": "string", "notnull": true, "length": null, "precision": 100, "label": "Arquivo", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "file": { "container": "exames" } } },
                    { "name": "download", "type": "custom", "notnull": true, "length": null, "precision": 10, "label": "Download", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [],
                      "toString": function (rawData, model) {
                        var btn = '<button type="button" class="btn btn-default" onclick="window.open(\'http://localhost:3000/api/upload/exames/download/\' + angular.element(this).parent().parent().find(\'.string-cell\').last()[0].innerText)">' +
                          '<span class="glyphicon glyphicon-download"></span>&nbsp;&nbsp;Visualizar' +
                          '</button>';

                        return btn;
                      }
                    }
                  ],
                  "route": "exames",
                  "tab_route": "prontuarios/details/",
                  "data_path": "paciente"
                },
                "documentos-medicos": {
                  "label": "Documentos Médicos do(a) Paciente",
                  "label_row": "Documento Médico",
                  "label_stem": "documentos_medicos",
                  "settings": {
                    "edit": true,
                    "delete": true
                  },
                  "fields": [
                    { "name": "id", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "updatedAt", "type": "date", "notnull": true, "length": 50, "precision": 10, "label": "Data Criada", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "nome", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Nome", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "documento_tipo", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Tipo", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "texto", "type": "text", "notnull": true, "length": 100, "precision": 10, "label": "Modelo Texto", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "placeholder": "Opções de substituição de informações: [PACIENTE]" } },
                    { "name": "assinatura_pac_status", "type": "boolean", "notnull": false, "length": 20, "precision": 10, "label": "Assin. Paciente", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "statusTrueText": "Ativo", "statusFalseText": "Inativo" } },
                    { "name": "assinatura_pres_status", "type": "boolean", "notnull": false, "length": 20, "precision": 10, "label": "Assin. Prestador", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "statusTrueText": "Ativo", "statusFalseText": "Inativo" } },
                    { "name": "prestador", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Prestador", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "print", "type": "custom", "notnull": true, "length": null, "precision": 10, "label": "Imprimir", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [],
                      "toString": function (rawData, model) {
                        // angular.element(this).scope().printDocumentoById(angular.element(this).parent().parent().find('.string-cell').first()[0].outerText)
                        var btn = '<button type="button" class="btn btn-default" onclick="angular.element(this).scope().printDocumentoById(angular.element(this).parent().parent().find(\'.string-cell\').first()[0].outerText)">' +
                          '<span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Imprimir' +
                          '</button>';

                        return btn;
                      }
                    }
                  ],
                  "masterdetails": {
                    "search": {
                      "label": "Documentos Médicos",
                        "label_row": "Documentos Médicos",
                        "route": "documentos_medicos",
                        "fields": [
                        { "name": "documentos-medicos", "type": "integer", "notnull": false, "length": null, "precision": 10, "label": "Selecione um documento médico pré-cadastrado", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [{"field": 'emp_id'}], "customOptions": { "general": 'DocumentoMedico' } }
                      ]
                    }
                  },
                  "route": "documentos_medicos",
                  "tab_route": "prontuarios/details/",
                  "data_path": "paciente"
                },
                "encaminhamentos": {
                  "label": "Encaminhamentos",
                  "label_row": "Encaminhamento",
                  "route": "encaminhamentos",
                  "fields": [
                    { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.atendimento_encaminhamento.enc_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "observacao", "type": "string", "notnull": false, "length": 50, "precision": 10, "label": "Observação", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "especialidade", "type": "integer", "notnull": false, "length": null, "precision": 10, "label": "Especialidade", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "retorno", "type": "date", "notnull": false, "length": null, "precision": 10, "label": "Retorno", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    { "name": "prestador", "type": "integer", "notnull": false, "length": null, "precision": 10, "label": "Prestador", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [{ "field": 'especialidade', "label": 'especialidade' }], "customOptions": [] },
                  ],
                  "masterdetails": []
                },
                "receituario": {
                    "label": "Receituário",
                    "label_row": "Item",
                    "label_stem": "receituarios",
                    "generalRoute": "/atendimentos",
                    "fields": [
                        { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.atendimento_receituario.id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "nome", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Nome", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "data", "type": "date", "notnull": true, "length": 50, "precision": 10, "label": "Data", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
                    ],
                    "route": "receituario_itens",
                    "masterdetails": {
                        "receitaItem": {
                            "visible": true,
                            "label": "Receituário Item",
                            "label_row": "Item",
                            "label_stem": "receituarioItens",
                            "fields": [
                                [
                                    {
                                        "name": "medicamento", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Medicamento", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": {
                                            "autocomplete_table": {
                                                "table_name": "medicamentos",
                                                "table_columns": [
                                                    { "name": "nome_apresentacao", "label": "Descrição do Produto" },
                                                    { "name": "tipo.label", "label": "Tipo" },
                                                    { "name": "tarja.label", "label": "Tarja" },
                                                ]
                                            }
                                        }
                                    },
                                    { "name": "qtd", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Qtd.", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                                ],
                                [
                                    { "name": "posologia", "type": "integer", "notnull": true, "length": 5, "precision": 10, "label": "Posologia", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                                    { "name": "dosagem", "type": "integer", "notnull": true, "length": 5, "precision": 10, "label": "Dosagem", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                                    { "name": "unidade_medida", "type": "string", "notnull": true, "length": 5, "precision": 10, "label": "U. Medida", "editable": true, "viewable": false, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                                    { "name": "via", "type": "integer", "notnull": true, "length": 5, "precision": 10, "label": "Via", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                                    { "name": "frequencia", "type": "integer", "notnull": true, "length": 5, "precision": 10, "label": "Frequência", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                                    { "name": "duracao", "type": "integer", "notnull": true, "length": 5, "precision": 10, "label": "Duração", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                                ],
                                [
                                    { "name": "obs", "type": "text", "notnull": false, "length": null, "precision": 10, "label": "Obs:", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
                                ]
                            ]
                        },
                        "prereceita": {
                            "label": "Pré-Receitas",
                            "label_row": "Pré-Receita",
                            "route": "preReceitas",
                            "fields": [
                                { "name": "prereceita", "type": "integer", "notnull": false, "length": null, "precision": 10, "label": "Selecione uma receita pré-cadastrado", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": { "general": 'PreReceita' } }
                            ]
                        }
                    }
                }
            }
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('atendimentos', _headers);
    }

})();
