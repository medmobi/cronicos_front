(function() {
    'use strict';

    var module = angular.module('singApp.prontuarios', [
        'pascoal.crud'
    ]);

    module.filter('prontuarioFilter', function (fwComparatorService) {
      return function (list, filterPront) {
        if (list) {
          return fwComparatorService.filterList(list,
            { encaminhamentos: filterPront.extras.encaminhamentos, receituario: filterPront.extras.receitas, exames: filterPront.extras.exames, arquivos: filterPront.extras.arquivos, documentos: filterPront.extras.documentos },
            { tipo: ['Consulta', 'Retorno'], status: ['Finalizado', 'Cancelado'] },
            { tipo: filterPront.tipoResults, status: filterPront.statusResults },
            { tipo: filterPront.tipoPreviousResults, status: filterPront.statusPreviousResults },
            { tipo: 'nome', status: 'nome' }
          );
        } else return [];
      };
    });

    module.controller('ExameDigitalCtrl', function ($scope, Restangular, $stateParams, $timeout, headers, $rootScope, $modalInstance, $q, ngToast, $controller, PreReceitaItem, fwObjectService, $state, $http, Upload, $modal, parentModel, data, fwStringService, autocompleteDetail, fwDbTypesService) {
        $controller('CRUDFormModalController', { $controller: $controller, $scope: $scope, $modalInstance: $modalInstance, ngToast: ngToast, headers: headers, Restangular: Restangular, $stateParams: $stateParams, $timeout: $timeout, $state: $state, $rootScope: $rootScope, $q: $q, $http: $http, Upload: Upload, $modal: $modal, parentModel: parentModel, data: data, fwStringService: fwStringService, autocompleteDetail: autocompleteDetail });

        var indicadorAlternativasTipos = fwDbTypesService.getIndicadorExamAlternativas();
        var indicadorExamAsideAlternativas = fwDbTypesService.getIndicadorExamAsideAlternativas();
        var alternativaFormulaResultados = null;
        var acceptIndicador = false;
        var _exameDigitalFields = headers.masterdetails.exameDigitalItem.fields;
        $scope.fields = _exameDigitalFields[0].concat(_exameDigitalFields[1], _exameDigitalFields[2]);
        $scope.exameDigitalItens = [];

        $scope.deleteItem = deleteItem;
        $scope.editItem = editItem;
        $scope.save = save;
        $scope.submit = submit;

        //////////

        _init();

        function _init () {
            _restartScopeData();
            eventDropdownButton();
        }
        function _restartScopeData () {
            var date = null;
            if ($scope.data.date) date = $scope.data.date;
            else date = moment();
            $scope.data = { emp_id: $scope.data.emp_id, date: date };
        }
        function eventDropdownButton() {
            $timeout(function () {
                jQuery(".input-group-btn.ng-scope").on("click", function () {
                    jQuery(this).parent().children('input').click();
                });
            });
        }
        function deleteItem (index) {
            $scope.exameDigitalItens.splice(index, 1);
        }
        function editItem (index) {
            var _item = $scope.exameDigitalItens[index];
            fwObjectService.setInputsFromObject(_item);
            $scope.deleteItem(index);
        }
        function save () {
            var _exameDigitalItem = angular.copy($scope.data);
            $scope.exameDigitalItens.push(_exameDigitalItem);

            // Clear exame digital items form inputs
            $scope.fields.forEach(function (field) {
                if (field.name !== 'date') angular.element('#' + field.name).val('');
            });

            $timeout(function () {
                angular.element('.form-horizontal').closest('form').parsley().reset();
            });

            _restartScopeData();
        }
        /**
         * @override CRUDFormModalController.submit
         */
        function submit () {
            if ($scope.exameDigitalItens.length > 0) {
                $modalInstance.close($scope.exameDigitalItens);
            }
            else {
                var messages = [];
                if ($scope.exameDigitalItens.length === 0) messages.push('Insira pelo menos um item no exame digital!');
                if (messages.length > 0) ngToast.warning(messages.join("<br />"));
            }
        }

        // Watch indicador types (alternativas, numerico and text)
        $scope.$watch('data.indicador', function(_new, _old) {
            if ($scope.data['indicador.label'] !== undefined) {
                var indicador = ($scope.data['indicador.label']);
                // indicador as alternativa
                if (indicadorAlternativasTipos.indexOf(indicador.tipo.nome) !== -1) {
                    acceptIndicador = true;
                    $scope.headers.fields.forEach(function (_field) {
                        if (_field.name === 'valor') {
                            _field.label = 'Chave';
                        }
                    });

                    alternativaFormulaResultados = JSON.parse(indicador.formula).alternativas;
                }
                else {
                    alternativaFormulaResultados = null;

                    if (indicadorExamAsideAlternativas.indexOf(indicador.tipo.nome) !== -1) {
                        acceptIndicador = true;
                        $scope.headers.fields.forEach(function (_field) {
                            if (_field.name === 'valor') {
                                _field.label = 'Valor';
                            }
                        });
                    }
                    else {
                        acceptIndicador = false;
                        ngToast.warning("Esse tipo de exame não pode ser registrado como exame digital.");
                    }
                }
            }
        });

        $scope.$watch('data.valor', function(_new, _old) {
            if ($scope.data['indicador.label'] !== undefined) {
                if (acceptIndicador) {
                    var found = false;

                    $timeout(function() {
                        if (alternativaFormulaResultados) {
                            alternativaFormulaResultados.forEach(function (row) {
                                if (row.chave == _new) {
                                    $scope.data.resultado = row.legenda;
                                    $scope.data.cor = row.cor;
                                    found = true;

                                    return false;
                                }
                            });
                        } else {
                            var resultados = ($scope.data['indicador.label'].resultados);
                            resultados.forEach(function (row) {
                                // console.log(row);

                                if (row.indice_min < _new && _new <= row.indice_max ) {
                                    $scope.data.resultado = row.resultado;
                                    $scope.data.cor = row.cor;
                                    found = true;

                                    return false;
                                }
                            });

                        }

                        if (!found) {
                            $scope.data.resultado = null;
                            $scope.data.cor = null;

                            ngToast.warning("Este resultado está fora da faixa prevista.");
                        } else {
                            setTimeout(function () {
                                angular.element('#cor').val($scope.data.resultado);
                            }, 50);
                        }
                    });
                } else {
                    ngToast.warning("Esse tipo de exame não pode ser registrado como exame digital.");
                }
            }
        });
    });

    module.controller('ProntuarioController', function($scope, Restangular, $stateParams, $timeout, $modal, module, $state, $rootScope, $q, ngToast, $http, Upload, $controller, Paciente, Atendimento, StatusAtendimento, fwDateTimeService, fwPrintService, Usuario, auth, fwModalService, PacienteCid, PacienteExame, PacienteDocumentoMedico, fwChartService, fwStringService, PacienteExameDigital, fwObjectService) {
        $controller('CRUDEditController', { $scope: $scope, Restangular: Restangular, $stateParams: $stateParams, $timeout: $timeout, $modal: $modal, module: module, $state: $state, $rootScope: $rootScope, $q: $q, ngToast: ngToast, $http: $http, Upload: Upload }); //This works

        // $scope.cidTabHeaders = {
        //     "label": "CIDs",
        //     "label_row": "CID",
        //     "fields": [
        //         { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        //         { "name": "cid", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "CID", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        //
        //     ],
        //     "masterdetails": [],
        //     "route": "/details/cids"
        // };
        $scope.filterPront = {
            tipo: { Consulta: true, Retorno: true },
            status: { Finalizado: true, Cancelado: true },
            extras: { encaminhamentos: true, receitas: true, exames: true, arquivos: true, documentos: true },
            tipoResults: ['Consulta', 'Retorno'],
            statusResults: ['Finalizado', 'Cancelado'],
            tipoPreviousResults: ['Consulta', 'Retorno'],
            statusPreviousResults: ['Finalizado', 'Cancelado']
        };

        $scope.dashboards = [];

        //////////

        $scope.startAtendimento = startAtendimento;
        $scope.goToAtendimento = goToAtendimento;
        $scope.clearFilter = clearFilter;
        $scope.print = print;
        $scope.printReceituario = printReceituario;
        $scope.printDocumento = printDocumento;
        $scope.printDocumentoById = printDocumentoById;
        $scope.openTabModal = openTabModal;
        $scope.downloadExam = downloadExam;
        $scope.renderCharts = renderCharts;

        function startAtendimento () {
          var atendimento = {
            pac_id: $scope.paciente.id
          };

          var atendimentoType = null;
          if($scope.prontuario.length === 0) atendimentoType = 'Consulta';
          else atendimentoType = _checkAtendimentoType($scope.prontuario[0].inicio);
          Atendimento.createWithType({ nome: atendimentoType }, atendimento)
            .$promise
            .then(function success(row) {
              // console.log(row.data)
              // console.log(arguments);
              $state.go('app.atendimentos.edit', {id: row.data.id})
            });
        }
        function goToAtendimento (atendimento) {
          if (atendimento.encaminhamentos) $state.go('app.atendimentos.edit',{id: atendimento.id})
        }
        function clearFilter () {
            $scope.filterPront = {
                tipo: { Consulta: false, Retorno: false },
                status: { Finalizado: false, Cancelado: false },
                extras: { encaminhamentos: false, receitas: false, exames: false, arquivos: false, documentos: false },
                tipoResults: [],
                statusResults: [],
                tipoPreviousResults: $scope.filterPront.tipoResults,
                statusPreviousResults: $scope.filterPront.statusResults
            };
        }
        function _print (type, data) {
            var user = auth.getUser();
            if (user !== null) {
                Usuario
                    .findOne({
                        filter: {
                            where: {
                                id: user.id
                            },
                            include: [
                              {
                                'prestador': [ 'conselho', 'conselho_uf']
                              },
                              {
                                'empresa': [ 'telefones', 'enderecos' ]
                              }
                            ]
                        }
                    })
                    .$promise
                    .then(function (user) {
                        switch (type) {
                          case 'receituario':
                              fwPrintService.printReceituario(data, $scope.paciente, user.empresa, user.prestador);
                              break;
                          case 'documento':
                              var _data = angular.copy(data);
                              _data.texto = fwStringService.changePlaceholders(_data.texto, { paciente: $scope.paciente });
                              fwPrintService.printDocumento(_data, $scope.paciente, user.empresa, user.prestador);
                              break;
                          case 'prontuario':
                              fwPrintService.printProntuario($scope.prontuario, $scope.paciente, user.empresa, user.prestador);
                              break;
                          default:
                              console.error("Type not available for print.");
                        }
                    });
            }
        }
        function print () {
          _print('prontuario', null);
        }
        function printReceituario (data) {
          _print('receituario', data);
        }
        function printDocumento (data) {
          _print('documento', data);
        }
        function printDocumentoById (id) {
            PacienteDocumentoMedico.findOne({
                  filter: {
                      where: {
                          id: id
                      },
                      include: [
                          {
                            'prestador': [ 'conselho', 'conselho_uf']
                          },
                          'documento_tipo'
                      ]
                  }
                })
                .$promise
                .then(function (doc) {
                    _print('documento', doc);
                });
        }
        function openTabModal (type) {
            if (type == 'exames_digitais') {
                // fwModalService.createCRUDModal($scope.headers['tabs'][type], $scope.headers.label.toLowerCase(), null, true, 'ExameDigitalCtrl')
                fwModalService.createCRUDModal($scope.headers['tabs'][type], $scope.headers.label.toLowerCase(), null, true, 'ExameDigitalCtrl', 'app/modules/prontuarios/exames-digitais-form.html')
                    .then(function (response) {
                        switch (type) {
                            case 'exames_digitais':
                                var promises = [];
                                response.forEach(function (exame) {
                                    exame.pac_id = $scope.paciente.id;
                                    promises.push(PacienteExameDigital.create(response));
                                });
                                return $q.all(promises);
                            default:
                                console.error("Tab modal not implemented yet.");
                        }
                    })
                    .then(function () {
                        $timeout(function () {
                            $rootScope.$broadcast('refreshGRID');
                            _loadProntuario();
                            // $rootScope.$emit('update-chart', { type: 'new' });
                        }, 500);
                    });
            }
            else if (type === 'documentos-medicos') {
                auth.isPrestador()
                    .then(function success(user) {
                        var _doc = {};
                        _doc.prestador = user.prestador;
                        _doc['prestador.label'] = user.prestador.nome;
                        return fwModalService.createCRUDModal($scope.headers['tabs'][type], $scope.headers.label.toLowerCase(), _doc, true);
                    })
                    .then(function (response) {
                        switch (type) {
                          case 'documentos-medicos':
                              response.pac_id = $scope.paciente.id;
                              return PacienteDocumentoMedico.create(response);
                          default:
                              console.error("Tab modal not implemented yet.");
                        }
                    })
                    .then(function () {
                        $timeout(function () {
                            $rootScope.$broadcast('refreshGRID');
                            if (type === 'exames' || type === 'documentos-medicos') _loadProntuario();
                        }, 500);
                    })
                    .catch(function error(status) {
                        if(!status) {
                            // User isn't prestador
                            fwModalService.createCRUDModal($scope.headers['tabs'][type], $scope.headers.label.toLowerCase(), null, true)
                                .then(function (response) {
                                    switch (type) {
                                        case 'documentos-medicos':
                                            response.pac_id = $scope.paciente.id;
                                            return PacienteDocumentoMedico.create(response);
                                        default:
                                            console.error("Tab modal not implemented yet.");
                                    }
                                })
                                .then(function () {
                                    $timeout(function () {
                                        $rootScope.$broadcast('refreshGRID');
                                        if (type === 'exames' || type === 'documentos-medicos') _loadProntuario();
                                    }, 500);
                                })
                        }
                    });
            }
            else {
                fwModalService.createCRUDModal($scope.headers['tabs'][type], $scope.headers.label.toLowerCase(), null, true)
                    .then(function (response) {
                        switch (type) {
                            case 'exames':
                                response.pac_id = $scope.paciente.id;
                                return PacienteExame.create(response);
                            case 'cid':
                                return PacienteCid.create({cid_id: response.cid, pac_id: $scope.data.id});
                            case 'documentos-medicos':
                                response.pac_id = $scope.paciente.id;
                                return PacienteDocumentoMedico.create(response);
                            default:
                                console.error("Tab modal not implemented yet.");
                        }
                    })
                    .then(function () {
                        $timeout(function () {
                            $rootScope.$broadcast('refreshGRID');
                            if (type === 'exames' || type === 'documentos-medicos') _loadProntuario();
                        }, 500);
                    });
            }
        }
        function _checkAtendimentoType (inicio) {
          var diff = fwDateTimeService.getDiffDuration(moment(), moment(inicio), 'day');
          if(diff > 30) return 'Consulta';
          else return 'Retorno';
        }
        function _mixProntuarios (prontuarios) {
            var _newList = angular.copy(prontuarios);

            // Add multiple label on consultas and retornos
            _newList.forEach(function (el) {
              el.multiple = true;
            });

            // Add encaminhamentos and receituario to prontuario list
            prontuarios.forEach(function (prontuario) {
              if (prontuario.encaminhamentos.length > 0) {
                prontuario.encaminhamentos.forEach(function (enc) {
                  enc.inicio = enc.createdAt;
                  enc.fim = enc.updatedAt;
                  enc.status = { nome: 'Finalizado' };
                  enc.label = 'encaminhamentos';
                  _newList.push(enc);
                });
              }
              else if (prontuario.receituarios.length > 0) {
                prontuario.receituarios.forEach(function (rec) {
                  rec.inicio = rec.createdAt;
                  rec.fim = rec.updatedAt;
                  rec.status = { nome: 'Finalizado' };
                  rec.label = 'receituario';
                  rec.prestador = prontuario.prestador;
                  _newList.push(rec);
                });
              }
            });

            // Add arquivos to prontuario list
            if ($scope.data.exames && $scope.data.exames.length > 0) {
              $scope.data.exames.forEach(function (exam) {
                exam.inicio = exam.createdAt;
                exam.fim = exam.updatedAt;
                exam.status = { nome: 'Finalizado' };
                exam.label = 'arquivos';
                _newList.push(exam);
              });
            }

            // Add exames to prontuario list
            if ($scope.data.exames_digitais && $scope.data.exames_digitais.length > 0) {
              $scope.data.exames_digitais.forEach(function (exam) {
                exam.inicio = exam.createdAt;
                exam.fim = exam.updatedAt;
                exam.status = { nome: 'Finalizado' };
                exam.label = 'exames';
                _newList.push(exam);
              });
            }

            // Add documentos medicos to prontuario list
            if ($scope.data.documentos_medicos && $scope.data.documentos_medicos.length > 0) {
              $scope.data.documentos_medicos.forEach(function (doc) {
                doc.inicio = doc.createdAt;
                doc.fim = doc.updatedAt;
                doc.status = { nome: 'Finalizado' };
                doc.label = 'documentos';
                _newList.push(doc);
              });
            }

            return _newList;
        }
        function downloadExam (exam) {
          var url = ($rootScope.appSettings.API_URL + 'upload/exames/download/' + exam.arquivo);
          window.open(url, '_blank');
          // _download(url, exam.arquivo);
        }
        function _download (url, filename) {
          $http({
            method: 'GET',
            url: url,
            responseType: 'arraybuffer'
          }).success(function (data, status, headers) {
            headers = headers();

            var contentType = headers['content-type'];

            var blob = new Blob([data], { type: contentType });

            _downloadFile(blob, filename);
          });
        }
        function _downloadFile (blob, filename) {
          var linkElement = document.createElement('a');
          try {

            var url = window.URL.createObjectURL(blob);

            linkElement.setAttribute('href', url);
            linkElement.setAttribute("download", filename);

            var clickEvent = new MouseEvent("click", {
              "view": window,
              "bubbles": true,
              "cancelable": false
            });
            linkElement.dispatchEvent(clickEvent);
            console.log(clickEvent)
          } catch (ex) {
            // console.log(ex);
          }
        }
        function _loadProntuario () {
            $scope.resource = Restangular.all('prontuarios');
            $scope.resource.customGET('crudGET/' + $scope.data.id)
                .then(function (data) {
                    $scope.data = data;
                    $scope.$emit('data-loaded');
                });
        }
        function renderCharts() {
            $rootScope.$emit('update-chart', { type: 'new' });
        }

        $scope.$watchCollection('filterPront.tipo', function () {
          $scope.filterPront.tipoPreviousResults = angular.copy($scope.filterPront.tipoResults);
          $scope.filterPront.tipoResults = [];
          angular.forEach($scope.filterPront.tipo, function (value, key) {
            if (value) $scope.filterPront.tipoResults.push(key);
          });
        });
        $scope.$watchCollection('filterPront.status', function () {
          $scope.filterPront.statusPreviousResults = angular.copy($scope.filterPront.statusResults);
          $scope.filterPront.statusResults = [];
          angular.forEach($scope.filterPront.status, function (value, key) {
            if (value) $scope.filterPront.statusResults.push(key);
          });
        });
        $rootScope.$on('data-grid-updated', function(event, args) {
            if (args.type === 'exames_digitais') {
                $rootScope.$emit('update-chart', { type: 'delete' });
            }
            _loadProntuario();
        });
        $scope.$on('data-loaded', function() {
            console.log('Loading prontuario data...');
            $scope.paciente = $scope.data;

            Paciente.getProntuario({ id: $stateParams.id })
                .$promise
                .then(function (prontuarios) {
                    $scope.prontuario = _mixProntuarios(prontuarios);

                    // Get exames digitais to be plotted into dashboard
                    $scope.dashboards = [];
                    var dashboards = fwObjectService.convertDashboardExams($scope.data.exames_digitais, $scope.headers['tabs']['exames_digitais'].chart_settings);
                    dashboards.forEach(function (dashboard) {
                        $scope.dashboards.push(dashboard);
                    });
                });
        });

    });
    module.config(appConfig);

    appConfig.$inject = ['$stateProvider'];

    function appConfig($stateProvider) {
        // console.log('tango312');
        $stateProvider
            .state('app.prontuarios', {
                abstract: true,
                cache: false,
                url: '/prontuarios',
                templateUrl: 'app/modules/core/utils/crud/crud.html',
                controller: 'CRUDController',
                resolve: {
                    id: ['$stateParams', function($stateParams) {
                        return $stateParams.id;
                    }],
                    module: function() {
                        return 'prontuarios';
                    }
                }
            })
            .state('app.prontuarios.list', {
                url: '',
                templateUrl: 'app/modules/core/utils/crud/crud.list.html',
                resolve: {
                    module: function() {
                        return 'pacientes';
                    }
                }
            })
            // .state('app.prontuarios.new', {
            //     url: '/new',
            //     templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
            //     controller: 'CRUDEditController'
            // })
            .state('app.prontuarios.edit', {
                url: '/:id/edit',
                templateUrl: 'app/modules/prontuarios/prontuario.html',
                controller: 'ProntuarioController'
            })


    };
})();
