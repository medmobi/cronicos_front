(function () {
    'use strict';

    var module = angular.module('singApp.atendimentos', [
        'pascoal.crud',
        'timer'
    ]);

    // console.log('321');


    module.controller('AtendimentoController', function ($scope, Restangular, $stateParams, $timeout, $modal, module, $state, $rootScope, $q, ngToast, $http, Upload, $controller, Formulario, Paciente, Prestador, Usuario, StatusAtendimento, jQuery, fwDateTimeService, fwModalService, fwPrintService, auth, PacienteCid, PacienteExame, PacienteDocumentoMedico, fwChartService, fwStringService, PacienteExameDigital, fwDaoService, fwObjectService) {
        $controller('CRUDEditController', { $scope: $scope, Restangular: Restangular, $stateParams: $stateParams, $timeout: $timeout, $modal: $modal, module: module, $state: $state, $rootScope: $rootScope, $q: $q, ngToast: ngToast, $http: $http, Upload: Upload }); //This works

        $scope.inicio = null;
        $scope.paciente = {};
        $scope.tabDescritivos = {
            "label": "Cids",
            "label_row": "Cid",
            "fields": [

                { "name": "tipo", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Tipo de Atendimento", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "prestador", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Prestador", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "descricao", "type": "text", "notnull": true, "length": null, "precision": 10, "label": "Descrição do Atendimento", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },

            ],
            "masterdetails": []
        };
        $scope.formularios = [];
        $scope.timerRunning = true;
        $scope.canceled = false;
        $scope.finished = false;
        $scope.noBackButton = false;
        $scope.disabledFields = ['prestador'];
        $scope.encaminhamentoId = 0;
        $scope._id = 1;
        $scope.sendingEmail = false;

        $scope.goToList = goToList;
        $scope.openReceituario = openReceituario;
        $scope.openFormulario = openFormulario;
        $scope.openEncaminhamento = openEncaminhamento;
        $scope.startTimer = startTimer;
        $scope.finishAtendimento = finishAtendimento;
        // $scope.print = print;
        $scope.deleteTabItem = deleteTabItem;
        $scope.editDetail = editDetail;
        $scope.emailReceituario = emailReceituario;

        //import from prontuario
        $scope.openTabModal = openTabModal;
        $scope.print = print;
        $scope.printReceituario = printReceituario;
        $scope.printDocumento = printDocumento;
        $scope.printDocumentoById = printDocumentoById;
        $scope.clearFilter = clearFilter;

        //////////

        $scope.filterPront = {
            tipo: { Consulta: true, Retorno: true },
            status: { Finalizado: true, Cancelado: true },
            extras: { encaminhamentos: true, receitas: true, exames: true, arquivos: true, documentos: true },
            tipoResults: ['Consulta', 'Retorno'],
            statusResults: ['Finalizado', 'Cancelado'],
            tipoPreviousResults: ['Consulta', 'Retorno'],
            statusPreviousResults: ['Finalizado', 'Cancelado']
        };


        init();

        function init() {
            if ($scope.data.inicio === undefined) {
                $scope.inicio = moment();
            }
        }
        function goToList() {
          if ($state.current.name.indexOf('.list') == -1) {
            $scope.noBackButton = true;
            if($state.previous.route.name) $state.go($state.previous.route.name, $state.previous.routeParams);
            // HTML5 Alternative for development cases (auto-reload)
            else window.history.back();
          }
        }
        function openReceituario(data) {

            fwModalService.createCRUDModal($scope.headers.tabs.receituario, 'atendimentoreceituarios', data, true, 'ReceituarioNovoController', 'app/modules/prontuarios/receituario-form.html')
                .then(function (receita) {
                    if (!$scope.data.receituarios) $scope.data.receituarios = new Array();
                    else if (receita.id) {
                        $scope.data.receituarios.forEach(function (_receita, index) {
                            if (receita.id === _receita.id) $scope.data.receituarios.splice(index, 1);
                        });
                    } else {
                        receita._id = 'rec' + $scope._id;
                        $scope._id++;
                    }

                    $scope.data.receituarios.push(receita);
                    // _syncData(false);
                });

            // var modalInstance = $modal.open({
            //     animation: true,
            //     templateUrl: 'app/modules/prontuarios/receituario-form.html',
            //     controller: 'ReceituarioNovoController',
            //     resolve: {
            //         headers: {
            //             receituario: angular.copy($scope.data.receituarios),
            //             fields: _tab.fields,
            //             route: module + _tab.route,
            //             generalRoute: "/atendimentos",
            //             // route: module + '/:id/' + $scope._tab.name,
            //             label_row: _tab.label_row,
            //             label: _tab.label,
            //             tabs: {},
            //             search: _tab.search
            //         }
            //     },
            //     size: 'xl',
            // });

            // $rootScope.$$phase || $rootScope.$apply();
            //
        }
        function openFormulario(formulario) {
            // console.log(tab,data);
            var fields = [];
            var blurEvent = function (e) {
                console.log(e);
                var $scope = angular.element(this).scope();

                var indicadores = $scope.headers.formulario.indicadores;
                var indicadoresMap = [];

                _.each(indicadores, function (indicador, k) {
                    indicadoresMap[indicador.indicador.codigo] = indicador.id;
                });

                _.each(indicadores, function (indicador, k) {
                    if (indicador.indicador.tipo.id == 3) { // resultado
                        // console.log();

                        var formula = JSON.parse(indicador.indicador.formula).formula.trim();
                        // console.log('formula', formula);

                        var tokens = formula.split(/[0-9\.\(|\)|*|^|\-|\+|\/]/g);
                        // console.log('tokens', tokens);
                        var tokens_unique = tokens.filter(function onlyUnique(value, index, self) {
                            if (value.trim().length == 0) {
                                return false;
                            }
                            return self.indexOf(value) === index;
                        });

                        // console.log('tokens unique:', tokens_unique);
                        var _formula = formula;

                        var err = false;

                        _.each(tokens_unique, function (token) {
                            var _token = indicadoresMap[token];
                            if ($scope.data[_token] == undefined || $scope.data[_token] == null) {
                                err = true;
                                return;
                            }

                            _formula = _formula.replace(token, $scope.data[_token]);
                        })

                        if (!err) {
                            var result = eval(_formula);

                            $scope.data[indicadoresMap[indicador.indicador.codigo]] = result != NaN ? result : null;
                        }

                    }
                });

            };

            var _modalData = {};

            _.each(formulario.indicadores, function (indicador, key) {
                var field = {
                    name: indicador.id,
                    type: 'string',
                    label: indicador.informacao + ' (' + indicador.indicador.unidade_medida.nome + ')',
                    editable: true,
                    autocomplete: false,
                    required: true,
                    notnull: true,
                    readonly: $scope.disabled,
                    customOptions: {
                        events: {
                            blur: blurEvent
                        }
                    }
                }

                $scope.data.formulario_respostas.forEach(function (formRes, k) {
                    // console.log(formRes)
                    // console.log(indicador)
                    if (formRes.formulario_indicador.ind_id == indicador.ind_id && formulario.id == formRes.form_id) {
                        if (indicador.indicador.tipo.id !== 5) {
                            _modalData[indicador.id] = formRes.resposta;
                        }
                        // In dropdown list cases, chosen alternative must be an key-value object to be recognized by select template
                        else {
                            var _formula = JSON.parse(indicador.indicador.formula);
                            _.each(_formula.alternativas, function (alt, k) {
                                if (alt.chave == formRes.resposta) {
                                    _modalData[indicador.id] = {
                                        value: alt.legenda,
                                        option_key: alt.chave
                                    };
                                }
                            });
                        }
                    }
                });

                if (indicador.indicador.tipo.id == 1) { // number
                    field.type = 'float';

                    if (_modalData[indicador.id] != undefined) {
                        _modalData[indicador.id] = parseFloat(_modalData[indicador.id], 10);
                    }
                }
                else if (indicador.indicador.tipo.id == 2) { // string
                    field.type = 'string';
                }
                else if (indicador.indicador.tipo.id == 3) { // resultado
                    field.type = 'string'
                    field.readonly = true;
                }
                else if ([4, 5, 6].indexOf(indicador.indicador.tipo.id) != -1) { // alternativa s/n
                    var _options = {};
                    // console.log(indicador)
                    var _formula = JSON.parse(indicador.indicador.formula);
                    _.each(_formula.alternativas, function (alt, k) {
                        // console.log(alt)
                        // console.log(alt.chave)
                        _options[alt.chave] = alt.legenda;
                    });

                    // lista type (w/ dropdown)
                    if (indicador.indicador.tipo.id === 5) {
                        field.customOptions = {
                            "list": _options
                        }
                    } else {
                        field.type = 'string'
                        field.customOptions = {
                            "enum": _options
                        }
                    }

                    if (indicador.indicador.tipo.id == 6) { // multiplas escolhas
                        field.customOptions.multiple = true;
                    }
                }
                // else if (indicador.indicador.tipo.id == 5) { // lista
                //     field.type = 'tags'
                // }

                fields.push(field);
            });
            // console.log(_modalData)
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'app/modules/core/utils/crud/crud-modal.html',
                controller: 'AtendimentoFormularioController',
                resolve: {
                    data: _modalData,
                    headers: function () {
                        return {
                            fields: fields,
                            // route: module + tab.route,
                            // route: module + '/:id/' + tab.name,
                            label_row: 'Formulário ' + formulario.nome,
                            label: 'Formulário ' + formulario.nome,
                            readonly: $scope.disabled,
                            tabs: {},
                            formulario: formulario,
                            route: 'temp'
                        }
                    }
                },
                size: 'xl',
                // resolve: {
                //   items: function () {
                //     return $scope.items;
                //   }
                // }
            });
            $rootScope.$$phase || $rootScope.$apply();

            modalInstance.result.then(function (form) {
                // console.log(form)
                if ($scope.data == undefined) {
                    $scope.data = {};
                }

                // if ($scope.data.formulario_respostas == undefined) {
                //     $scope.data.formulario_respostas = {};
                // }

                // console.log()

                // $scope.formularios[formulario.id] = form;

                // var respostas = [];
                _.each(form, function (val, key) {
                    // debugger;
                    var _indicador = null;
                    formulario.indicadores.forEach(function (_ind) {
                        if (_ind.id == key) {
                            // _indicador = {
                            //     ind_id: _ind.ind_id
                            // };
                            _indicador = _ind;
                        }
                    });

                    var _pergunta = {
                        form_id: formulario.id,
                        find_id: Number(key),
                        resposta: val,
                        formulario_indicador: _indicador,
                        ate_id: $scope.data.id,
                        new: true
                    };

                    // respostas.push(_pergunta);
                    var newRespostas = [];

                    _.each($scope.data.formulario_respostas, function (resp, y) {
                        if (resp.new == true || !(resp.ate_id != undefined && _pergunta.ate_id == resp.ate_id)) {
                            newRespostas.push(resp);
                        }
                    });

                    $scope.data.formulario_respostas = newRespostas;

                    $scope.data.formulario_respostas.push(_pergunta);
                });

                // $scope.formulario_respostas[formulario.id] = true;

                _syncData(false);

            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
        }
        function openEncaminhamento(encaminhamento) {

            fwModalService
                .createCRUDModal($scope.headers.tabs.encaminhamentos, 'atendimentos', encaminhamento, true)
                .then(function (enc) {
                    if ($scope.data === undefined) {
                        $scope.data = {
                            encaminhamentos: []
                        };
                    } else if ($scope.data.encaminhamentos === undefined) {
                        $scope.data.encaminhamentos = [];
                    }

                    if (encaminhamento === undefined) {
                        $scope.encaminhamentoId++;
                        enc._id = $scope.encaminhamentoId;

                        // Checking if prestador was used by 'Adicionar Novo' and change
                        var prestador = enc['prestador.label'].label;
                        if (prestador === undefined) prestador = enc['prestador.label'];

                        // Setting attr object values to view
                        enc.nome = prestador + ' - ' + enc['especialidade.label'].label;

                        $scope.data.encaminhamentos.push(enc);
                    } else {
                        enc._id = encaminhamento.id;
                        encaminhamento = angular.copy(enc);
                    }

                });

        }
        function startTimer() {
            $scope.$broadcast('timer-start');
            $scope.timerRunning = true;
        }
        function _stopTimer() {
            // console.log('Timer Stopped - data = ', $scope.data);
            if (!$scope.finished) {
                $scope.data.fim = moment();
                $scope.data.duracao = $scope.data.minutes;
            }
            $scope.timerRunning = false;
        }
        function _syncData(finish) {
            var $_scope = $scope;

            // var response = $scope.$parent.resource.put($scope.data);
            if ($scope.data.inicio === undefined) $scope.data.inicio = $scope.inicio;

            // Adaptação técnica para escopo #558
            if ($scope.data.receituarios && $scope.data.receituarios.length) {
                $scope.data.receituarios.forEach(function (receituario) {
                    if (receituario.receituario_itens) {
                        if (typeof receituario.receituario_itens === 'object' && receituario.receituario_itens.length) {
                            receituario.receituario_itens.forEach(function (item) {
                                Object.keys(item).forEach(function (attr) {
                                    if (attr.indexOf('.label') !== -1) {
                                        if (typeof item[attr] === 'string') {
                                            var _attr = attr.split('.label')[0];

                                            item[attr] = {
                                                id: angular.copy(item[_attr + '.labelCopy'].id),
                                                label: angular.copy(item[_attr + '.labelCopy'].label)
                                            };
                                            delete item[_attr + '.labelCopy'];
                                        }
                                    }
                                });
                            });
                        }
                    }
                });
            }
            $scope.data
                .put()
                .then(function (data) {
                    var _updatedAtendimento = {
                        stat_id: $scope.data.stat_id,
                        inicio: $scope.data.inicio  // sending inicio date due to observer on atendimento.js line 17
                    };

                    // if ($scope.data.receituarios) _updatedAtendimento.receituarios = $scope.data.receituarios;
                    return $scope.data.customPUT(_updatedAtendimento);
                })
                .then(function (data) {
                    if (finish) {
                        $scope.noBackButton = true;
                        $state.go('app.prontuarios.edit', { id: $scope.data.paciente.id });
                    }
                })
                .catch(function errorCallback(error) {
                    console.log(error);
                    var messages = [];
                    function findLabel(name) {
                        for (var _x in $_scope.headers.fields) {
                            var field = $_scope.headers.fields[_x];

                            if (field.name == name) {
                                return field.label;
                            }
                        }
                    }
                    if (error.status == 422) { //hook for loopback
                        if (error.data.error.code == 'CANT_SAVE_MODEL') {
                            messages.push(error.data.error.message);
                        } else {
                            var codes = error.data.error.details.codes;

                            var friendlyErrors = {
                                presence: 'O campo %s é obrigatório',
                                absence: 'O campo %s deve ser nulo',
                                'unknown-property': 'O campo %s não foi definido',
                                length: {
                                    min: 'O campo %s é muito curto',
                                    max: 'O campo %s é muito longo',
                                    is: 'O campo %s está com tamanho inválido',
                                },
                                common: {
                                    blank: 'O campo %s está em branco',
                                    'null': 'O campo %s está nulo',
                                },
                                numericality: {
                                    'int': 'O campo %s não é um número inteiro',
                                    'number': 'O campo %s não é um número',
                                },
                                inclusion: 'O campo %s não foi incluído na lista',
                                exclusion: 'O campo %s não pode ser excluído',
                                uniqueness: 'O campo %s está repetido com o de outro registro',
                            }

                            // debugger;
                            _.each(codes, function (code, key) {
                                var _name = findLabel(key);

                                var _message = friendlyErrors[code].replace('%s', _name);
                                // debugger;
                                // console.log(code, key);
                                messages.push(_message);
                            })
                        }

                    }
                    ngToast.warning(messages.join("<br />"));
                });
        }
        function _getPrestadorFormularios(id) {
            fwDaoService.getPrestadorWithEspecialidades(id)
                .then(function (prestWithEspecialidades) {
                    // Filter especialidades objects inside PrestadorEspecialidades
                    var especialidades = [];
                    prestWithEspecialidades.especialidades.forEach(function (prestEsp) {
                        especialidades.push(prestEsp.especialidade);
                    });

                    return Restangular.all('formularios')
                        .customPOST({ "especialidades": especialidades }, 'filtered');
                })
                .then(function success(response) {
                    response.formularios.forEach(function (form, k) {
                        response.formularios[k].active = $scope.data.formulario_respostas.some(function (resposta) {
                            return form.id === resposta.form_id;
                        });
                    });
                    $scope.formularios = response.formularios;
                })
                .catch(function error(err) {
                    console.error(err);
                });
        }
        function finishAtendimento(type, finish) {
            StatusAtendimento
                .findOne({
                    filter: {
                        where: {
                            nome: type
                        }
                    }
                })
                .$promise
                .then(function success(status) {
                    $scope.data.status = status;
                    $scope.data.stat_id = status.id;
                    _stopTimer();
                    _syncData(finish);
                });
        }
        function _changeToReadonly(types) {
            // Disabled form inputs and buttons
            var inputEls = jQuery('div.input-container :input');
            if (!types) {
                inputEls.each(function (index) {
                    jQuery(this).attr('disabled', true);
                });

                // Disabled formularios links
                $scope.disabled = true;
            }
            // Disable form specified array of inputs and right button along
            else {
                types.forEach(function (type) {
                    var inputNextBtn = false;
                    inputEls.each(function (index) {
                        if (jQuery(this).attr('name') === type) {
                            inputNextBtn = true;
                            jQuery(this).attr('disabled', true);
                            return;
                        }

                        // if(inputNextBtn) {
                        //   inputNextBtn = false;
                        //   jQuery(this).attr('disabled', true);
                        // }
                    });
                });
            }
        }
        // function print() {
        //   var user = auth.getUser();
        //   if (user !== null) {
        //     Usuario
        //       .findOne({
        //         filter: {
        //           where: {
        //             id: user.id
        //           },
        //           include: ['empresa', 'prestador']
        //         }
        //       })
        //       .$promise
        //       .then(function (user) {
        //         fwPrintService.printReceituario($scope.data.receituarios, $scope.paciente, user.empresa, user.prestador);
        //       });
        //   }
        // }
        function deleteTabItem(type, obj) {
            // console.log(obj)
            if (obj.id) {
                var _resource = Restangular.all('atendimentos');
                if (type === 'receituarios') {
                    _resource.customDELETE('details/' + $scope.data.id + '/' + type + '/' + obj.id).then(function () {
                        $scope.data.receituarios.forEach(function (_receita, index) {
                            if (obj.id === _receita.id) {
                                $scope.data.receituarios.splice(index, 1);
                                alert('Receita removida com sucesso!');
                            }
                        });
                    });
                }
                else if (type === 'encaminhamentos') {
                    _resource.customDELETE('details/' + $scope.data.id + '/' + type + '/' + obj.id).then(function () {
                        $scope.data.encaminhamentos.forEach(function (_enc, index) {
                            if (obj.id === _enc.id) {
                                $scope.data.encaminhamentos.splice(index, 1);
                                alert('Encaminhamento removida com sucesso!');
                            }
                        });
                    });
                }
            }
            else {
                if (type === 'receituarios') {
                    $scope.data.receituarios.forEach(function (_receita, index) {
                        if (obj.id === _receita.id) $scope.data.receituarios.splice(index, 1);
                    });
                } else if (type === 'encaminhamentos') {
                    $scope.data.encaminhamentos.forEach(function (_enc, index) {
                        if (obj.id === _enc.id) $scope.data.encaminhamentos.splice(index, 1);
                    });
                }
            }
        }

        function _dataLoaded() {
            // console.log($scope.data)
            var pacienteID = $scope.data.paciente.id;

            // Getting paciente
            Paciente.crudGET({ id: pacienteID }, function (_paciente) {
                $scope.paciente = (_paciente);
                // $scope.data.pac_id = pacienteID;
            });

            // retrieve prontuário
            Paciente.getProntuario({ id: pacienteID })
                .$promise
                .then(function (prontuarios) {
                    // console.log(prontuarios)
                    $scope.prontuario = _mixProntuarios(prontuarios);
                });

            // Encaminhamentos verifications
            if ($scope.data.encaminhamentos !== undefined) {
                $scope.data.encaminhamentos.forEach(function (enc) {
                    enc._id = enc.id
                    // Setting attr object values to view
                    enc.nome = enc.prestador.nome + ' - ' + enc.especialidade.descricao;
                });
            }

            // Receituario inner receita itens conversions
            if ($scope.data.receituarios !== undefined) {
                $scope.data.receituarios.forEach(function (receita) {
                    if (receita.receituario_itens.length > 0) {
                        // Set _id for ng-repeat
                        receita._id = receita.id;
                        receita.receituario_itens.forEach(function (receitaItem) {
                            Object.keys(receitaItem).forEach(function (attr) {
                                if (receitaItem[attr]) {
                                    if (typeof receitaItem[attr] === 'object') {
                                        var _receitaItemType = {};
                                        Object.keys(receitaItem[attr]).forEach(function (_attr) {
                                            if (_attr === 'Nome' || _attr === 'nome' || _attr === 'nome_apresentacao') {
                                                _receitaItemType['label'] = receitaItem[attr][_attr];
                                                _receitaItemType['nome'] = receitaItem[attr][_attr];
                                            } else _receitaItemType[_attr] = receitaItem[attr][_attr];
                                        });
                                        receitaItem[attr + '.label'] = _receitaItemType;
                                        receitaItem[attr] = receitaItem[attr].id;
                                    }
                                    else if (attr.indexOf("_id") !== -1) {
                                        delete receitaItem[attr];
                                    }
                                }
                            });
                        });
                    }
                });
            }
            // Formularios filling
            // if($scope.data.formulario_respostas) {
            //   $scope.formularios = fwModelService.respostasToFormularios($scope.data.formulario_respostas);
            // }

            // Prestador verifications for 'Em Atendimento' cases
            if ($scope.data.prestador) {
                _getPrestadorFormularios($scope.data.prestador.id);
            }
            else {
                Formulario
                    .find({
                        filter: {
                            include: [
                                { 'indicadores': { 'indicador': ['tipo', 'unidade_medida'] } },
                                { 'especialidades': { 'especialidade': [] } }
                            ],
                            where: {
                                geral: true
                            }
                        }
                    })
                    .$promise
                    .then(function success(formularios) {
                        response.formularios.forEach(function (form, k) {
                            response.formularios[k].active = $scope.data.formulario_respostas.some(function (resposta) {
                                return form.id === resposta.form_id;
                            });
                        });
                        $scope.formularios = response.formularios;
                    })
                    .catch(function error(err) {
                        console.error(err);
                    });
            }

            // Verification depending on status type
            if ($scope.data.status.nome === 'Cancelado') _changeToReadonly();
            else if ($scope.data.status.nome === 'Finalizado') {
                $scope.finished = true;
                _changeToReadonly($scope.disabledFields);
            }
            else {
                _changeToReadonly($scope.disabledFields);
            }

            // Disable form after 24 hours of edit
            var diffDuration = fwDateTimeService.getDiffDuration(moment(), moment($scope.data.fim), 'hour');
            if (diffDuration >= 24) {
                // console.log('não pode mais editar')
                _changeToReadonly();
            }

        }

        $scope.$on('data-loaded', _dataLoaded);
        $scope.$on('selected-autocomplete', function (event, res) {
            if (res.type === 'prestador') {
                console.log('Getting prestador formularios...')
                _getPrestadorFormularios(res.data.id);
            }
        });
        $scope.$on('$destroy', function () {
            if (!$scope.noBackButton) {
                console.log('Returning with back button...');
                if (!$scope.disabled && !$scope.finished) {
                    console.log('Atendimento ainda em realização. Cancelando o mesmo...');
                    $scope.finishAtendimento('Cancelado', false);
                }
            }
        });

        $scope.$watchCollection('filterPront.tipo', function () {
            $scope.filterPront.tipoPreviousResults = angular.copy($scope.filterPront.tipoResults);
            $scope.filterPront.tipoResults = [];
            angular.forEach($scope.filterPront.tipo, function (value, key) {
                if (value) $scope.filterPront.tipoResults.push(key);
            });
        });
        $scope.$watchCollection('filterPront.status', function () {
            $scope.filterPront.statusPreviousResults = angular.copy($scope.filterPront.statusResults);
            $scope.filterPront.statusResults = [];
            angular.forEach($scope.filterPront.status, function (value, key) {
                if (value) $scope.filterPront.statusResults.push(key);
            });
        });
        $rootScope.$on('data-grid-deleted', function () {
            _loadProntuario();
        });

        function _mixProntuarios (prontuarios) {
            var _newList = angular.copy(prontuarios);

            // Add multiple label on consultas and retornos
            _newList.forEach(function (el) {
                el.multiple = true;
            });

            // Add encaminhamentos and receituario to prontuario list
            prontuarios.forEach(function (prontuario) {
                if (prontuario.encaminhamentos.length > 0) {
                    prontuario.encaminhamentos.forEach(function (enc) {
                        enc.inicio = enc.createdAt;
                        enc.fim = enc.updatedAt;
                        enc.status = { nome: 'Finalizado' };
                        enc.label = 'encaminhamentos';
                        _newList.push(enc);
                    });
                }
                else if (prontuario.receituarios.length > 0) {
                    prontuario.receituarios.forEach(function (rec) {
                        rec.inicio = rec.createdAt;
                        rec.fim = rec.updatedAt;
                        rec.status = { nome: 'Finalizado' };
                        rec.label = 'receituario';
                        rec.prestador = prontuario.prestador;
                        _newList.push(rec);
                    });
                }
            });

            // Add arquivos to prontuario list
            if ($scope.data.exames && $scope.data.exames.length > 0) {
                $scope.data.exames.forEach(function (exam) {
                    exam.inicio = exam.createdAt;
                    exam.fim = exam.updatedAt;
                    exam.status = { nome: 'Finalizado' };
                    exam.label = 'arquivos';
                    _newList.push(exam);
                });
            }

            // Add exames to prontuario list
            if ($scope.data.exames_digitais && $scope.data.exames_digitais.length > 0) {
                $scope.data.exames_digitais.forEach(function (exam) {
                    exam.inicio = exam.createdAt;
                    exam.fim = exam.updatedAt;
                    exam.status = { nome: 'Finalizado' };
                    exam.label = 'exames';
                    _newList.push(exam);
                });
            }

            // Add documentos medicos to prontuario list
            if ($scope.data.documentos_medicos && $scope.data.documentos_medicos.length > 0) {
                $scope.data.documentos_medicos.forEach(function (doc) {
                    doc.inicio = doc.createdAt;
                    doc.fim = doc.updatedAt;
                    doc.status = { nome: 'Finalizado' };
                    doc.label = 'documentos';
                    _newList.push(doc);
                });
            }

            return _newList;
        }

        function openTabModal (type) {
            if (type == 'exames_digitais') {
                // fwModalService.createCRUDModal($scope.headers['tabs'][type], $scope.headers.parent_route.toLowerCase(), null, true, 'ExameDigitalCtrl')
                fwModalService.createCRUDModal($scope.headers['tabs'][type], $scope.headers.parent_route.toLowerCase(), null, true, 'ExameDigitalCtrl', 'app/modules/prontuarios/exames-digitais-form.html')
                    .then(function (response) {
                        switch (type) {
                            case 'exames_digitais':
                                var promises = [];
                                response.forEach(function (exame) {
                                    exame.pac_id = $scope.paciente.id;
                                    promises.push(PacienteExameDigital.create(response));
                                });
                                return $q.all(promises);
                            default:
                                console.error("Tab modal not implemented yet.");
                        }
                    })
                    .then(function () {
                        $timeout(function () {
                            $rootScope.$broadcast('refreshGRID');
                            _dataLoaded();
                            // $rootScope.$emit('update-chart', { type: 'new' });
                        }, 500);
                    });
            }
            else if (type === 'documentos-medicos') {
                auth.isPrestador()
                    .then(function success(user) {
                        var _doc = {};
                        _doc.prestador = user.prestador;
                        _doc['prestador.label'] = user.prestador.nome;
                        return fwModalService.createCRUDModal($scope.headers['tabs'][type], $scope.headers.parent_route.toLowerCase(), _doc, true);
                    })
                    .then(function (response) {
                        switch (type) {
                            case 'documentos-medicos':
                                response.pac_id = $scope.paciente.id;
                                return PacienteDocumentoMedico.create(response);
                            default:
                                console.error("Tab modal not implemented yet.");
                        }
                    })
                    .then(function () {
                        $timeout(function () {
                            $rootScope.$broadcast('refreshGRID');
                            if (type === 'exames' || type === 'documentos-medicos')
                                _dataLoaded();
                        }, 500);
                    })
                    .catch(function error(status) {
                        if (!status) {
                            // User isn't prestador
                            fwModalService.createCRUDModal($scope.headers['tabs'][type], $scope.headers.label.toLowerCase(), null, true)
                                .then(function (response) {
                                    switch (type) {
                                        case 'documentos-medicos':
                                            response.pac_id = $scope.paciente.id;
                                            return PacienteDocumentoMedico.create(response);
                                        default:
                                            console.error("Tab modal not implemented yet.");
                                    }
                                })
                                .then(function () {
                                    $timeout(function () {
                                        $rootScope.$broadcast('refreshGRID');
                                        if (type === 'exames' || type === 'documentos-medicos')
                                            _dataLoaded();
                                    }, 500);
                                })
                        }
                    });
            }
            else if (type === 'formulario') {
                var formulario = { prestador: $scope.data.prestador.id, respostas: $scope.data.formulario_respostas };
                fwModalService.createCRUDModal($scope.headers['tabs'][type], $scope.headers.parent_route.toLowerCase(), formulario, true, 'FormularioCtrl')
                    .then(function (response) {
                        switch (type) {
                            case 'formulario':
                                _editFormularioResponses(response);
                                return $scope.data.put();
                            default:
                                console.error("Tab modal not implemented yet.");
                        }
                    })
                    .then(function (response) {
                      $timeout(function () {
                          // console.log(response)
                          // $scope.data = response;
                          $rootScope.$broadcast('refreshGRID');
                          _dataLoaded();
                      }, 500);
                    });
            }
            else {
                fwModalService.createCRUDModal($scope.headers['tabs'][type], $scope.headers.parent_route.toLowerCase(), null, true)
                    .then(function (response) {
                        switch (type) {
                            case 'exames':
                                response.pac_id = $scope.paciente.id;
                                return PacienteExame.create(response);
                            case 'cid':
                                return PacienteCid.create({ cid_id: response.cid, pac_id: $scope.paciente.id });
                            case 'documentos-medicos':
                                response.pac_id = $scope.paciente.id;
                                return PacienteDocumentoMedico.create(response);
                            default:
                                console.error("Tab modal not implemented yet.");
                        }
                    })
                    .then(function () {
                        $timeout(function () {
                            $rootScope.$broadcast('refreshGRID');
                            if (type === 'exames' || type === 'documentos-medicos')
                                _dataLoaded();
                        }, 500);
                    });
            }
        }

        function _print (type, data) {
            var user = auth.getUser();
            if (user !== null) {
                Usuario
                    .findOne({
                        filter: {
                            where: {
                                id: user.id
                            },
                            include: [
                                {
                                    'prestador': [ 'conselho', 'conselho_uf']
                                },
                                {
                                    'empresa': [ 'telefones', 'enderecos' ]
                                }
                            ]
                        }
                    })
                    .$promise
                    .then(function (user) {
                        switch (type) {
                            case 'receituario':
                                fwPrintService.printReceituario(data, $scope.paciente, user.empresa, user.prestador);
                                break;
                            case 'documento':
                                var _data = angular.copy(data);
                                _data.texto = fwStringService.changePlaceholders(_data.texto, { paciente: $scope.paciente });
                                fwPrintService.printDocumento(_data, $scope.paciente, user.empresa, user.prestador);
                                break;
                            case 'prontuario':
                                fwPrintService.printProntuario($scope.prontuario, $scope.paciente, user.empresa, user.prestador);
                                break;
                            default:
                                console.error("Type not available for print.");
                        }
                    });
            }
        }
        function print () {
          _print('prontuario', null);
        }
        function printReceituario (data) {
            var _receituario = angular.copy(data);
            _receituario.receituario_itens = fwObjectService.convertObjLabels(_receituario.receituario_itens);
            _print('receituario', _receituario);
        }
        function printDocumento (data) {
          _print('documento', data);
        }
        function printDocumentoById (id) {
            PacienteDocumentoMedico.findOne({
                  filter: {
                      where: {
                          id: id
                      },
                      include: [
                          {
                              'prestador': [ 'conselho', 'conselho_uf']
                          },
                          'documento_tipo'
                      ]
                  }
                })
                .$promise
                .then(function (doc) {
                    _print('documento', doc);
                });
        }
        function clearFilter () {
            $scope.filterPront = {
                tipo: { Consulta: false, Retorno: false },
                status: { Finalizado: false, Cancelado: false },
                extras: { encaminhamentos: false, receitas: false, exames: false, arquivos: false, documentos: false },
                tipoResults: [],
                statusResults: [],
                tipoPreviousResults: $scope.filterPront.tipoResults,
                statusPreviousResults: $scope.filterPront.statusResults
            };
        }
        function _editFormularioResponses (response) {
            // response: { data: { 1: '123' }, formulario: { ... } }
            _.each(response.data, function (val, key) {
                var _indicador = null;
                response.formulario.indicadores.forEach(function (_ind) {
                    if (_ind.id == key) {
                        _indicador = _ind;
                    }
                });

                var _pergunta = {
                    form_id: response.formulario.id,
                    find_id: Number(key),
                    resposta: val,
                    formulario_indicador: _indicador,
                    ate_id: $scope.data.id,
                    new: true
                };

                // respostas.push(_pergunta);
                var newRespostas = [];

                var _formRespostasCopy = angular.copy($scope.data.formulario_respostas);

                _.each(_formRespostasCopy, function (resp, index) {
                    // if (resp.new == true || !(resp.ate_id != undefined && _pergunta.ate_id == resp.ate_id)) {
                    if (_pergunta.form_id != resp.form_id) {
                        newRespostas.push(resp);
                    }
                    // }
                });

                // console.log($scope.data.formulario_respostas)

                $scope.data.formulario_respostas = newRespostas;

                $scope.data.formulario_respostas.push(_pergunta);
            });
        }
        function _sendEmail (type, data) {
            $scope.sendingEmail = true;
            var user = auth.getUser();
            if (user !== null) {
                Usuario
                    .findOne({
                        filter: {
                            where: {
                                id: user.id
                            },
                            include: [
                                {
                                    'prestador': [ 'conselho', 'conselho_uf']
                                },
                                {
                                    'empresa': [ 'telefones', 'enderecos' ]
                                }
                            ]
                        }
                    })
                    .$promise
                    .then(function (user) {
                        var document = { type: type, receituario: data, paciente: $scope.paciente, empresa: user.empresa, prestador: user.prestador };
                        return Usuario.sendDocumentEmail(document).$promise;
                    })
                    .then(function (res) {
                        var messages = [];
                        if (res.accepted && res.accepted.length > 0) {
                            messages.push('E-mail da receita enviado com sucesso para os seguintes e-mails: ');
                            res.accepted.forEach(function (email) {
                                messages.push(email);
                            });
                        }
                        if (messages.length > 0) ngToast.success(messages.join("<br />"));
                        $scope.sendingEmail = false;
                    })
                    .catch(function (err) {
                        $scope.sendingEmail = false;
                        ngToast.warning('O e-mail não pode ser enviado! Tente novamente mais tarde.');
                    });
            }
        }
        function emailReceituario (receituario) {
            var _receituario = angular.copy(receituario);
            _receituario.receituario_itens = fwObjectService.convertObjLabels(_receituario.receituario_itens);
            _sendEmail('receituario', _receituario);
        }

        /**
         * @override editDetail (crud.module.js > CRUDEditController)
         * @param route
         * @param row
         * @param detail_key
         */
        function editDetail (route, row, detail_key) {
            var origin = jQuery('.button-new').attr('origin');
            var headers = $scope.headers[origin][detail_key];
            var parentModel = $scope.headers.route.toLowerCase();
            if ($scope.headers.parent_route) parentModel = $scope.headers.parent_route;

            var controller = null;
            if (headers.label === 'Formulários') {
                controller = 'FormularioCtrl';
                row.prestador = $scope.data.prestador.id;
                row.respostas = $scope.data.formulario_respostas;
                row.editDetail = true;
            }

            fwModalService.createCRUDModal(headers, parentModel, row, true, controller)
                .then(function (response) {

                    // _data = { data: { 1: '123' }, formulario: _formulario };
                    var _data = response;
                    if (headers.label === 'Formulários') {
                        _editFormularioResponses(response);
                        $scope.data.put()
                            .then(function () {
                                $rootScope.$broadcast('refreshGRID');
                                $rootScope.$broadcast('data-grid-updated', { type: route.split('/').pop() });
                                _dataLoaded();
                            });
                    }

                    var resource = Restangular.all(route);
                    resource.customPUT(response, row.id).then(function (data) {
                        // $state.go($scope.$parent.path + '.list');
                        $rootScope.$broadcast('refreshGRID');
                        $rootScope.$broadcast('data-grid-updated', { type: route.split('/').pop() });
                    });
                });
        }
    });

    module.controller('AtendimentoReceituarioController', function ($scope, Restangular, $stateParams, $timeout, $modal, module, $state, $rootScope, $q, ngToast, $http, Upload, $controller) {
        // function ($scope, Restangular, $stateParams, $timeout, headers, $rootScope, $modalInstance, $q, ngToast, $controller) {

        $controller('CRUDEditController', { $scope: $scope, Restangular: Restangular, $stateParams: $stateParams, $timeout: $timeout, $modal: $modal, module: module, $state: $state, $rootScope: $rootScope, $q: $q, ngToast: ngToast, $http: $http, Upload: Upload });
        // $controller('CRUDEditDetailController', { $scope: $scope, Restangular: Restangular, $stateParams: $stateParams, $timeout: $timeout, headers: headers, $rootScope: $rootScope, $modalInstance: $modalInstance, $q: $q, ngToast: ngToast });

        // $scope.detail = detail;
        $scope.detail_key = "receituarios";
        $scope.detail = "receituarios";
        $scope.detail = {
            label: 'Receituários',
            label_row: 'Item',
            fields: [
                { "name": "medicamento", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Medicamento", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "qtd", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Qtd.", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "posologia", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Posologia", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "dosagem", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Dosagem", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "unidade_medida", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "U. Medida", "editable": true, "viewable": false, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "via", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Via", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "frequencia", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Frequência", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "duracao", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Duração", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "obs", "type": "text", "notnull": true, "length": null, "precision": 10, "label": "Obs:", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
            ],
            route: "/details/receituarios",
            origin: "tabs"

        };
        $scope.tab = $scope.detail;

        $scope.newReceituarioItem = function (tab, data) {
            // console.log(tab,data);

            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'app/modules/prontuarios/receituario-form.html',
                controller: 'ReceituarioNovoController',
                resolve: {
                    headers: {
                        fields: tab.fields,
                        route: module + tab.route,
                        // route: module + '/:id/' + tab.name,
                        label_row: tab.label_row,
                        label: tab.label,
                        tabs: {}
                    }
                },
                size: 'xl',
                // resolve: {
                //   items: function () {
                //     return $scope.items;
                //   }
                // }
            });

            $rootScope.$$phase || $rootScope.$apply();

            modalInstance.result.then(function (selectedItem) {
                // $scope.selected = selectedItem;

            }, function () {
                // console.info('Modal dismissed at: ' + new Date());
            });
        };
    });

    module.controller('ReceituarioNovoController', function ($scope, Restangular, $stateParams, $timeout, headers, $rootScope, $modalInstance, $q, ngToast, $controller, PreReceitaItem, fwObjectService, $state, $http, Upload, $modal, parentModel, data, fwStringService, autocompleteDetail) {
        $controller('CRUDFormModalController', { $controller: $controller, $scope: $scope, $modalInstance: $modalInstance, ngToast: ngToast, headers: headers, Restangular: Restangular, $stateParams: $stateParams, $timeout: $timeout, $state: $state, $rootScope: $rootScope, $q: $q, $http: $http, Upload: Upload, $modal: $modal, parentModel: parentModel, data: data, fwStringService: fwStringService, autocompleteDetail: autocompleteDetail });

        var _receitaItemFields = headers.masterdetails.receitaItem.fields;
        $scope.fields = _receitaItemFields[0].concat(_receitaItemFields[1], _receitaItemFields[2]);
        $scope.receitaItens = [];

        $scope.deleteItem = deleteItem;
        $scope.editItem = editItem;
        $scope.save = save;
        $scope.submit = submit;

        //////////

        _init();

        function _init () {
            $scope.data = {};
            eventDropdownButton();
            if (data) {
                if (data.nome) {
                    $timeout(function () {
                        var _nomeScope = angular.element('#nome').scope();
                        _nomeScope.$parent.data.nome = data.nome;
                    }, 500)
                }
                if (data.data) {
                  $scope.data.data = data.data;
                }
                if (data.id) {
                    $scope.data.id = data.id;
                }
                if (data.receituario_itens) $scope.receitaItens = data.receituario_itens;
            }
        }
        function eventDropdownButton() {
            $timeout(function () {
                jQuery(".input-group-btn.ng-scope").on("click", function () {
                    jQuery(this).parent().children('input').click();
                });
            });
        }
        function deleteItem (index) {
            $scope.receitaItens.splice(index, 1);
        }
        function editItem (index) {
            var _item = $scope.receitaItens[index];
            fwObjectService.setInputsFromObject(_item);
            $scope.deleteItem(index);
        }
        function save () {
            var _receitaItem = angular.copy($scope.data);
            delete _receitaItem['nome'];
            $scope.receitaItens.push(_receitaItem);

            // Clear receituario items form inputs
            $scope.fields.forEach(function (field) {
                angular.element('#' + field.name).val('');
            });

            // Clear medicamento autocomplete table input
            angular.element('#input-ac-table').val('');

            $timeout(function () {
                angular.element('.field-medicamento').closest('form').parsley().reset();
            });
        }
        /**
         * @override CRUDFormModalController.submit
         */
        function submit () {
            if ($scope.data.nome !== undefined && $scope.data.data !== undefined && $scope.receitaItens.length > 0) {
                var _data = {
                    nome: $scope.data.nome,
                    data: $scope.data.data,
                    receituario_itens: $scope.receitaItens
                };
                if ($scope.data.id) {
                    _data.id = $scope.data.id;
                    _data._id = $scope.data.id;
                }
                $modalInstance.close(_data);
            }
            else {
                // debugger;
                var messages = [];
                if ($scope.data.nome === undefined) messages.push('O campo nome é obrigatório!');
                if ($scope.data.data === undefined) messages.push('O campo data é obrigatório!');
                if ($scope.receitaItens.length === 0) messages.push('Insira pelo menos um item na receita!');
                if (messages.length > 0) ngToast.warning(messages.join("<br />"));
            }
        }

        $scope.$on('selected-autocomplete', function (event, res) {
            if (res.type === 'prereceita') {
                PreReceitaItem
                    .find ({
                        filter: {
                            where: { prei_id: res.data.id },
                            include: [ 'medicamento', 'posologia', 'unidade_medida', 'via', 'frequencia', 'duracao']
                        }
                    })
                    .$promise
                    .then (function (data) {
                      console.log(data)
                        $scope.receitaItens = new Array();
                        data.forEach (function (dataObj) {
                            $scope.receitaItens.push(fwObjectService.convertObjSearch(dataObj, { 'relation': 'medicamento', 'label': 'nome_apresentacao' }));
                        });
                        var _nomeScope = angular.element('#nome').scope();
                        _nomeScope.$parent.data.nome = res.data.label;
                    });
            }
        });
    });

    module.controller('FormularioCtrl', function ($scope, Restangular, $stateParams, $timeout, headers, $rootScope, $modalInstance, $q, ngToast, $controller, PreReceitaItem, fwObjectService, $state, $http, Upload, $modal, parentModel, data, fwStringService, autocompleteDetail, fwDaoService, fwDbTypesService, fwErrorService, fwComparatorService) {
        $controller('CRUDFormModalController', { $controller: $controller, $scope: $scope, $modalInstance: $modalInstance, ngToast: ngToast, headers: headers, Restangular: Restangular, $stateParams: $stateParams, $timeout: $timeout, $state: $state, $rootScope: $rootScope, $q: $q, $http: $http, Upload: Upload, $modal: $modal, parentModel: parentModel, data: data, fwStringService: fwStringService, autocompleteDetail: autocompleteDetail });

        $scope.autocompleteDetail = autocompleteDetail;
        $scope.submit = submit;

        ////////

        init();

        function init () {
            fwDaoService.getPrestadorWithEspecialidades($scope.data.prestador)
                .then(function (prestWithEspecialidades) {
                    $scope.data.especialidades = [];

                    // Filter especialidades objects inside PrestadorEspecialidades
                    prestWithEspecialidades.especialidades.forEach(function (prestEsp) {
                        $scope.data.especialidades.push(prestEsp.especialidade);
                    });
                });
        }

        /**
         * @override autocompleteDetail (crud.module.js > CRUDEditController)
         * @param detail
         * @param field
         * @param val
         * @returns {*}
         */
        function autocompleteDetail (detail, field, val) {

            var queries = [];

            var deferred = $q.defer();

            if (field.autocomplete_dependencies.length > 0) {
                var deps = field.autocomplete_dependencies;
                for (var x in deps) {
                    var dep = deps[x];
                    if ($scope.data[dep.field] == undefined || $scope.data[dep.field] == null) {
                        // window.alert('missing ' + dep);
                        //            var text = 'missing ' + dep.field;


                        var text = 'Selecione antes o(a) ' + dep.label;

                        var data = [];
                        data.push({ id: -1, label: text });

                        deferred.resolve(data);

                        return deferred.promise;
                    } else {
                        queries[dep.field] = $scope.data[dep.field];
                        // queries.push(dep + "=" + $scope.data[dep]);
                    }

                  // console.log("?" + queries.join("&"));
                }
            }

            val = val.trim();
            if (val.length == 0 || field.customOptions.select == true) {
                val = '[blank]';
            }

            if (field.customOptions.general !== undefined) {
                  if (field.name === 'formulario') {
                      fwDaoService.getPrestadorFormulariosFilteredByEspecialidades(queries.especialidades, true)
                          .then(function (data) {
                              data = data.formularios;
                              if (field.quickAdd === true && val != '[blank]') {
                                  data.push({ id: -1, label: 'Adicionar novo: ' + val });
                              }

                              deferred.resolve(data);
                          }, function errorCallback() {
                              return deferred.reject();
                          });
                  }

                  else {
                      $scope.resource.customGET('general/autocomplete/' + field.customOptions.general + '/' + val, queries).then(function (data) {

                          if (field.quickAdd === true && val != '[blank]') {
                              data.push({ id: -1, label: 'Adicionar novo: ' + val });
                          }


                          deferred.resolve(data);

                      }, function errorCallback() {
                          return deferred.reject();
                      });
                  }
            } else $scope.resource.customGET('details/' + detail + '/autocomplete/' + field.name + '/' + val, queries).then(function (data) {
                if (field.quickAdd && val != '[blank]') {
                    data.push({ id: -1, query: val, label: 'Adicionar novo: ' + val });
                }

                deferred.resolve(data);

            }, function errorCallback() {
                return deferred.reject();
            });

            return deferred.promise;
        }

        /**
         * @override submit (crud.module.js > CRUDFormModalController)
         */
        function submit () {

            var _formulario;
            if (typeof $scope.data['formulario.label'] === 'object') {
                _formulario = $scope.data['formulario.label'];
            }
            else _formulario = $scope.data.formulario;

            var _data = { data: {}, formulario: _formulario };
            if (typeof $scope.data.resposta === 'object') _data.data[_formulario.id] = $scope.data.resposta.option_key;
            else _data.data[_formulario.id] = $scope.data.resposta;

            if (this.crudForm.$valid) {
                $modalInstance.close(_data);
            } else {
                // debugger;
                fwErrorService.emitFormErrors(this.crudForm)
            }
        }

        function _loadFormulario (edit) {
            $scope.headers.fields.forEach(function (field) {
                if (field.name === 'resposta') {
                    field.label = 'Resposta';
                    field.customOptions = {};
                }
            });

            var _formulario = {};
            if (typeof $scope.data['formulario.label'] === 'object') _formulario = $scope.data['formulario.label'];
            else _formulario = $scope.data.formulario;

            if (_formulario) {
                if (!edit) {
                    var _hasAnswer = false;
                    $scope.data.respostas.forEach(function (resposta) {
                        if (_formulario.id === resposta.form_id) {
                            _hasAnswer = true;
                            return;
                        }
                    });
                    if (!_hasAnswer) {
                        _loadFormularioRespostas(_formulario, false);
                    }
                    else {
                        ngToast.warning("Esse formulário já possui uma resposta!");
                        $scope.headers.fields.forEach(function (field) {
                            if (field.name === 'resposta') {
                                field.readonly = true;
                                field.customOptions = {};
                            }
                        });
                    }
                }
                else {
                    fwDaoService.getFormularioByIdWithIndicadores(_formulario.id)
                        .then(function (formulario) {
                            // Edit main object with formulario w/ indicadores
                            $scope.data.formulario.indicadores = formulario.indicadores;
                            _loadFormularioRespostas(formulario, true);
                        });
                }
            }
        }

        function _loadFormularioRespostas (form, edit) {
            // Edit modal field structure
            var indicador = form.indicadores[0];
            if (indicador) {
                $scope.headers.fields.forEach(function (field) {
                    // Modify field resposta structure
                    if (field.name === 'resposta') {
                        field.readonly = false;
                        field.customOptions = {
                            events: {}
                        };

                        if (indicador.indicador.tipo.nome === 'Numérico') field.label = indicador.indicador.descricao + ' (' + indicador.indicador.unidade_medida.nome + ')';
                        else field.label = indicador.informacao + ' (' + indicador.indicador.unidade_medida.nome + ')';

                        var blurEvent = function (e) {
                            var $scope = angular.element(this).scope();

                            var indicadores = form.indicadores;
                            var indicadoresMap = [];

                            _.each(indicadores, function (indicador, k) {
                                indicadoresMap[indicador.indicador.codigo] = indicador.id;
                            });

                            _.each(indicadores, function (indicador, k) {
                                if (indicador.indicador.tipo.id == 3) { // resultado
                                    // console.log();

                                    var formula = JSON.parse(indicador.indicador.formula).formula.trim();
                                    // console.log('formula', formula);

                                    var tokens = formula.split(/[0-9\.\(|\)|*|^|\-|\+|\/]/g);
                                    // console.log('tokens', tokens);
                                    var tokens_unique = tokens.filter(function onlyUnique(value, index, self) {
                                        if (value.trim().length == 0) {
                                            return false;
                                        }
                                        return self.indexOf(value) === index;
                                    });

                                    // console.log('tokens unique:', tokens_unique);
                                    var _formula = formula;

                                    var err = false;

                                    _.each(tokens_unique, function (token) {
                                        var _token = indicadoresMap[token];
                                        if ($scope.data[_token] == undefined || $scope.data[_token] == null) {
                                            err = true;
                                            return;
                                        }

                                        _formula = _formula.replace(token, $scope.data[_token]);
                                    });

                                    if (!err) {
                                        var result = eval(_formula);

                                        $scope.data[indicadoresMap[indicador.indicador.codigo]] = result != NaN ? result : null;
                                    }
                                }
                            });
                        };

                        field.customOptions.events.blur = blurEvent;
                        if (fwDbTypesService.getIndicadorExamAlternativas().indexOf(indicador.indicador.tipo.nome) !== -1) {
                            var _options = {};
                            var _formula = JSON.parse(indicador.indicador.formula);
                            _formula.alternativas.forEach(function (alt, k) {
                                _options[alt.chave] = alt.legenda;
                                // Edit case: change legenda to chave in order to check an option
                                if (edit) {
                                    if (alt.legenda === $scope.data.resposta) {
                                        if (indicador.indicador.tipo.nome === 'Lista') {
                                            $scope.data.resposta = {
                                              option_key: alt.chave,
                                              value: alt.legenda
                                            };
                                        } else {
                                            $scope.data.resposta = alt.chave;
                                        }
                                    }
                                }
                            });

                            // lista type (w/ dropdown)
                            if (indicador.indicador.tipo.nome === 'Lista') {
                                field.customOptions.list = _options;
                            } else {
                                field.type = 'string';
                                field.customOptions.enum = _options;
                            }

                            if (indicador.indicador.tipo.nome === 'Múltiplas Alternativas') { // multiplas escolhas
                                field.customOptions.multiple = true;
                            }
                        }
                        else {
                            field.customOptions = {};
                        }

                        field.customOptions.events = { blur: blurEvent };
                    }
                });
            }
        }

        $scope.$watch('data.formulario', function(_new, _old) {
            // console.log(_new, _old, $scope.data)
            _loadFormulario($scope.data.editDetail);
        });

        // $scope.$watch('data.resposta', function(_new, _old) {
        //     console.log(_new, _old)
        // });
    });

    module.controller('AtendimentoFormularioController', function ($scope, Restangular, $stateParams, $timeout, headers, $rootScope, $modalInstance, $q, ngToast, $controller, data, fwErrorService) {
        $controller('CRUDEditDetailController', { $scope: $scope, Restangular: Restangular, $stateParams: $stateParams, $timeout: $timeout, headers: headers, $rootScope: $rootScope, $modalInstance: $modalInstance, $q: $q, ngToast: ngToast });

        if (data != undefined) {
            $scope.data = data;
        }

        $timeout(function () {
            $scope.shouldChange = function (key) {
                // console.log("should change " + key);
                return true;
            }
            $scope.submit = function () {
                Object.keys($scope.data).forEach(function (attr) {
                    if (typeof $scope.data[attr] === 'object') {
                        $scope.data[attr] = $scope.data[attr].option_key;
                    }
                });
                if (this.crudForm.$valid) {
                    $modalInstance.close($scope.data);
                } else {
                    fwErrorService.emitFormErrors(this.crudForm)
                }
            }
        }, 510);
    });

    module.config(appConfig);

    appConfig.$inject = ['$stateProvider'];

    function appConfig($stateProvider) {
        $stateProvider
            .state('app.atendimentos', {
                abstract: true,
                url: '/atendimentos',
                templateUrl: 'app/modules/core/utils/crud/crud.html',
                controller: 'CRUDController',
                resolve: {
                    id: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    module: function () {
                        return 'atendimentos';
                    }
                }
            })
            .state('app.atendimentos.list', {
                url: '',
                templateUrl: 'app/modules/core/utils/crud/crud.list.html',
                resolve: {
                    module: function () {
                        return 'pacientes';
                    }
                }
            })
            // .state('app.atendimentos.new', {
            //     url: '/new/:paciente',
            //     templateUrl: 'app/modules/prontuarios/atendimento.html',
            //     controller: 'AtendimentoController'
            // })
            .state('app.atendimentos.edit', {
                url: '/:id/edit',
                templateUrl: 'app/modules/prontuarios/atendimento.html',
                controller: 'AtendimentoController'
            })
    };
})();
