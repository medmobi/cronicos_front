(function() {
    'use strict';

    var module = angular.module('singApp.categoria-doencas', [
        'pascoal.crud'
    ]);

    // console.log('321');

    module.config(appConfig);

    appConfig.$inject = ['$stateProvider'];

    function appConfig($stateProvider) {
        // console.log('tango312');
        $stateProvider
            .state('app.categoria-doencas', {
                abstract: true,
                url: '/categoria-doencas',
                templateUrl: 'app/modules/core/utils/crud/crud.html',
                controller: 'CRUDController',
                resolve: {
                    id: ['$stateParams', function($stateParams) {
                        return $stateParams.id;
                    }],
                    module: function() {
                        return 'categoria-doencas';
                    }
                }
            })
            .state('app.categoria-doencas.list', {
                url: '',
                templateUrl: 'app/modules/core/utils/crud/crud.list.html'
            })
            .state('app.categoria-doencas.new', {
                url: '/new',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'CRUDEditController'
            })
            .state('app.categoria-doencas.edit', {
                url: '/:id/edit',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'CRUDEditController'
            })
    };
})();