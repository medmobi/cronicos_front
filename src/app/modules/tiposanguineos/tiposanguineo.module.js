(function() {
    'use strict';

    var module = angular.module('singApp.tiposanguineos', [
        'pascoal.crud'
    ]);

    // console.log('321');

    module.config(appConfig);

    appConfig.$inject = ['$stateProvider'];

    function appConfig($stateProvider) {
        // console.log('tango312');
        $stateProvider
            .state('app.tiposanguineos', {
                abstract: true,
                url: '/tiposanguineos',
                templateUrl: 'app/modules/core/utils/crud/crud.html',
                controller: 'CRUDController',
                resolve: {
                    id: ['$stateParams', function($stateParams) {
                        return $stateParams.id;
                    }],
                    module: function() {
                        return 'tiposanguineos';
                    }
                }
            })
            .state('app.tiposanguineos.list', {
                url: '',
                templateUrl: 'app/modules/core/utils/crud/crud.list.html'
            })
            .state('app.tiposanguineos.new', {
                url: '/new',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'CRUDEditController'
            })
            .state('app.tiposanguineos.edit', {
                url: '/:id/edit',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'CRUDEditController'
            })
    };
})();