(function() {
    'use strict';

    angular.module('singApp.dashboard')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$scope', '$http', 'appSettings', 'Usuario', 'fwChartService'];

    function DashboardController($scope, $http, appSettings, Usuario, fwChartService) {
        const today_start = moment().startOf('day');
        const today_end = moment().endOf('day');
        const dateFilter = {
            createdAt: {
                gte: today_start.toDate()
            }
        }; //, lte: today_end.toDate()}};
        const today_filter = {
            where: dateFilter
        };

        const instagram_url = 'https://www.instagram.com/';
        const facebook_graph = 'https://graph.facebook.com/';
        const facebook_app_secret = 'afef03700a8912117084660efc72d937';
        const facebook_app_id = '610556595795002';

        const fb_likes_config = {
            params: {
                fields: 'fan_count',
                access_token: facebook_app_id + '|' + facebook_app_secret
            }
        };
        const instagram_config = {
            params: {
                __a: '1'
            }
        }

        $scope.cards = [
            // {
            //     title: 'Clientes',
            //     lb: Usuario,
            //     count: 0,
            //     count_new: 0,
            //     ui_sref: 'app.customers.list',
            //     icon: 'glyphicon glyphicon-user',
            //     style: 'bg-primary'
            // },
            // {
            //     title: 'Anúncios',
            //     lb: Listing,
            //     count: 0,
            //     count_new: 0,
            //     ui_sref: 'app.listings.list',
            //     icon: 'glyphicon glyphicon-list',
            //     style: 'bg-success'
            // },
            // {
            //     title: 'Reservas',
            //     lb: Reservation,
            //     count: 0,
            //     count_new: 0,
            //     ui_sref: 'app.reservations.list',
            //     icon: 'glyphicon glyphicon-user',
            //     style: 'bg-warning'
            // },
            // {
            //     title: 'Problemas Reserva',
            //     lb: ReservationProblem,
            //     count: 0,
            //     count_new: 0,
            //     ui_sref: 'app.reservationProblems.list',
            //     icon: 'fa fa-warning',
            //     style: 'bg-danger'
            // }
        ];

        var d3chartConfigs = fwChartService.getMockD3chartsConfig();
        var d3chartData = fwChartService.getMockD3chartsData(false);

        $scope.d3chartGlicemiaCapilarConfig = d3chartConfigs.glicemiaCapilar;
        $scope.d3chartGlicemiaCapilarData = d3chartData.glicemiaCapilar;

        $scope.d3chartPressaoConfig = d3chartConfigs.pressao;
        $scope.d3chartPressaoData = d3chartData.pressao;

        $scope.d3chartPesoConfig = d3chartConfigs.peso;
        $scope.d3chartPesoData = d3chartData.peso;

        $scope.d3chartAlturaConfig = d3chartConfigs.altura;
        $scope.d3chartAlturaData = d3chartData.altura;

        $scope.d3chartAlturaConfig = d3chartConfigs.altura;
        $scope.d3chartAlturaData = d3chartData.altura;

        $scope.d3chartIMCConfig = d3chartConfigs.imc;
        $scope.d3chartIMCData = d3chartData.imc;

        $scope.d3chartHdlLdlConfig = d3chartConfigs.hdlLdl;
        $scope.d3chartHdlLdlData = d3chartData.hdlLdl;

        $scope.companies = ['cabemcasa', 'wishastorage'];
        $scope.socials = [{
                id: 'facebook',
                cabemcasa: {
                    likes: 0
                },
                wishastorage: {
                    likes: 0
                },
                style: 'bg-primary'
            },
            {
                id: 'instagram',
                cabemcasa: {
                    likes: 0
                },
                wishastorage: {
                    likes: 0
                },
                style: 'bg-info'
            }
        ];

        initialize();

        function initialize() {
            // loadCards();
            // loadSocialData();
            loadCharts();
        }

        function loadCharts() {

        }

        function loadCards() {
            $scope.cards.forEach(function(card) {
                card.lb.count().$promise.then(function(res) {
                    card.count = res.count;
                })

                card.lb.count(today_filter).$promise.then(function(res) {
                    card.count_new = res.count;
                })
            })
        }

        function loadSocialData() {

            $scope.socials.forEach(function(social) {
                if (social.id === 'facebook') {
                    $scope.companies.forEach(function(company) {
                        $http.get(facebook_graph + company, fb_likes_config).then(function(res) {
                            social[company].likes = res.data.fan_count;
                        }, function(err) {
                            console.log(err);
                        })
                    })
                } else if (social.id === 'instagram') {
                    $scope.companies.forEach(function(company) {
                        $http.get(instagram_url + company, instagram_config).then(function(res) {
                            console.log(res);
                            social[company].likes = res.data.user.followed_by.count;
                        }, function(err) {
                            console.log(err);
                        })
                    })
                }
            });

        }

    }

})();
