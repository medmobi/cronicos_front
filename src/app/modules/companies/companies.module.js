(function() {
  'use strict';

  var module = angular.module('singApp.companies', [
    'pascoal.crud'
  ]);

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    $stateProvider
      .state('app.companies', {
        abstract: true,
        url: '/companies',
        templateUrl: 'app/modules/core/utils/crud/crud.html',
        controller: 'CRUDController',
        resolve:{
          id: ['$stateParams', function($stateParams){
            return $stateParams.id;
          }],
          module: function () {
            return 'company';
          }
        }
      })
      .state('app.companies.list', {
        url: '',
        templateUrl: 'app/modules/core/utils/crud/crud.list.html'
      })
      .state('app.companies.new', {
        url: '/new',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
      .state('app.companies.edit', {
        url: '/:id/edit',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
  };

})();
