(function() {
    'use strict';

    var module = angular.module('singApp.convenios');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Convênios",
            "label_row": "Convênio",
            "route": "convenios",
            "settings": {
                "add": true,
                "edit": true,
                "delete": true
            },
            "fields": [{
                    "name": "id",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "ID",
                    "editable": false,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "codigo",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Código",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                { "name": "conveniado", "type": "integer", "notnull": true, "length": 10, "precision": 10, "label": "Conveniado", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                {
                    "name": "programas",
                    "type": "custom",
                    "notnull": false,
                    "length": 10,
                    "precision": 10,
                    "label": "Programas",
                    "editable": false,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": [],
                    "toString": function(rawData, model) {
                        // debugger;
                        var _label = [];
                        model.attributes.programas.forEach(function(val, key) { _label.push(val.programa.descricao) })

                        return _label.join(', ');
                        // return model.attributes.codigo + model.attributes['conveniado.label'];
                        // debugger;
                        // return 'fabiano gay';
                    }
                },
                {
                    "name": "situacao",
                    "type": "boolean",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Situação",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": {
                      "statusTrueText": "Ativo",
                      "statusFalseText": "Inativo",
                      "default": true
                    }
                }
            ],
            "masterdetails": {
                "programas": {
                    "label": "Programas",
                    "label_row": "Programa",
                    "label_stem": "programas",
                    "fields": [
                        { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        // { "name": "convenio", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "Convênio", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "programa", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Programa", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                        // { "name": "situacao", "type": "boolean", "notnull": true, "length": 255, "precision": 10, "label": "Situação", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
                    ],
                    "masterdetails": [],
                    "route": "programas"
                }
            }
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('convenios', _headers);
    }

})();
