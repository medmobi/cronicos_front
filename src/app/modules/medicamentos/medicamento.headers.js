(function () {
  'use strict';

  var module = angular.module('singApp.medicamentos');

  module.config(cfg);

  // console.log('123');

  function cfg(headersProvider) {
    var _headers = {
      "label": "Medicamentos",
      "label_row": "Medicamento",
      "route": "medicamentos",
      "settings": {
        "add": true,
        "edit": true,
        "delete": true
      },
      "fields": [
        { "name": "id", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "nome_apresentacao", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Nome", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "apresentacao", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Apresentação", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "ggrem", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "GGREM", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "ean", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "EAN", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "registro", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Registro", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
        { "name": "restricao_hospitalar", "type": "boolean", "notnull": true, "length": null, "precision": 10, "label": "Restrição Hospitalar", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "statusTrueText": "Sim", "statusFalseText": "Não" } },
        { "tab": 'fixed', "name": "tipo", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Tipo", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
        { "tab": 'fixed', "name": "principio_ativo_medicamentos", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Princípio Ativo", "editable": true, "viewable": false, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
        { "tab": 'fixed', "name": "produto", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Produto", "editable": true, "viewable": false, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
        { "tab": 'fixed', "name": "classe_terapeutica", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Classe Terapêutica", "editable": true, "viewable": false, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
        { "tab": 'fixed', "name": "laboratorio", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Laboratório", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
        { "tab": 'fixed', "name": "tarja", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Tarja", "editable": true, "viewable": false, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } }
      ]
    };

    // console.log('123');
    // $scope.headers = headers;
    headersProvider.set('medicamentos', _headers);
  }

})();
