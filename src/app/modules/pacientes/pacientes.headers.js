(function () {
  'use strict';

  var module = angular.module('singApp.pacientes');

  module.config(cfg);

  // console.log('123');

  function cfg(headersProvider) {
    var _headers = {
        "label": "Pacientes",
        "label_row": "Paciente",
        "route": "pacientes",
        "fixed_tab": true,
        "settings": {
            "add": true,
            "edit": true,
            "delete": true
        },
        "fields": [
            {"tab":"fixed","name":"id","type":"bigint","notnull":true,"length":null,"precision":10,"label":"ID","editable":false,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":"fixed","name":"nome","type":"string","notnull":true,"length":50,"precision":10,"label":"Nome","editable":true,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":"fixed","name":"sexo","type":"string","notnull":true,"length":100,"precision":10,"label":"Sexo","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":{"enum":{"m":"Masculino","f":"Feminino","o":"Indefinido"}}},
            {"tab":"fixed","name":"foto","type":"string","notnull":false,"length":100,"precision":10,"label":"Foto","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":{"file":{"container":"fotos"},"only_upload":true}},
            {"tab":"fixed","name":"data_nascimento","type":"date","notnull":true,"length":null,"precision":10,"label":"Data de Nascimento","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":"fixed","name":"email","type":"string","notnull":false,"length":255,"precision":10,"label":"E-mail","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":{"email":true}},
            {"tab":"fixed","name":"obs","type":"text","notnull":false,"length":255,"precision":10,"label":"Observações","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},

            // { "tab": 0, "name": "planodesaude", "type": "integer", "notnull": false, "length": 10, "precision": 10, "label": "Plano de Saúde", "editable": true, "viewable": false, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            // { "tab": 0, "name": "plano_carteira", "type": "string", "notnull": false, "length": 10, "precision": 10, "label": "Carteira", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            // { "tab": 0, "name": "plano_validade", "type": "date", "notnull": false, "length": 10, "precision": 10, "label": "Validade", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },

            {"tab":0,"name":"nome_fonetico","type":"string","notnull":false,"length":10,"precision":10,"label":"Nome Fonético","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"profissao","type":"string","notnull":false,"length":10,"precision":10,"label":"Profissão","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"cpf","type":"string","notnull":false,"length":20,"precision":10,"label":"CPF","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":{"cpf":[]}},
            {"tab":0,"name":"estado_civil","type":"integer","notnull":false,"length":10,"precision":10,"label":"Estado Civil","editable":true,"viewable":false,"autocomplete":true,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":{"select":true}},

            {"tab":0,"name":"nome_mae","type":"string","notnull":false,"length":10,"precision":10,"label":"Nome da Mãe","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"profissao_mae","type":"string","notnull":false,"length":10,"precision":10,"label":"Profissão da Mãe","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"nome_pai","type":"string","notnull":false,"length":10,"precision":10,"label":"Nome do Pai","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},

            {"tab":0,"name":"profissao_pai","type":"string","notnull":false,"length":10,"precision":10,"label":"Profissão do Pai","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},

            {"tab":0,"name":"nascimento_pais","type":"integer","notnull":false,"length":10,"precision":10,"label":"País de Nascimento","editable":true,"viewable":false,"autocomplete":true,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"nascimento_uf","type":"integer","notnull":false,"length":10,"precision":10,"label":"Estado de Nascimento","editable":true,"viewable":false,"autocomplete":true,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"nascimento_cidade","type":"integer","notnull":false,"length":10,"precision":10,"label":"Cidade de Nascimento","editable":true,"viewable":false,"autocomplete":true,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"como_conheceu","type":"string","notnull":false,"length":10,"precision":10,"label":"Como conheceu","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"descricao_indicacao","type":"string","notnull":false,"length":10,"precision":10,"label":"Descrição Indicação","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},

            {"tab":0,"name":"rg","type":"string","notnull":false,"length":10,"precision":10,"label":"RG","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"rg_data_emissao","type":"date","notnull":false,"length":10,"precision":10,"label":"Data de Emissão","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"rg_orgao_emissor","type":"string","notnull":false,"length":10,"precision":10,"label":"Org\u00e3o Emissor","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},

            {"tab":0,"name":"registro_nascimento","type":"string","notnull":false,"length":10,"precision":10,"label":"Registro de Nascimento","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"tipo_sangue","type":"integer","notnull":false,"length":10,"precision":10,"label":"Tipo Sanguíneo","editable":true,"viewable":false,"autocomplete":true,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
            {"tab":0,"name":"cns","type":"string","notnull":false,"length":10,"precision":10,"label":"CNS","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},

            {"name":"idade","type":"custom","notnull":true,"length":null,"precision":10,"label":"Idade","editable":false,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[],
             "toString": function (rawData, model) {
                var _label = [];
                // model.attributes.data_nascimento;

                return _calculateAge(new Date(model.attributes.data_nascimento));

                function _calculateAge(birthday) { // birthday is a date
                    var _birthType = ' meses';
                    var _birthMoment = moment(birthday);
                    var _age = moment().diff(_birthMoment, 'months');
                    if (!_age) {
                      _birthType = ' dias';
                      _age = moment().diff(_birthMoment, 'days');
                    }
                    else if (_age > 12) {
                      _birthType = ' anos';
                      _age = moment().diff(_birthMoment, 'years');
                    }

                    return _age + _birthType;
                    // var ageDifMs = Date.now() - birthday.getTime();
                    // var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    // return Math.abs(ageDate.getUTCFullYear() - 1970);
                }
              }
            },
            {"name":"cidade","type":"custom","notnull":true,"length":null,"precision":10,"label":"Cidade","editable":false,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[],
             "toString": function (rawData, model) {
                if (model.attributes.enderecos[0] != undefined) {
                    return model.attributes.enderecos[0].cidade + " - " + model.attributes.enderecos[0].estado;
                } else {}
             }
            },
            {"name":"convenios","type":"custom","notnull":true,"length":null,"precision":10,"label":"Convênios","editable":false,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[],
             "toString": function(rawData, model) {
                var _label = [];
                model.attributes.programas.forEach(function(val, key) {
                    if (val.convenio) {
                    _label.push(val.convenio.codigo)
                    }
                });
                return _label.join(", ");
              }
            },
            {"name":"cids","type":"custom","notnull":true,"length":null,"precision":10,"label":"Cids","editable":false,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[],
             "toString": function (rawData, model) {
                var _label = [];
                model.attributes.cids.forEach(function (val, key) {
                    _label.push(val.cid.descricao)
                });
                return _label.join(", ");
              }
            },
        ],
        "main_tabs": [
            // "Plano de Saúde",
            "Dados Complementares"
        ],
        "masterdetails": {
            // "enderecos": {
            //     "label": "Endere\u00e7os",
            //     "label_row": "Endere\u00e7o",
            //     "label_stem": "enderecos",
            //     "fields": [
            //         { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.padd_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "pes_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "cep", "type": "string", "notnull": true, "length": 10, "precision": 10, "label": "CEP", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "cep": { "address": "logradouro", "city": "cidade", "district": "bairro", "state": "estado" } } },
            //         { "name": "logradouro", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "Endere\u00e7o", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "numero", "type": "string", "notnull": true, "length": 5, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "complemento", "type": "string", "notnull": false, "length": 255, "precision": 10, "label": "Complemento", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         // { "name": "quadra", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Quadra", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         // { "name": "lote", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Lote", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "bairro", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Bairro", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "cidade", "type": "string", "notnull": true, "length": 60, "precision": 10, "label": "Cidade", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "estado", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Estado", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
            //     ],
            //     "masterdetails": []
            // },
            // "telefones": {
            //     "label": "Telefones",
            //     "label_row": "Telefone",
            //     "label_stem": "telefones",
            //     "fields": [
            //         { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "pac_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "tipo_telefone", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Tipo de Telefone", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },
            //         { "name": "numero", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "telefone": true } }
            //     ],
            //     "masterdetails": []
            // }
            // ,
            // "programas": {
            //     "label": "Programas",
            //     "label_row": "Programa",
            //     "label_stem": "programas",
            //     "fields": [
            //         { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "convenio", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "Convênio", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "programa", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Programa", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "situacao", "type": "boolean", "notnull": true, "length": 255, "precision": 10, "label": "Situação", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
            //     ],
            //     "masterdetails": []
            // }
        },
        "tabs_session": {
            "telefones": {
                "label": "Telefones",
                "label_row": "Telefone",
                "label_stem": "telefones",
                "fields": [
                    {"name":"id","type":"integer","notnull":true,"length":null,"precision":10,"label":"models.person_phones.ppho_id","editable":false,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
                    {"name":"pac_id","type":"bigint","notnull":true,"length":null,"precision":10,"label":"models.person_phones.per_id","editable":false,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
                    {"name":"tipo_telefone","type":"integer","notnull":true,"length":30,"precision":10,"label":"Tipo de Telefone","editable":true,"viewable":true,"autocomplete":true,"quickAdd":false,"autocomplete_dependencies":[],"customOptions": []},
                    {"name":"numero","type":"string","notnull":true,"length":60,"precision":10,"label":"N\u00famero","editable":true,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":{"telefone":true}},
                    {"name":"contato","type":"string","notnull":false,"length":60,"precision":10,"label":"Nome do Contato","editable":true,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]}
                ],
                "masterdetails": [],
                "route": "telefones"
            },
            "enderecos": {
                "label": "Endere\u00e7os",
                "label_row": "Endere\u00e7o",
                "label_stem": "enderecos",
                "fields": [
                    {"name":"id","type":"integer","notnull":true,"length":null,"precision":10,"label":"models.person_address.padd_id","editable":false,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
                    {"name":"pes_id","type":"bigint","notnull":true,"length":null,"precision":10,"label":"models.person_address.per_id","editable":false,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
                    {"name":"cep","type":"string","notnull":true,"length":5,"precision":10,"label":"CEP","editable":true,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":{"cep":{"address":"logradouro","city":"cidade","district":"bairro","state":"estado"}}},
                    {"name":"logradouro","type":"string","notnull":true,"length":20,"precision":10,"label":"Endere\u00e7o","editable":true,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
                    {"name":"numero","type":"string","notnull":true,"length":3,"precision":10,"label":"N\u00famero","editable":true,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
                    {"name":"complemento","type":"string","notnull":false,"length":7,"precision":10,"label":"Complemento","editable":true,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
                    // { "name": "quadra", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Quadra", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    // { "name": "lote", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Lote", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                    {"name":"bairro","type":"string","notnull":true,"length":7,"precision":10,"label":"Bairro","editable":true,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
                    {"name":"cidade","type":"string","notnull":true,"length":10,"precision":10,"label":"Cidade","editable":true,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
                    {"name":"estado","type":"string","notnull":true,"length":1,"precision":10,"label":"Estado","editable":true,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]}
                ],
                "masterdetails": []
            },
            "planos_saude": {
                "label": "Planos de Saúde",
                "label_row": "Plano",
                "label_stem": "planos_saude",
                "fields": [
                    {"name":"planodesaude","type":"integer","notnull":true,"length":10,"precision":10,"label":"Plano de Saúde","editable":true,"viewable":false,"autocomplete":true,"quickAdd":[],"autocomplete_dependencies":[],"customOptions": { "select": true }},
                    {"name":"carteira","type":"string","notnull":true,"length":10,"precision":10,"label":"Carteira","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
                    {"name":"validade","type":"date","notnull":true,"length":10,"precision":10,"label":"Validade","editable":true,"viewable":false,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":[]},
                ],
                "masterdetails": [],
                "route": "planos_saude"
            },
            "programas": {
                "label": "Programas",
                "label_row": "Programa",
                "label_stem": "programas",
                "fields": [
                    {"name":"convenio","type":"integer","notnull":true,"length":null,"precision":10,"label":"Convênio","editable":true,"viewable":true,"autocomplete":true,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":{ "select": true }},
                    {"name":"programa","type":"bigint","notnull":true,"length":null,"precision":10,"label":"Programa","editable":true,"viewable":true,"autocomplete":true,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":{ "select": true }},
                    {"name":"situacao","type":"boolean","notnull":true,"length":null,"precision":10,"label":"Situação","editable":true,"viewable":true,"autocomplete":false,"quickAdd":[],"autocomplete_dependencies":[],"customOptions":{"statusTrueText":"Ativo","statusFalseText":"Inativo","default":true}}
                ],
                "masterdetails": [],
                "route": "programas"
            }
        },
        "tabs": {
            // "telefones": {
            //     "name": "telefones",
            //     "label": "Telefones",
            //     "label_row": "Telefone",
            //     "route": "/details/telefones",
            //     "fields": [
            //         { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         // { "name": "pac_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "tipo_telefone", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Tipo de Telefone", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "numero", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "telefone": true } }
            //     ],
            //     "masterdetails": []
            // },
            // "enderecos": {
            //     "label": "Endere\u00e7os",
            //     "label_row": "Endere\u00e7o",
            //     "route": "/details/enderecos",
            //     "fields": [
            //         { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.padd_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         // { "name": "pac_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_address.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "cep", "type": "string", "notnull": true, "length": 10, "precision": 10, "label": "CEP", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "cep": { "address": "logradouro", "city": "cidade", "district": "bairro", "state": "estado" } } },
            //         { "name": "logradouro", "type": "string", "notnull": true, "length": 255, "precision": 10, "label": "Endere\u00e7o", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "numero", "type": "string", "notnull": true, "length": 5, "precision": 10, "label": "N\u00famero", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "complemento", "type": "string", "notnull": false, "length": 255, "precision": 10, "label": "Complemento", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "quadra", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Quadra", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "lote", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Lote", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "bairro", "type": "string", "notnull": true, "length": 100, "precision": 10, "label": "Bairro", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "cidade", "type": "string", "notnull": true, "length": 60, "precision": 10, "label": "Cidade", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "estado", "type": "string", "notnull": true, "length": 50, "precision": 10, "label": "Estado", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
            //     ],
            //     "masterdetails": []
            // },
            // "programas": {
            //     "label": "Programas",
            //     "label_row": "Programa",
            //     "fields": [
            //         { "name": "convenio", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Convênio", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "programa", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "Programa", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
            //         { "name": "situacao", "type": "boolean", "notnull": true, "length": null, "precision": 10, "label": "Situação", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] }
            //     ],
            //     "masterdetails": [],
            //     "route": "/details/programas"
            // }
        }
    };

    // console.log('123');
    // $scope.headers = headers;
    headersProvider.set('pacientes', _headers);
  }

})();
