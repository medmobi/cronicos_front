(function () {
  'use strict';

  var module = angular.module('singApp.pacientes', [
    'pascoal.crud',
    'webcam'
  ]);

  // console.log('321');

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  // module.controller('PacienteEditController', function($scope, Restangular, $stateParams, $timeout, $modal, module, $state, $rootScope, $q, ngToast, $http, $controller) {
  module.controller('PacienteEditController', function ($scope, Restangular, $stateParams, $timeout, $modal, module, $state, $rootScope, $q, ngToast, $http, Upload, $controller) {
    $controller('CRUDEditController', {
      $scope: $scope,
      Restangular: Restangular,
      $stateParams: $stateParams,
      $timeout: $timeout,
      $modal: $modal,
      module: module,
      $state: $state,
      $rootScope: $rootScope,
      $q: $q,
      ngToast: ngToast,
      $http: $http,
      Upload: Upload
    }); //This works

    // angular.element('.fixed-caption-block').prepend('tangod odwn');
    // $scope.apply();



    $scope.customFixedTabHTML = 'app/modules/pacientes/avatar.html';

    $scope.$on('webcam-photo-taken', function (evt, file) {

      function findByName(name) {
        for (var _x in $scope.headers.fields) {
          var field = $scope.headers.fields[_x];

          if (field.name == name) {
            return field;
          }
        }
      }

      var field = findByName('foto');

      $scope._upload(field, file).then(function () {
        // _input.scope().data[_input.scope().field.customOptions.file[0]]
        $scope.data['foto'] = file.name;
      })
    })

    $scope.startWebcam = function () {
      // console.log(tab,data);

      var modalInstance = $modal.open({
        animation: true,
        templateUrl: 'app/modules/pacientes/webcam.html',
        controller: 'WebcamController',

        // size: size,
        // resolve: {
        //   items: function () {
        //     return $scope.items;
        //   }
        // }
      });

      $rootScope.$$phase || $rootScope.$apply();

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
        // console.info('Modal dismissed at: ' + new Date());
      });
    };


  });

  module.controller('WebcamController', function ($scope, WebcamService, $rootScope, $modalInstance) {
    // var $scope = this;

    $scope.showweb = false;
    $scope.webcam = WebcamService.webcam;
    //override function for be call when capture is finalized
    $scope.webcam.success = function () { //function(image, type) {
      // $scope.photo = image;
      // $scope.fotoContentType = type;
      // $scope.showweb = false;

      var filename = 'paciente-foto-' + new Date().getTime() + '.png';
      // console.log($scope.webcam.snapshotData);
      // var blob = new Blob([image], { type: type });
      $scope.webcam.canvas.toBlob(function (_blob) {
        // $scope.downloadFile(_blob, filename);

        var file = _blob;
        file.name = filename;
        // $scope._upload = function(field, file) {


        $rootScope.$broadcast('webcam-photo-taken', file);

        $modalInstance.dismiss('success');

        // }
      });


    };

    function turnOffWebCam() {
      if ($scope.webcam && $scope.webcam.isTurnOn === true)
        $scope.webcam.turnOff();
    }


  });

  function appConfig($stateProvider) {
    // console.log('tango312');
    $stateProvider
      .state('app.pacientes', {
        abstract: true,
        url: '/pacientes',
        templateUrl: 'app/modules/core/utils/crud/crud.html',
        controller: 'CRUDController',
        resolve: {
          id: ['$stateParams', function ($stateParams) {
            return $stateParams.id;
          }],
          module: function () {
            return 'pacientes';
          }
        }
      })
      .state('app.pacientes.list', {
        url: '',
        templateUrl: 'app/modules/core/utils/crud/crud.list.html'
      })
      .state('app.pacientes.new', {
        url: '/new',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'PacienteEditController'
      })
      .state('app.pacientes.edit', {
        url: '/:id/edit',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'PacienteEditController'
      })
  };
})();
