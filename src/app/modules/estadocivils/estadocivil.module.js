(function() {
    'use strict';

    var module = angular.module('singApp.estadocivils', [
        'pascoal.crud'
    ]);

    // console.log('321');

    module.config(appConfig);

    appConfig.$inject = ['$stateProvider'];

    function appConfig($stateProvider) {
        // console.log('tango312');
        $stateProvider
            .state('app.estadocivils', {
                abstract: true,
                url: '/estadocivils',
                templateUrl: 'app/modules/core/utils/crud/crud.html',
                controller: 'CRUDController',
                resolve: {
                    id: ['$stateParams', function($stateParams) {
                        return $stateParams.id;
                    }],
                    module: function() {
                        return 'estadocivils';
                    }
                }
            })
            .state('app.estadocivils.list', {
                url: '',
                templateUrl: 'app/modules/core/utils/crud/crud.list.html'
            })
            .state('app.estadocivils.new', {
                url: '/new',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'CRUDEditController'
            })
            .state('app.estadocivils.edit', {
                url: '/:id/edit',
                templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
                controller: 'CRUDEditController'
            })
    };
})();