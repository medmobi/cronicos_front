(function() {
    'use strict';

    var core = angular.module('singApp.core', [
        'singApp.core.utils',
        'ui.router',
        'ui.bootstrap',
        'ngAnimate',
        'ngStorage',
        'restangular',
        'satellizer',
        'ngToast'
    ]);

    core.config(appConfig);
    core.run(function($rootScope, jQuery, $http, $state, $auth, $window, SatellizerConfig, appSettings, ngToast, $modal, Usuario, auth, Menu, fwModalService) {

        $state.previous = undefined;

        $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
            // if (toState.authenticate && !auth.isAuthenticated()) {
            //   // User isn’t authenticated
            //   $state.transitionTo("main.login.enter");
            //   event.preventDefault();
            // }
            // console.log('change start')
            if (toState.name.indexOf("app.prontuarios.") === 0) {
                auth.isPrestador()
                  .then(function success(user) {})
                  .catch(function error(status) {
                    if(!status) {
                      // User isn't prestador
                      event.preventDefault();
                      $state.go(fromState.name);
                      alert('Usuario não é um prestador, então não pode acessar prontuários!');
                    }
                  });
            }
        });

        $rootScope.$on("$stateChangeSuccess", function (ev, to, toParams, from, fromParams) {
          $state.previous = { route: from, routeParams: fromParams };
          // Force hiding the modal if they are open in page change
          fwModalService.hide();
        });

        $rootScope.$on('$locationChangeSuccess', function() {
            // console.log('change end');
            // $rootScope.actualLocation = $location.path();
        });

        $rootScope.controllerHeaders = {};
        $rootScope.appSettings = appSettings;
        $rootScope.user = {};

        $rootScope._ = _;

        // console.log(jQuery);

        jQuery(document).ajaxSend(function(event, jqxhr, settings) {
            // if ( settings.url == "ajax/test.html" ) {
            //   $( ".log" ).text( "Triggered ajaxSend handler." );
            // }
            // console.log('tango');

            // var config = angular.element(document).injector().get('SatellizerConfig');
            // var storage = angular.element(document).injector().get('SatellizerStorage');
            // var shared = angular.element(document).injector().get('SatellizerShared');

            // console.log(shared);
            // debugger;

            var auth = angular.element(document).injector().get('auth');

            // debugger;


            if (auth.isAuthenticated()) {
                var tokenName = 'X-Access-Token';
                var token = auth.getCurrentUserToken();

                // if (config.authHeader && config.authToken) {
                // token = config.authToken + ' ' + token;
                // }

                // request.headers[config.authHeader] = token;
                jqxhr.setRequestHeader(tokenName, token);
            }
        });

        $rootScope.logout = function() {
            // console.log("LOGOUT");
            var userToken = auth.getCurrentUserToken();
            auth.logout(userToken);
        }

        $rootScope.isAuthenticated = function() {
            return auth.isAuthenticated();
        };

        $rootScope.getMenus = function() {
            // console.log('entrou');

            Menu.getMenus(function(data) {
                $rootScope.menus = data.menus;
            })
            //  console.log(JSON.stringify(menus));
            // if ($window.localStorage.getItem('menus') == undefined) {
            //     var userToken = auth.getCurrentUserToken();
            //   $http.get(appSettings.API_URL + 'users/'+userToken+'user-menu').then(function(data) {

            // var menus = data.data;
            //     $window.localStorage.setItem('menus', JSON.stringify(menus));
            //     $rootScope.menus = menus;
            //   })
            // }
            // else {
            //   var menus = JSON.parse($window.localStorage.getItem('menus'));
            //   $rootScope.menus = menus;
            // }
        };

        $rootScope.recover = function(credentials) {
            Usuario.resetPassword(credentials, function success() {
                ngToast.success('Um e-mail com instruções foi enviado para o endereço solicitado.');
            }, function err() {
                // ngToast.warning('Não foi possível .');
            });
        };

        $rootScope.passwordReset = function(credentials) {
          Usuario.resetPasswordAction(credentials)
            .$promise
            .then(function(response) {
                ngToast.success('Senha alterada com sucesso!');

                // var newCredentials = {email: credentials.email, password: credentials.password};

                $state.go('login');
            })
            .catch(function (err) {
                console.log(err)
                var _msg = err.data.error.message;
                if (_msg === "The token has expired") ngToast.warning('Esse link de recuperação de senha foi expirado! Peça novamente a recuperação de senha.');
                else ngToast.warning(_msg);
            });
        };

        $rootScope.login = function(credentials) {
            Usuario.login(credentials, function success(data) {
                    //console.debug(data);
                    //$auth.setToken(data.id);
                    //window.localStorage['user'] = angular.toJson(data.user);
                    // console.log(data);
                    var accessToken = {
                        id: data.id,
                        userId: data.user.id,
                        user: data.user
                    }
                    auth.setUserLb(accessToken);
                    auth.setUserInfo(data.user);
                    // if (!data.user.emailVerified) {
                    //   angular.extend(toastrConfig, {
                    //     autoDismiss: false,
                    //     containerId: 'toast-container',
                    //     maxOpened: 1,
                    //     newestOnTop: true,
                    //     positionClass: 'toast-top-right',
                    //     preventDuplicates: false,
                    //     preventOpenDuplicates: true,
                    //     progressBar: false,
                    //     target: 'body',
                    //     timeOut: 5000
                    //   });
                    //   toastr.warning('Para poder utilizar a plataforma, realize a verificação de seu e-mail.');
                    // }
                    // vm.countLogin--;
                    // $state.go('main.search');

                    // $window.localStorage.setItem('user', JSON.stringify(data.user));
                    $rootScope.user = data.user;
                    $rootScope.getMenus();

                    $state.go('app.dashboard', {});
                }, function error(data) {
                    if (data.status === 401 || data.status == 400) {
                        // vm.isWrongCredential = true;
                        // console.log('Email ou senha inválidos');
                        // console.log('tango down');
                        ngToast.warning('Não foram encontrados usuários com as credenciais encontradas.');
                    }
                    // vm.countLogin--;
                    // console.log(data);
                })
                // $auth.login(credentials).then(function(data) {

            //   $http.get(appSettings.API_URL + 'auth/user').then(function(user) {

            //     $window.localStorage.setItem('user', JSON.stringify(user.data));
            //     $rootScope.user = user.data;
            //     $rootScope.getMenus();

            //     $state.go('app.dashboard', {});
            //   });

            // }, function errorCallback(error) {
            //   ngToast.warning('Não foram encontrados usuários com as credenciais encontradas.');
            // });
        };

        // $rootScope.register = function(credentials) {
        //   $http.post(appSettings.API_URL + 'auth/register', credentials).then(function(response) {
        //     var data = response.data;
        //     if(data.status == 0) {
        //       // ngToast.success(data.message);

        //       var newCredentials = {email: credentials.email, password: credentials.password};

        //       $rootScope.login(credentials);
        //     }
        //     else {
        //       ngToast.warning(data.message);
        //     }
        //   },function(error) {
        //     var messages = [];

        //     for (var name in error.data) {
        //       // for (var idx in error.data[name]) {
        //         messages.push(error.data[name]);
        //       // }
        //     }

        //     ngToast.warning(messages.join("<br />"));
        //   });
        // };

        $rootScope.myProfile = function(tab, data) {
            // console.log(tab,data);

            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'app/modules/core/utils/my-profile.html',
                controller: 'MyProfileController',
                resolve: {},
                size: 'lg',
                // resolve: {
                //   items: function () {
                //     return $scope.items;
                //   }
                // }
            });

            $rootScope.$$phase || $rootScope.$apply();

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {
                // console.info('Modal dismissed at: ' + new Date());
            });
        };

        $rootScope.aboutUs = function(tab, data){
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'app/modules/core/utils/about-us.html',
                controller: 'AboutUsController',
                resolve: {},
                size: 'lg',
            });

            $rootScope.$$phase || $rootScope.$apply();

            modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem;
            }, function() {
                // console.info('Modal dismissed at: ' + new Date());
            });
        };

        if (auth.isAuthenticated()) {
            $rootScope.user = JSON.parse($window.localStorage.getItem('user'));
            $rootScope.getMenus();
        }

        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams, options) {
                if (!$rootScope.isAuthenticated()) {
                    // console.log(toState.name);
                    var allowed = ['login', 'password', 'password-reset', 'signup', 'new-password'];

                    if (allowed.indexOf(toState.name) == -1) {
                        event.preventDefault();
                        $state.go('login');
                        // return;
                    }
                }
            });

        $rootScope.$state = $state;

    });

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider', 'RestangularProvider', '$httpProvider', '$authProvider', 'appSettings', 'LoopBackResourceProvider', '$locationProvider'];

    function appConfig($stateProvider, $urlRouterProvider, RestangularProvider, $httpProvider, $authProvider, appSettings, LoopBackResourceProvider, $locationProvider) {
        var API_URL = appSettings.API_URL;

        // Use a custom auth header instead of the default 'Authorization'
        LoopBackResourceProvider.setAuthHeader('X-Access-Token');

        // Change the URL where to access the LoopBack REST API server
        LoopBackResourceProvider.setUrlBase(API_URL.replace(/\/$/, ''));

        $authProvider.loginUrl = '/auth/login';
        $authProvider.baseUrl = API_URL;
        $authProvider.httpInterceptor = function(request) { // make sure token is sent only to API URL. Should be improved
            if (request.url.indexOf(API_URL) == 0) {
                return true;
            }
            return false;
        }

        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'app/modules/core/app.html'
            });

        $urlRouterProvider.otherwise(function($injector) {
            var $state = $injector.get('$state');
            $state.go('app.dashboard');
        });

        $locationProvider.html5Mode(true);

        RestangularProvider.setBaseUrl(API_URL);
        RestangularProvider.setRequestInterceptor(function(elem, operation) {
            if (operation === "remove") {
                return undefined;
            }

            if (operation === "put") {
                delete elem.id;
            }
            return elem;
        });

        RestangularProvider.addResponseInterceptor(function(response, operation) {
            if (operation === 'getList') {
                var newResponse = response.data;
                newResponse.pager = response;

                return newResponse;
            }
            return response;
        });

        $httpProvider.interceptors.push('Interceptor');

    }


})();
