(function () {
    'use strict';
    angular
        .module('singApp.core')
        .service('fwDbTypesService', fwDbTypesService);

    fwDbTypesService.inject = [];

    function fwDbTypesService () {

        var self = this;

        self.getIndicadorExamAlternativas = function () {
            return ['Alternativa S/N', 'Lista', 'Múltiplas Alternativas'];
        };

        self.getIndicadorExamAsideAlternativas = function () {
            return ['Numérico', 'Resultado'];
        };

        return {
            getIndicadorExamAlternativas: self.getIndicadorExamAlternativas,
            getIndicadorExamAsideAlternativas: self.getIndicadorExamAsideAlternativas
        };

    }

})();
