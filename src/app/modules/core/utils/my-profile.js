(function() {
  'use strict';

  var module = angular.module('singApp.core');

  module.controller('MyProfileController', function($scope, $rootScope, $http, appSettings, $modalInstance) {
    $scope.showChangePassword = false;

    $scope.toggleChangePassword = function() {
      $scope.showChangePassword = !$scope.showChangePassword;

      if ($scope.showChangePassword == false) {
        delete $rootScope.user.password_new;
        delete $rootScope.user.password_new_confirmation;
        delete $rootScope.user.password_old;
      }
    }

    $scope.cancel = function() {
      $modalInstance.dismiss('success');
    };

    $scope.submit = function() {
      var credentials = {
        password_new : $rootScope.user.password_new,
        password_new_confirmation: $rootScope.user.password_new_confirm,
        password_old: $rootScope.user.password_old
      };

      $http.post(appSettings.API_URL + 'auth/password/change', credentials).then(function(response) {
        var data = response.data;

        if (data.status == 0) {
          ngToast.success(data.message);

          delete $rootScope.user.password_new;
          delete $rootScope.user.password_new_confirmation;
          delete $rootScope.user.password_old;

          $modalInstance.dismiss('success');
        }
        else {
          ngToast.warning(data.message);
        }

      });
    };
  });
})();
