(function() {
    'use strict';
  
    var module = angular.module('singApp.core');
  
    module.controller('AboutUsController', function($scope, $rootScope, $http, appSettings, $modalInstance) {  
      $scope.cancel = function() {
        $modalInstance.dismiss('success');
      };
        
    });
  })();
  