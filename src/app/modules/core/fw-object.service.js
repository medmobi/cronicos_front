(function () {
    'use strict';
    angular
        .module('singApp.core')
        .service('fwObjectService', fwObjectService);

    fwObjectService.inject = ['fwDbTypesService'];

    function fwObjectService(fwDbTypesService) {

        var self = this;

        // labelObj = { 'relation': 'medicamento', 'label': 'nome_apresentacao' }
        self.convertObjSearch = function (original, labelObj) {
            var _objConv = {};

            Object.keys(original).forEach(function (attr) {

                if (original[attr]) {
                    if (typeof original[attr] === 'object') {
                        var _labelObj = {};

                        // if (labelObj && attr === labelObj.relation) {
                        //     _labelObj.label = original[attr][labelObj.label];
                        // }
                        Object.keys(original[attr]).forEach(function (objAttr) {
                            if (objAttr !== 'createdAt' && objAttr !== 'updatedAt') {
                                var _attr = objAttr;
                                if (objAttr !== 'id') _attr = 'label';
                                if (_attr === 'label' && labelObj && attr === labelObj.relation) {
                                    _labelObj.label = original[attr][labelObj.label];
                                } else {
                                    _labelObj[_attr] = original[attr][objAttr];
                                }
                            }
                        });

                        _objConv[attr+'.label'] = _labelObj;
                        _objConv[attr] = _labelObj.id;
                    } else if (attr.indexOf("id") === -1 && attr !== 'createdAt' && attr !== 'updatedAt') {
                        _objConv[attr] = original[attr];
                    }
                }
            });
            return _objConv;
        };

        self.convertObjLabels = function (list) {
            list.forEach(function (item) {
                Object.keys(item).forEach(function (_attr) {
                    if (typeof item[_attr] === 'object') {
                        var attrNoLabel = _attr.split('.')[0];
                        if (_attr.split('.')[1] === 'label') {
                            item[attrNoLabel] = angular.copy(item[_attr]);
                            delete item[_attr];
                        }
                    }
                });
            });
            return list;
        };

        self.setInputsFromObject = function (obj) {
            Object.keys(obj).forEach(function (attr) {

                if (obj[attr] && attr.indexOf("hashKey") === -1 && attr !== 'id' && attr !== 'createdAt' && attr !== 'updatedAt') {
                    var _attrScope = angular.element('#'+attr).scope();

                    if (_attrScope === undefined && attr.indexOf("label") !== -1) {
                        _attrScope = angular.element('#'+attr.split('.')[0]).scope();
                    }

                    // Skip IDs from autocomplete
                    if (_attrScope) {
                        if (!_attrScope.field.autocomplete) {
                            _attrScope.$parent.data[attr] = obj[attr];
                        }
                        else {
                            if (typeof obj[attr] === 'object') {
                                if (!obj[attr].label) obj[attr].label = obj[attr].nome;
                                _attrScope.$parent.data[attr] = obj[attr].label;
                                _attrScope.$parent.data[attr+'.label'] = obj[attr];
                            }
                        }
                    }
                }
            });
        };

        /**
         * Receive list of exams from indicadores with different types and return an array with each type as an object.
         * Example of object to be returned: { ind_id: 1, indicador.label: 'Glicose', indicador: {}, valores: [{ data: '2018-05-18T03:00:00.000Z', valor: 126, resultado: 'Diabetes', cor: '#FF4136' }] }
         * @param exams
         * @param settings
         * @return {Array} examTypeResults
         */
        self.convertDashboardExams = function (exams, settings) {
            var _dashboards = [];
            exams.forEach(function (exam) {
                if (exam.indicador && exam.indicador.exame) {
                    var _dIndex = -1;
                    _dashboards.forEach(function (_dash, dIndex) {
                        if (exam.indicador.id === _dash.ind_id) {
                            _dIndex = dIndex;
                            return;
                        }
                    });

                    var _examValue = {
                        data: exam.date,
                        valor: exam.valor,
                        resultado: exam.resultado,
                        cor: exam.cor
                    };

                    if (_dIndex === -1) {
                        var _newExam = {
                            'ind_id': exam.indicador.id,
                            'indicador.label': exam['indicador.label'],
                            'indicador': exam.indicador,
                            'valores': [],
                            'chart_settings': settings
                        };
                        _newExam.valores.push(_examValue);
                        _dashboards.push(_newExam);
                    } else {
                        _dashboards[_dIndex].valores.push(_examValue);
                    }
                }
            });
            return _dashboards;
        };

        /**
         * Receive list of values from major structure of crudTabList to be converted into backgrid data.
         * Example: prontuario structure with many responses (values)
         * @param type
         * @param structure
         * @param values
         * @return {Array} convertedList
         */
        self.convertCrudTabListExtraData = function (type, structure, values) {
            switch (type) {
                case 'formulario':
                    return self._convertProntuarioExtraData(structure, values);
                default:
                    return null;
            }
        };

        self._convertProntuarioExtraData = function (prontuarios, respostas) {
            respostas.forEach(function (resposta) {
                prontuarios.forEach(function (prontuario) {
                    if (resposta.formulario.id === prontuario.id) {
                        // Modify resultado value for alternativas cases
                        if (fwDbTypesService.getIndicadorExamAlternativas().indexOf(prontuario.indicadores[0].indicador.tipo.nome) !== -1) {
                            var objPairs = JSON.parse(prontuario.indicadores[0].indicador.formula);
                            objPairs.alternativas.forEach(function (alternativa) {
                                if (alternativa.chave == resposta.resposta) {
                                    resposta.resposta = alternativa.legenda;
                                    return;
                                }
                            });
                        }
                    }
                });
            });
            return respostas;
        };

        return {
            convertObjSearch: self.convertObjSearch,
            convertObjLabels: self.convertObjLabels,
            setInputsFromObject: self.setInputsFromObject,
            convertDashboardExams: self.convertDashboardExams,
            convertCrudTabListExtraData: self.convertCrudTabListExtraData
        };

    }

})();
