(function() {
    'use strict';

    angular
        .module('singApp.core')
        .controller('App', AppController)
        .factory('jQuery', jQueryService)
        .factory('$exceptionHandler', exceptionHandler)
        .factory('Interceptor', Interceptor)
        .constant('appSettings', {
            // 'API_URL': 'http://api.cronicos.anexs.com.br:3001/api/'
                'API_URL': 'http://localhost:3000/api/',
                // 'API_URL': 'http://192.168.0.29:3000/api/',
                // 'API_URL': 'http://189.103.145.219:3000/api/',
        });

    AppController.$inject = ['config', '$scope', '$localStorage', '$state'];

    function AppController(config, $scope, $localStorage, $state) {
        /*jshint validthis: true */
        var vm = this;

        vm.title = config.appTitle;

        $scope.app = config;
        $scope.$state = $state;

        if (angular.isDefined($localStorage.state)) {
            $scope.app.state = $localStorage.state;
        } else {
            $localStorage.state = $scope.app.state;
        }
    }

    jQueryService.$inject = ['$window'];

    function jQueryService($window) {
        return $window.jQuery; // assumes jQuery has already been loaded on the page
    }

    exceptionHandler.$inject = ['$log', '$window', '$injector'];

    function exceptionHandler($log, $window, $injector) {
        return function(exception, cause) {
            var errors = $window.JSON.parse($window.localStorage.getItem('sing-2-angular-errors')) || {};
            errors[new Date().getTime()] = arguments;
            $window.localStorage.setItem('sing-2-angular-errors', $window.JSON.stringify(errors));
            if ($injector.get('config').debug) {
                $log.error.apply($log, arguments);
                // $window.alert('check error
            }
        };
    }

    Interceptor.$inject = ['$injector'];

    function Interceptor($injector) {
      return {
        responseError: function(error) {
          var ngToast = $injector.get('ngToast');
          if (error.status === 401 || error.status === 403) {
            if (error.data.error.code === "INVALID_TOKEN") {
              var auth = $injector.get('auth');
              ngToast.warning('Você ficou muito tempo inativo(a) no sistema! Entre novamente.');
              auth.logout();
            }
          }
          else if (error.status === 500) {
              var _split = error.data.error.message.split("update or delete on table \"");
              var _relation = _split[1].split("\" violate");
              if (error.data.error.code === '23503') ngToast.warning('Essa(e) ' + _relation[0] + ' está sendo utilizada(o) e não pode ser removida(o)!' + '<br/>' + 'Remova essa(e) ' + _relation[0] + ' de outros cadastros para conseguir efetivamente removê-la(o).');
          }
          var $q = $injector.get('$q');
          return $q.reject(error);
        }
      };
    }

})();
