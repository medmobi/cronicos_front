(function () {
  'use strict';
  angular
    .module('singApp.core')
    .service('auth', authService);

  authService.inject = ['$window', 'LoopBackAuth', 'Usuario', '$state', '$auth', 'appSettings', '$http'];

  function authService($window, LoopBackAuth, Usuario, $state, $auth, appSettings, $http, $q) {
    var SERVER_URL = appSettings.API_URL;
    var self = this;

    self.updateLocalStorage = function (item, i) {
      var user = self.getUser();
      user[item] = i;
      self.setUserInfo(user);
    }

    self.setUserLb = function (accessToken) {
      // console.log(LoopBackAuth);
      LoopBackAuth.setUser(accessToken.id, accessToken.userId, accessToken.user);
      LoopBackAuth.rememberMe = true;
      LoopBackAuth.save();
    }

    self.setUserInfo = function (user) {
      window.localStorage['user'] = angular.toJson(user);
    }

    self.getUser = function () {
      if (window.localStorage['user']) {
        return JSON.parse(window.localStorage['user']);
      }
      return false;
    }

    self.isAuthenticated = function () {
      return LoopBackAuth.currentUserId != null;
    }

    //Autenticacao Provider
    self.authenticate = function (provider) {
      // self.count++;
      return $auth.authenticate(provider)
        .then(function (response) {
          var obj = { accessToken: response.access_token }
          $http.post(SERVER_URL + 'users/facebook/token', obj)
            .then(
            function (response) {
              // console.log(response);
              var accessToken = {
                id: response.data.id,
                userId: response.data.user.id,
                user: response.data.user
              }
              self.setUserLb(accessToken);
              self.setUserInfo(response.data.user);
              self.removeSatellizer();
              //$auth.setToken(response.data.id);
              // window.localStorage['user'] = angular.toJson(response.data.user);
              // window.localStorage['$LoopBack$accessTokenId'] = window.localStorage.getItem('satellizer_token');
              // window.localStorage['$LoopBack$currentUserId'] = JSON.parse(window.localStorage.getItem('user')).id;
              // window.localStorage['$LoopBack$rememberMe'] = true;
              $state.go('main.search');
            },
            function (response) {
              // console.log(response);
            });
        })
        .catch(function (response) {
          console.debug(response);
        })
        // .finally(function () {
        //   self.count--;
        // })
    }

    self.logout = function (token) {

      Usuario.userLogout({
        'accessToken': token
      }).$promise.then(function (success) {
        LoopBackAuth.clearUser();
        LoopBackAuth.clearStorage();
        $window.localStorage.removeItem('user');
        $state.go('login');
      }, function (err) {
        // console.log('err');
        // console.log(err);
        LoopBackAuth.clearUser();
        LoopBackAuth.clearStorage();
        $window.localStorage.removeItem('user');
        $state.go('login');
      });
    }

    self.getCurrentUserId = function () {
      return LoopBackAuth.currentUserId;
    }

    self.getCurrentUserToken = function () {
      return LoopBackAuth.accessTokenId;
    }

    self.removeSatellizer = function () {
      if ($window.localStorage.getItem('satellizer_token')) {
        $window.localStorage.removeItem('satellizer_token');
      }
    }

    self.isPrestador = function () {
      var deferred = $q.defer();
      var user = self.getUser();

      if(user) {
        Usuario.findById({
          id: user.id,
          filter: {
            include: [
              { "prestador": {} }
            ]
          }
        })
        .$promise
        .then(function success(data) {
          if(data.prestador !== undefined && data.prestador.id) deferred.resolve(data);
          else deferred.reject(false);
        })
        .catch(function error(err) {
          console.error(err);
          deferred.reject(err);
        });
      }
      else {
        deferred.reject(false);
      }

      return deferred.promise;
    }
  }

})();
