(function () {
  'use strict';
  angular
    .module('singApp.core')
    .service('fwDateTimeService', fwDateTimeService);

  fwDateTimeService.inject = [];

  function fwDateTimeService() {

    var self = this;

    self.getDiffDuration = function(start, end, type) {
      if(!moment.isMoment(start)) start = moment(start);
      if(!moment.isMoment(end)) end = moment(end);

      var diff = moment.duration(start.diff(end));
      switch (type) {
        case 'day':
          return diff.asDays();
        case 'hour':
          return diff.asHours();
        case 'minute':
          return diff.asMinutes();
        case 'second':
          return diff.asSeconds();
        case 'week':
          return diff.asWeeks();
        case 'month':
          return diff.asMonths();
        case 'year':
          return diff.asYears();
        default:
          return diff.asMilliseconds();
      }
    };

    return {
      getDiffDuration: self.getDiffDuration
    }

  }

})();
