(function () {
    'use strict';
    angular
        .module('singApp.core')
        .service('fwHeadersService', fwHeadersService);

    fwHeadersService.inject = [];

    function fwHeadersService() {

        var self = this;


        self.updateHeadersFields = function (crudListType, data) {
            // return fields array list
            switch (crudListType) {
                case 'Formulario':
                    return self._updateFormularioFields();
                default:
                    return null;
            }
        };

        self._updateFormularioFields = function () {
            return [
                { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "formulario", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Formulário", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
                { "name": "resposta", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Resposta", "editable": true, "viewable": true, "autocomplete": false, "quickAdd": false, "autocomplete_dependencies": [], "customOptions": [] },
            ];
        };

      // var fields = [];
      //
      // var blurEvent = function (e) {
      //
      //   var $scope = angular.element(this).scope();
      //   var indicadores = $scope.headers.formulario.indicadores;
      //
      //   var indicadoresMap = [];
      //
      //   Object.keys(indicadores).forEach(function (indicador, k) {
      //     indicadoresMap[indicador.indicador.codigo] = indicador.id;
      //   });
      //
      //   Object.keys(indicadores).forEach(function (indicador, k) {
      //     if (indicador.indicador.tipo.id === 3) { // resultado
      //       // console.log();
      //
      //       var formula = JSON.parse(indicador.indicador.formula).formula.trim();
      //       // console.log('formula', formula);
      //
      //       var tokens = formula.split(/[0-9\.\(|\)|*|^|\-|\+|\/]/g);
      //       // console.log('tokens', tokens);
      //       var tokens_unique = tokens.filter(function onlyUnique(value, index, self) {
      //         if (value.trim().length === 0) {
      //           return false;
      //         }
      //         return self.indexOf(value) === index;
      //       });
      //
      //       // console.log('tokens unique:', tokens_unique);
      //       var _formula = formula;
      //
      //       var err = false;
      //
      //       _.each(tokens_unique, function (token) {
      //         var _token = indicadoresMap[token];
      //         if (data[_token] === undefined || data[_token] === null) {
      //           err = true;
      //           return;
      //         }
      //
      //         _formula = _formula.replace(token, data[_token]);
      //       });
      //
      //       if (!err) {
      //         var result = eval(_formula);
      //
      //         data[indicadoresMap[indicador.indicador.codigo]] = result != NaN ? result : null;
      //       }
      //
      //     }
      //   });
      //
      // };
      //
      // var _modalData = {};
      //
      // data.forEach(function (formulario) {
      //   Object.keys(formulario.indicadores).forEach(function (indicador, key) {
      //     var field = {
      //       name: indicador.id,
      //       type: 'string',
      //       label: indicador.informacao + ' (' + indicador.indicador.unidade_medida.nome + ')',
      //       editable: true,
      //       autocomplete: false,
      //       required: true,
      //       // readonly: $scope.disabled,
      //       customOptions: {
      //         events: {
      //           blur: blurEvent
      //         }
      //       }
      //     };
      //
      //     $scope.data.formulario_respostas.forEach(function (formRes, k) {
      //       // console.log(formRes)
      //       // console.log(indicador)
      //       if (formRes.formulario_indicador.ind_id == indicador.ind_id && formulario.id == formRes.form_id) {
      //         if (indicador.indicador.tipo.id !== 5) {
      //           _modalData[indicador.id] = formRes.resposta;
      //         }
      //         // In dropdown list cases, chosen alternative must be an key-value object to be recognized by select template
      //         else {
      //           var _formula = JSON.parse(indicador.indicador.formula);
      //           _.each(_formula.alternativas, function (alt, k) {
      //             if (alt.chave == formRes.resposta) {
      //               _modalData[indicador.id] = {
      //                 value: alt.legenda,
      //                 option_key: alt.chave
      //               };
      //             }
      //           });
      //         }
      //       }
      //     });
      //
      //     if (indicador.indicador.tipo.id == 1) { // number
      //       field.type = 'float';
      //
      //       if (_modalData[indicador.id] != undefined) {
      //         _modalData[indicador.id] = parseFloat(_modalData[indicador.id], 10);
      //       }
      //     }
      //     else if (indicador.indicador.tipo.id == 2) { // string
      //       field.type = 'string';
      //     }
      //     else if (indicador.indicador.tipo.id == 3) { // resultado
      //       field.type = 'string'
      //       field.readonly = true;
      //     }
      //     else if ([4, 5, 6].indexOf(indicador.indicador.tipo.id) != -1) { // alternativa s/n
      //       var _options = {};
      //       console.log(indicador)
      //       var _formula = JSON.parse(indicador.indicador.formula);
      //       _.each(_formula.alternativas, function (alt, k) {
      //         console.log(alt)
      //         console.log(alt.chave)
      //         _options[alt.chave] = alt.legenda;
      //       });
      //
      //       // lista type (w/ dropdown)
      //       if (indicador.indicador.tipo.id === 5) {
      //         field.customOptions = {
      //           "list": _options
      //         }
      //       } else {
      //         field.type = 'string'
      //         field.customOptions = {
      //           "enum": _options
      //         }
      //       }
      //
      //       if (indicador.indicador.tipo.id == 6) { // multiplas escolhas
      //         field.customOptions.multiple = true;
      //       }
      //     }
      //     // else if (indicador.indicador.tipo.id == 5) { // lista
      //     //     field.type = 'tags'
      //     // }
      //
      //     fields.push(field);
      //   });
      //
      // });

        return {
            updateHeadersFields: self.updateHeadersFields
        };

    }

})();
