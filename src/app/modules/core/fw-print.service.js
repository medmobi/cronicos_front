(function () {
    'use strict';
    angular
        .module('singApp.core')
        .service('fwPrintService', fwPrintService);

    fwPrintService.inject = ['$templateRequest', '$rootScope', '$compile', '$timeout'];

    function fwPrintService($templateRequest, $rootScope, $compile, $timeout) {

        var self = this;

        self._printHTML = function (html) {
            var hiddenFrame = $('<iframe></iframe>').appendTo('body')[0];
            hiddenFrame.contentWindow.printAndRemove = function() {
                hiddenFrame.contentWindow.print();
                setTimeout(function(){ $(hiddenFrame).remove(); }, 3000);
            };

            var htmlDocument = "<!doctype html>"+
                "<html>"+
                '<body onload="printAndRemove();">' + // Print only after document is loaded
                html +
                '</body>'+
                "</html>";
            var doc = hiddenFrame.contentWindow.document.open("text/html", "replace");

            doc.write(htmlDocument);
            doc.close();
        };
        self._print = function (templateURL, data) {
            moment.locale('pt-br');
            var printScope = angular.extend($rootScope.$new(), data);
            $templateRequest(templateURL).then(function (template) {
                var element = $compile($('<div>' + template + '</div>'))(printScope);
                var waitForRenderAndPrint = function() {
                    if(printScope.$$phase) {
                        $timeout(waitForRenderAndPrint);
                    } else {
                        self._printHTML(element.html());
                        printScope.$destroy(); // To avoid memory leaks from scope create by $rootScope.$new()
                    }
                };
                waitForRenderAndPrint();
            });
        };
        self.printProntuario = function (prontuario, paciente, empresa, prestador) {
            self._print('app/modules/prontuarios/print/print-prontuario.html',
                {paciente: paciente, prontuario: prontuario, empresa: empresa, prestador: prestador}
            );
        };
        self.printReceituario = function (receituario, paciente, empresa, prestador) {
            self._print('app/modules/prontuarios/print/print-receituario.html',
                {paciente: paciente, receituario: receituario, empresa: empresa, prestador: prestador}
            );
        };
        self.printDocumento = function (documento, paciente, empresa, prestador) {
            self._print('app/modules/prontuarios/print/print-documento-medico.html',
                {paciente: paciente, documento: documento, empresa: empresa, prestador: prestador, data: moment(documento.inicio)}
            );
        };

        return {
            printProntuario: self.printProntuario,
            printReceituario: self.printReceituario,
            printDocumento: self.printDocumento
        }

    }

})();
