(function () {
    'use strict';
    angular
        .module('singApp.core')
        .service('fwDaoService', fwDaoService);

    fwDaoService.inject = ['Restangular', 'Prestador'];

    function fwDaoService (Restangular, Prestador, Formulario) {

        var self = this;

        self.getFormularioByIdWithIndicadores = function (id) {
            return Formulario.findOne({
                filter: {
                    where: { id: id },
                    include: [
                        { 'indicadores': { 'indicador': ['tipo', 'unidade_medida'] } }
                    ]
                }
            })
            .$promise;
        };

        self.getPrestadorFormularios = function (presId, formularioRespostas) {
            return self.getPrestadorWithEspecialidades(presId)
                .then(function (presWithEspecialidades) {
                    // Filter especialidades objects inside PrestadorEspecialidades
                    var especialidades = [];
                    presWithEspecialidades.especialidades.forEach(function (presEsp) {
                        especialidades.push(presEsp.especialidade);
                    });

                    return Restangular.all('formularios')
                        .customPOST({ 'especialidades': especialidades }, 'filtered');
                })
                .then(function success(response) {
                    response.formularios.forEach(function (form, k) {
                        response.formularios[k].active = formularioRespostas.some(function (resposta) {
                            return form.id === resposta.form_id;
                        });
                    });
                    return response.formularios;
                });
        };

        self.getPrestadorWithEspecialidades = function (presId) {
            return Prestador
                .findById({
                    id: presId,
                    filter: {
                        include: [
                            { 'especialidades': { 'especialidade': [] } }
                        ]
                    }
                })
                .$promise;
        };

        self.getPrestadorFormulariosFilteredByEspecialidades = function (especialidades, autocomplete) {
            var body = { 'especialidades': especialidades };
            if (autocomplete) body.autocomplete = true;
            return Restangular.all('formularios')
                .customPOST(body, 'filtered');
        };

        return {
            getFormularioByIdWithIndicadores: self.getFormularioByIdWithIndicadores,
            getPrestadorFormularios: self.getPrestadorFormularios,
            getPrestadorWithEspecialidades: self.getPrestadorWithEspecialidades,
            getPrestadorFormulariosFilteredByEspecialidades: self.getPrestadorFormulariosFilteredByEspecialidades
        };

    }

})();
