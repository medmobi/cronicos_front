(function() {
  'use strict';

  var module = angular.module('singApp.conveniados', [
      'pascoal.crud'
  ]);

  // console.log('321');

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
      // console.log('tango312');
      $stateProvider
          .state('app.conveniados', {
              abstract: true,
              url: '/conveniados',
              templateUrl: 'app/modules/core/utils/crud/crud.html',
              controller: 'CRUDController',
              resolve: {
                  id: ['$stateParams', function($stateParams) {
                      return $stateParams.id;
                  }],
                  module: function() {
                      return 'conveniados';
                  }
              }
          })
          .state('app.conveniados.list', {
              url: '',
              templateUrl: 'app/modules/core/utils/crud/crud.list.html'
          })
          .state('app.conveniados.new', {
              url: '/new',
              templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
              controller: 'CRUDEditController'
          })
          .state('app.conveniados.edit', {
              url: '/:id/edit',
              templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
              controller: 'CRUDEditController'
          })
  };
})();