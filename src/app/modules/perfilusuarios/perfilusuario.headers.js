(function() {
    'use strict';

    var module = angular.module('singApp.perfilusuarios');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Perfil Usuário",
            "label_row": "Perfil Usuário",
            "route": "perfilusuarios",
            "settings": {
                "add": true,
                "edit": true,
                "delete": true
            },
            "fields": [{
                    "name": "id",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "ID",
                    "editable": false,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "nome",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Nome",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                }
            ],
            "tabs_session": {
                "roles": {
                    "label": "Controle de Acessos",
                    "label_row": "Permissão",
                    "label_stem": "permissao",
                    "fields": [
                        { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "role", "type": "integer", "notnull": true, "length": 50, "precision": 10, "label": "Permissão", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },

                    ],
                    "masterdetails": [],
                    "route": "roles"
                }
            }
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('perfilusuarios', _headers);
    }

})();
