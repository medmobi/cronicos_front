(function() {
  'use strict';

  var module = angular.module('singApp.perfilusuarios', [
    'pascoal.crud'
  ]);

  // console.log('321');

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    // console.log('tango312');
    $stateProvider
      .state('app.perfilusuarios', {
        abstract: true,
        url: '/perfilusuarios',
        templateUrl: 'app/modules/core/utils/crud/crud.html',
        controller: 'CRUDController',
        resolve:{
          id: ['$stateParams', function($stateParams){
            return $stateParams.id;
          }],
          module: function () {
            return 'perfilusuarios';
          }
        }
      })
      .state('app.perfilusuarios.list', {
        url: '',
        templateUrl: 'app/modules/core/utils/crud/crud.list.html'
      })
      .state('app.perfilusuarios.new', {
        url: '/new',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
      .state('app.perfilusuarios.edit', {
        url: '/:id/edit',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
  };
})();
