(function() {
  'use strict';

  var module = angular.module('singApp.settings', [
    'pascoal.crud'
  ]);

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    $stateProvider
      .state('app.settings', {
        abstract: true,
        url: '/settings',
        templateUrl: 'app/modules/core/utils/crud/crud.html',
        controller: 'CRUDController',
        resolve:{
          id: ['$stateParams', function($stateParams){
            return $stateParams.id;
          }],
          module: function () {
            return 'settings';
          }
        }
      })
      .state('app.settings.list', {
        url: '',
        templateUrl: 'app/modules/core/utils/crud/crud.list.html'
      })
      .state('app.settings.new', {
        url: '/new',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
      .state('app.settings.edit', {
        url: '/:id/edit',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
  };
})();
