(function() {
  'use strict';

  var module = angular.module('singApp.documentosMedicos', [
    'pascoal.crud'
  ]);

  // console.log('321');

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    // console.log('tango312');
    $stateProvider
      .state('app.documentosMedicos', {
        abstract: true,
        url: '/documentos-medicos',
        templateUrl: 'app/modules/core/utils/crud/crud.html',
        controller: 'CRUDController',
        resolve: {
          id: ['$stateParams', function($stateParams) {
            return $stateParams.id;
          }],
          module: function() {
            return 'documentosMedicos';
          }
        }
      })
      .state('app.documentosMedicos.list', {
        url: '',
        templateUrl: 'app/modules/core/utils/crud/crud.list.html'
      })
      .state('app.documentosMedicos.new', {
        url: '/new',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
      .state('app.documentosMedicos.edit', {
        url: '/:id/edit',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
  };
})();
