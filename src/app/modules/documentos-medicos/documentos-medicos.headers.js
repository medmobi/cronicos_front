(function() {
  'use strict';

  var module = angular.module('singApp.documentosMedicos');

  module.config(cfg);

  // console.log('123');

  function cfg(headersProvider) {
    var _headers = {
      "label": "Documentos Médicos",
      "label_row": "Documento Médico",
      "route": "documentos-medicos",
      "settings": {
        "add": true,
        "edit": true,
        "delete": true
      },
      "fields": [
        {
          "name": "id",
          "type": "string",
          "notnull": true,
          "length": null,
          "precision": 10,
          "label": "ID",
          "editable": false,
          "viewable": false,
          "autocomplete": false,
          "quickAdd": [],
          "autocomplete_dependencies": [],
          "customOptions": []
        },
        {
          "name": "nome",
          "type": "string",
          "notnull": true,
          "length": 50,
          "precision": 10,
          "label": "Nome",
          "editable": true,
          "viewable": true,
          "autocomplete": false,
          "quickAdd": [],
          "autocomplete_dependencies": [],
          "customOptions": []
        },
        {
          "name": "documento_tipo",
          "type": "string",
          "notnull": true,
          "length": 50,
          "precision": 10,
          "label": "Tipo",
          "editable": true,
          "viewable": true,
          "autocomplete": true,
          "quickAdd": [],
          "autocomplete_dependencies": [],
          "customOptions": []
        },
        {
          "name": "texto",
          "type": "text",
          "notnull": true,
          "length": 255,
          "precision": 10,
          "label": "Modelo Texto",
          "editable": true,
          "viewable": true,
          "autocomplete": false,
          "quickAdd": [],
          "autocomplete_dependencies": [],
          "customOptions": {
            "placeholder": "Opções de substituição de informações: [PACIENTE]",
            "rows": 10
          }
        },
        {
          "name": "assinatura_pac_status",
          "type": "boolean",
          "notnull": false,
          "length": 20,
          "precision": 10,
          "label": "Assin. Paciente",
          "editable": true,
          "viewable": true,
          "autocomplete": false,
          "quickAdd": [],
          "autocomplete_dependencies": [],
          "customOptions": {
            "statusTrueText": "Ativo",
            "statusFalseText": "Inativo",
            "default": true
          }
        },
        {
          "name": "assinatura_pres_status",
          "type": "boolean",
          "notnull": false,
          "length": 20,
          "precision": 10,
          "label": "Assin. Prestador",
          "editable": true,
          "viewable": true,
          "autocomplete": false,
          "quickAdd": [],
          "autocomplete_dependencies": [],
          "customOptions": {
            "statusTrueText": "Ativo",
            "statusFalseText": "Inativo",
            "default": true
          }
        }
      ]
    };

    // console.log('123');
    // $scope.headers = headers;
    headersProvider.set('documentosMedicos', _headers);
  }

})();
