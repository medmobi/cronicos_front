(function() {
    'use strict';

    var module = angular.module('singApp.formularios');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Formulários",
            "label_row": "Formulário",
            "route": "formularios",
            "settings": {
                "add": true,
                "edit": true,
                "delete": true
            },
            "fields": [{
                    "name": "id",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "ID",
                    "editable": false,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "nome",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Nome",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "descricao",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Descrição",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "situacao",
                    "type": "boolean",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Situação",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": {
                      "statusTrueText": "Ativo",
                      "statusFalseText": "Inativo",
                      "default": true
                    }
                },
                {
                    "name": "geral",
                    "type": "boolean",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Formulário Geral",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": {
                      "statusTrueText": "Sim",
                      "statusFalseText": "Não"
                    }
                }
            ],
            "masterdetails": {
                "indicadores": {
                    "label": "Informações",
                    "label_row": "Informação",
                    "label_stem": "indicadores",
                    "fields": [
                        { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "ID", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "informacao", "type": "string", "notnull": true, "length": null, "precision": 10, "label": "Informação", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                        { "name": "indicador", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Indicador", "editable": true, "viewable": false, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "currency": [], "select": true } },
                        { "name": "situacao", "type": "boolean", "notnull": true, "length": null, "precision": 10, "label": "Situação", "editable": true, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": {"statusTrueText": "Ativo", "statusFalseText": "Inativo", "default": true} }
                    ],
                    "masterdetails": [],
                    "route": "indicadores"
                },
                "especialidades": {
                    "label": "Especialidades",
                    "label_row": "Especialidade",
                    "label_stem": "especialidades",
                    "fields": [
                      { "name": "id", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.ppho_id", "editable": false, "viewable": false, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                      { "name": "pres_id", "type": "bigint", "notnull": true, "length": null, "precision": 10, "label": "models.person_phones.per_id", "editable": false, "viewable": true, "autocomplete": false, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": [] },
                      { "name": "especialidade", "type": "integer", "notnull": true, "length": null, "precision": 10, "label": "Especialidade", "editable": true, "viewable": true, "autocomplete": true, "quickAdd": [], "autocomplete_dependencies": [], "customOptions": { "select": true } },

                    ],
                    "masterdetails": [],
                    "route": "especialidades"
                }
            }
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('formularios', _headers);
    }

})();
