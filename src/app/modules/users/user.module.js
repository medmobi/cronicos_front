(function() {
  'use strict';

  var module = angular.module('singApp.users', [
    'pascoal.crud'
  ]);

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    $stateProvider
      .state('app.users', {
        abstract: true,
        url: '/users',
        templateUrl: 'app/modules/core/utils/crud/crud.html',
        controller: 'CRUDController',
        resolve:{
          id: ['$stateParams', function($stateParams){
            return $stateParams.id;
          }],
          module: function () {
            return 'users';
          }
        }
      })
      .state('app.users.list', {
        url: '',
        templateUrl: 'app/modules/core/utils/crud/crud.list.html'
      })
      .state('app.users.new', {
        url: '/new',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
      .state('app.users.edit', {
        url: '/:id/edit',
        templateUrl: 'app/modules/core/utils/crud/crud.edit.html',
        controller: 'CRUDEditController'
      })
  };
})();
