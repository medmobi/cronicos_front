(function() {
    'use strict';

    var module = angular.module('singApp.users');

    module.config(cfg);

    // console.log('123');

    function cfg(headersProvider) {
        var _headers = {
            "label": "Usuário",
            "label_row": "Usuário",
            "route": "users",
            "settings": {
                "add": true,
                "edit": true,
                "delete": true
            },
            "fields": [{
                    "name": "id",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "ID",
                    "editable": false,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "nome",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Nome",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "password",
                    "type": "password",
                    "notnull": false,
                    "length": null,
                    "precision": 10,
                    "label": "Senha",
                    "editable": true,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "confirm_password",
                    "type": "password",
                    "notnull": false,
                    "length": null,
                    "precision": 10,
                    "label": "Confirmar Senha",
                    "editable": true,
                    "viewable": false,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "email",
                    "type": "string",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "E-mail",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "status",
                    "type": "boolean",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Situação",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": {
                      "statusTrueText": "Ativo",
                      "statusFalseText": "Inativo",
                      "default": true
                    }
                },
                {
                    "name": "perfil_usuario",
                    "type": "integer",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Perfil de Usuário",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": true,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "prestador",
                    "type": "integer",
                    "notnull": false,
                    "length": null,
                    "precision": 10,
                    "label": "É Prestador?",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": true,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                },
                {
                    "name": "admin",
                    "type": "boolean",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Administrador Geral",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": false,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": {
                      "statusTrueText": "Sim",
                      "statusFalseText": "Não"
                    }
                },
                {
                    "name": "empresa",
                    "type": "integer",
                    "notnull": true,
                    "length": null,
                    "precision": 10,
                    "label": "Empresa",
                    "editable": true,
                    "viewable": true,
                    "autocomplete": true,
                    "quickAdd": [],
                    "autocomplete_dependencies": [],
                    "customOptions": []
                }
            ]
        };

        // console.log('123');
        // $scope.headers = headers;
        headersProvider.set('users', _headers);
    }

})();
